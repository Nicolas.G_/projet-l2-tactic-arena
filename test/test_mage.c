/**
 * \file test_mage.c
 * \brief Fichier de test de la classe mage
 * \author Pierre garcon
 * \version 1.0
 * \date 2 fevrier
 *
 * Test de toute les caracteristique de la classe mage
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/mage.h"

/**
 * \fn int main (void)
 * \brief Fonction main
 *
 * \return int 0
 */
int main()
{
  /**_*/
  err_t erreur;
  etat_t etat;
  liste_perso_t * equipe = NULL;
  equipe_t coul_equipe;
  race_t * orc = race_creer_orc();
  race_t * demon = race_creer_demon();

  coul_equipe = Bleu;

  /*On commence par tester la création des mages*/
  printf("\nCeci est un test de mages");
  mage_t * mage1, *mage2;
  printf("\n Test de création d'un mage : \n");
  mage1 = creer_mage(coul_equipe, orc, equipe);
  mage2 = creer_mage(coul_equipe, demon, equipe);

  /*Ensuite, on test l'affichage*/
  printf("\n Test d'affichage : \n");
  mage1->print((personnage_t*)mage1);
  printf("\nPersonnage 2 :\n");
  mage2->print((personnage_t*)mage2);


  /*On active les bonus des deux personnages (bonus différents en fonction des races)*/
  mage1->race->race_activer_bonus(mage1);
  mage2->race->race_activer_bonus(mage2);

  /*On affiche les personnage après activation des bonus pour voir si ils ont mmageé*/
  printf("\n--------------------------------------------------------------------\n");
  printf("\nAffichage après activation du bonus\n");

  printf("\n Test d'affichage : \n");
  mage1->print((personnage_t*)mage1);
  printf("\nPersonnage 2 :\n");
  mage2->print((personnage_t*)mage2);
  printf("\n--------------------------------------------------------------------\n");

  /*On test l'attaque sur les archer*/
  printf("\nTest d'attaque sur un archer : \n");

  mage2->deplacer(4,2,(personnage_t*)mage2);

  printf("\naffichage des coordonnées de archer2\n\n");
  printf("x : %d \ny : %d", mage2->pos_x, mage2->pos_y);

  statut_att comp = mage1->competence1((personnage_t*)mage1, mage2->position_pers((personnage_t*)mage2));
  afficher_statut_attaque(comp);

  etat = Vivant;

  /*On vérifie l'état de l'mage*/
  if(etat == Vivant)
  {
    printf("\nL'mage est toujours vivant");
  }
  else
  {
    printf("\nL'mage est mort\n");
  }

  if(etat == Vivant)
  {
    printf("\nL'mage est toujours vivant");
  }
  else
  {
    printf("\nL'mage est mort\n");
  }

  /*On désactive les bonus*/
  mage1->race->race_desactiver_bonus(mage1);
  mage2->race->race_desactiver_bonus(mage2);

  /*On test l'affichage et on supprime les mages*/
  printf("\nAfficher après dégats et les bonus sont enlevés : \n");
  mage1->print((personnage_t*)mage1);

  printf("\ntest de suppression : \n");
  erreur = mage1->detruire((personnage_t**)&mage1);
  erreur = mage2->detruire((personnage_t **)&mage2);

  demon->detruire(&demon);
  orc->detruire(&orc);

  if(erreur != Ok)
  {
    printf("\nerreur de suppression\n");
  }
  else
    printf("\nTout s'est bien passé\n");
  return 0;
}
