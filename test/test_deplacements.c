/**
 * \file test_combats.c
 * \brief Fichier pour tester les combats
 * \author 
 * \version 1.0
 * \date 
 *
 * Permet de s'assurer que les fonctions de déplacements fonctionnent
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/deplacements.h"
#include "../include/map.h"
#include "../include/personnage.h"


int main()
{
	srand(time(NULL));
	printf("---SEQUENCE DE TESTS DES FONCTIONS DE DEPLACEMENTS---\n");
	
	personnage_t * perso1;
	t_pos position_perso = {5,7};

	perso1 = malloc(sizeof(personnage_t));
	map[5][7].perso = perso1;
	perso1->pos_x = position_perso.x;
	perso1->pos_y = position_perso.y;
	perso1->portee.deplacement = 2;

	
	
	if( chargement_carte() == 1)
	{
		printf("Erreur chargement carte\n");
		return 1;
	}
	
	
	map[5][7].type_emplacement = PERSO;//Placement du personnage sur la map
	afficher_plateau(VISUEL);

	int portee_deplacement = perso1->portee.deplacement;
	int nb_cases_valides = compter_cases_valides(portee_deplacement, position_perso);
	printf("Nombre de cases disponible pour un déplacement de %i autour de [%i,%i] : %i .\n", portee_deplacement, position_perso.x, position_perso.y, nb_cases_valides);
	
	//Test de la liste des positions
	liste_pos_t * liste_de_positions = deplacement_valide(perso1);
	afficher_liste_positions_terminal(liste_de_positions);

	//Inviter le joueur à se déplacer
	afficher_plateau(VISUEL);
	proposer_deplacement_terminal(perso1);
	
	afficher_plateau(VISUEL);

	printf("Direction du personnage déplacé : ");
	switch(perso1->direction)
	{
		case haut: printf("Haut");break;
		case bas: printf("Bas");break;
		case gauche: printf("Gauche");break;
		case droite: printf("Droite");break;
		default: printf("Pas de position");
	}
	printf("\n");
	
	//Libération de la mémoire allouée
	liste_positions_detruire( &liste_de_positions );
	free(perso1);
	perso1 = NULL;
	return 0;
}
