#include <stdio.h>
#include "../include/liste_positions.h"

int main()
{
	int nb_indices = 10;
	int i = 0;
	liste_pos_t * liste_de_positions = liste_positions_initialiser(nb_indices);
	if( liste_de_positions != NULL)
	{
		printf("Création de la liste.\n");
	}

	t_pos case1 = {5,6};
	printf("Ajout de positions dans la liste.\n");
	for(i = 0; i < (nb_indices); i++)
	{
		//printf("tour %i\n", i);
		if( liste_positions_ajouter( liste_de_positions, case1, i ) == Erreur)
		{
			printf("Erreur d'ajout dans la liste");
		}	
		case1.x = (case1.x + 5)%24;
		case1.y = (case1.x + 3)%24;
	}

	printf("Taille de la liste : %i \n", taille_liste(liste_de_positions));
	afficher_liste_positions_terminal(liste_de_positions);




	//Test ajout d'une position en trop dans la liste
	printf("Ajout d'une position en trop.\n");
	liste_positions_ajouter( liste_de_positions, case1, 10 );


	//Test détection d'un doublon dans une liste
	printf("\n\nTest présence d'un doublon, insertion d'une pos [5,6]\n");
	t_pos pos_doublon = {5,6};
	if(detecter_doublon(pos_doublon, liste_de_positions) == 1) printf("Doublon détecté\n");
	else printf("Aucun doublon detecté\n");







	//Creation de la deuxieme liste
	printf("\n\nCréation d'une deuxieme liste\n");
	liste_pos_t * liste_de_positions2 = liste_positions_initialiser(nb_indices);
	if( liste_de_positions2 != NULL)
	{
		printf("Création de la liste.\n");
	}

	t_pos case2 = {10,13};
	printf("Ajout de positions dans la liste.\n");
	for(i = 0; i < (nb_indices); i++)
	{
		//printf("tour %i\n", i);
		if( liste_positions_ajouter( liste_de_positions2, case2, i ) == Erreur)
		{
			printf("Erreur d'ajout dans la liste");
		}	
		case2.x = (case2.x + 5)%24;
		case2.y = (case2.x + 3)%24;
	}

	printf("Taille de la liste liste_de_positions2 : %i \n", taille_liste(liste_de_positions2));
	afficher_liste_positions_terminal(liste_de_positions2);




	//Concaténation de deux listes
	printf("\n\nConcatenation des deux listes en une seule\n");
	liste_pos_t * nouvelle_liste = concatener_listes_positions(liste_de_positions, liste_de_positions2);

	printf("Taille de la liste : %i \n", taille_liste(nouvelle_liste));
	afficher_liste_positions_terminal(nouvelle_liste);


	liste_pos_t * encore_une_liste = NULL;
	printf("\n\nConcatenation des deux listes en une seule, la premiere est vide\n");
	encore_une_liste = concatener_listes_positions(encore_une_liste, liste_de_positions2);
	printf("Taille de la liste : %i \n", taille_liste(encore_une_liste));
	afficher_liste_positions_terminal(encore_une_liste);


	liste_pos_t * liste_trop_grande = liste_positions_initialiser(7);
	liste_positions_ajouter( liste_trop_grande, case1, 0 );
	liste_positions_ajouter( liste_trop_grande, case2, 1 );
	printf("\n\nTest de la fonction ajuster_taille_liste\n");
	printf("Taille de la liste avant réduction : %i\n", taille_liste(liste_trop_grande));
	afficher_liste_positions_terminal(liste_trop_grande);
	liste_trop_grande = ajuster_taille_liste(liste_trop_grande, 2);
	printf("Taille de la liste après réduction : %i\n", taille_liste(liste_trop_grande));	
	afficher_liste_positions_terminal(liste_trop_grande);

	printf("\n\nTest de la fonction retirer occurence de la liste\n");
	nouvelle_liste = retirer_occurences_liste(nouvelle_liste, liste_trop_grande);
	printf("Taille de la liste : %i \n", taille_liste(nouvelle_liste));
	afficher_liste_positions_terminal(nouvelle_liste);

	printf("\n\nTest de la fonction retirer occurence de la liste, 0 élements restants\n");
	nouvelle_liste = retirer_occurences_liste(nouvelle_liste, nouvelle_liste);
	printf("Taille de la liste : %i \n", taille_liste(nouvelle_liste));
	afficher_liste_positions_terminal(nouvelle_liste);
	
	printf("\n\nTest de la fonction retirer UNE occurence POSITION de la liste liste_de_positions2\n");
	printf("Taille de la liste : %i \n", taille_liste(liste_de_positions2));
	afficher_liste_positions_terminal(liste_de_positions2);
	t_pos pos_retirer = {1,4};
	
	nouvelle_liste = retirer_une_occurence_liste(liste_de_positions2, pos_retirer);
	printf("Taille de la liste : %i \n", taille_liste(liste_de_positions2));
	afficher_liste_positions_terminal(liste_de_positions2);

	if( liste_positions_detruire( &nouvelle_liste ) == Erreur)
	{
		printf("Erreur à la destruction de la liste\n");
	}
	else
	{
		printf("Destruction de la liste.\n");
	}
	liste_positions_detruire( &liste_trop_grande );

	return 0;
}
