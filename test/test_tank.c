/**
 * \file test_tank.c
 * \brief Fichier de test de la classe tank
 * \author Pierre garcon
 * \version 1.0
 * \date 2 fevrier
 *
 * Test de toute les caracteristique de la classe tank
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/tank.h"

/**
 * \fn int main (void)
 * \brief Fonction main
 *
 * \return int 0
 */
int main()
{
  /**_*/
  err_t erreur;
  etat_t etat;
  liste_perso_t * equipe = NULL;
  equipe_t coul_equipe;
  race_t * orc = race_creer_orc();
  race_t * demon = race_creer_demon();

  coul_equipe = Bleu;

  /*On commence par tester la création des tanks*/
  printf("\nCeci est un test de tanks");
  tank_t * tank1, *tank2;
  printf("\n Test de création d'un tank : \n");
  tank1 = creer_tank(coul_equipe, orc, equipe);
  tank2 = creer_tank(coul_equipe, demon, equipe);

  /*Ensuite, on test l'affichage*/
  printf("\n Test d'affichage : \n");
  tank1->print((personnage_t*)tank1);
  printf("\nPersonnage 2 :\n");
  tank2->print((personnage_t*)tank2);


  /*On active les bonus des deux personnages (bonus différents en fonction des races)*/
  tank1->race->race_activer_bonus(tank1);
  tank2->race->race_activer_bonus(tank2);

  /*On affiche les personnage après activation des bonus pour voir si ils ont mtanké*/
  printf("\n--------------------------------------------------------------------\n");
  printf("\nAffichage après activation du bonus\n");

  printf("\n Test d'affichage : \n");
  tank1->print((personnage_t*)tank1);
  printf("\nPersonnage 2 :\n");
  tank2->print((personnage_t*)tank2);
  printf("\n--------------------------------------------------------------------\n");

  /*On test l'attaque sur les archer*/
  printf("\nTest d'attaque sur un archer : \n");

  tank2->deplacer(4,2,(personnage_t*)tank2);

  printf("\naffichage des coordonnées de archer2\n\n");
  printf("x : %d \ny : %d", tank2->pos_x, tank2->pos_y);

  statut_att comp = tank1->competence1((personnage_t*)tank1, tank2->position_pers((personnage_t*)tank2));
  afficher_statut_attaque(comp);

  etat = Vivant;

  /*On vérifie l'état de l'tank*/
  if(etat == Vivant)
  {
    printf("\nL'tank est toujours vivant");
  }
  else
  {
    printf("\nL'tank est mort\n");
  }

  if(etat == Vivant)
  {
    printf("\nL'tank est toujours vivant");
  }
  else
  {
    printf("\nL'tank est mort\n");
  }

  /*On désactive les bonus*/
  tank1->race->race_desactiver_bonus(tank1);
  tank2->race->race_desactiver_bonus(tank2);

  /*On test l'affichage et on supprime les tanks*/
  printf("\nAfficher après dégats et les bonus sont enlevés : \n");
  tank1->print((personnage_t*)tank1);

  printf("\ntest de suppression : \n");
  erreur = tank1->detruire((personnage_t**)&tank1);
  erreur = tank2->detruire((personnage_t **)&tank2);

  demon->detruire(&demon);
  orc->detruire(&orc);

  if(erreur != Ok)
  {
    printf("\nerreur de suppression\n");
  }
  else
    printf("\nTout s'est bien passé\n");
  return 0;
}
