/**
 * \file test_archer.c
 * \brief Fichier de test de la classe archer
 * \author Pierre Garcon
 * \version 1.0
 * \date 2 fevrier
 *
 * Test de toute les caracteristique de la classe archer
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/archer.h"
#include "../include/personnage.h"


/*C'est un test de la classe archer*/
/**
 * \fn int main (void)
 * \brief Fonction main
 *
 * \return int 0
 */
int main()
{
  /*On créer les variables de travailles*/
  err_t erreur;
  //etat_t etat;
  equipe_t coul_equipe;
  liste_perso_t * equipe = NULL;
  race_t * orc = race_creer_orc();
  race_t * demon = race_creer_demon();

  coul_equipe = Bleu;

  /*On commence par tester la création des archers*/
  printf("\nCeci est un test de archers");
  archer_t * arch1, *arch2;
  printf("\n Test de création d'un archer : \n");
  arch1 = creer_archer(coul_equipe, orc, equipe);
  arch2 = creer_archer(coul_equipe, demon, equipe);

  /*Ensuite, on test l'affichage*/
  printf("\n Test d'affichage : \n");
  arch1->print((personnage_t*)arch1);
  printf("\nPersonnage 2 :\n");
  arch2->print((personnage_t*)arch2);

  /*On active les bonus des deux personnages (bonus différents en fonction des races)*/
  arch1->race->race_activer_bonus(arch1);
  arch2->race->race_activer_bonus(arch2);

  /*On affiche les personnage après activation des bonus pour voir si ils ont marché*/
  printf("\n--------------------------------------------------------------------\n");
  printf("\nAffichage après activation du bonus\n");

  printf("\n Test d'affichage : \n");
  arch1->print((personnage_t*)arch1);
  printf("\nPersonnage 2 :\n");
  arch2->print((personnage_t*)arch2);
  printf("\n--------------------------------------------------------------------\n");


  /*On test l'attaque sur les archer*/
  printf("\nTest d'attaque sur un archer : \n");

  arch2->deplacer(4,2,(personnage_t*)arch2);

  printf("\naffichage des coordonnées de archer2\n\n");
  printf("x : %d \ny : %d", arch2->pos_x, arch2->pos_y);

  statut_att comp = arch1->competence1((personnage_t*)arch1, arch2->position_pers((personnage_t*)arch2));
  afficher_statut_attaque(comp);

  /*On désactive les bonus*/
  arch1->race->race_desactiver_bonus(arch1);
  arch2->race->race_desactiver_bonus(arch2);

  /*On test l'affichage et on supprime les archers*/
  printf("\nAfficher après dégats et les bonus sont enlevés : \n");
  arch1->print((personnage_t*)arch1);

  printf("\ntest de suppression : \n");
  erreur = arch1->detruire((personnage_t**)&arch1);
  erreur = arch2->detruire((personnage_t **)&arch2);

  //free(*equipe);
  demon->detruire(&demon);
  orc->detruire(&orc);


  if(erreur != Ok)
  {
    printf("\nerreur de suppression\n");
  }
  else
    printf("\nTout s'est bien passé\n");
  return 0;
}
