#include <stdio.h>
#include "../include/commun.h"
#include "../include/map.h"
#include "../include/liste_malus.h"
#include "../include/malus.h"
#include "../include/attaques.h"
#include "../include/deplacements.h"
#include "../include/equipe.h"
#include "../include/personnage.h"



void terminal_choix_action(personnage_t * joueur, int * tour_pas_fini); //Faire un choix d'action à partir du terminal
void terminal_afficher_direction_persos(liste_perso_t * equipe_rouge, liste_perso_t * equipe_bleue);
void terminal_afficher_direction_perso(personnage_t * perso);

int main()
{
	t_temps cycle_jour_nuit = jour;

	equipe_t tour_en_cours = Bleu;

	liste_perso_t * equipe_rouge = liste_perso_creer(NB_PERS); // = fonction_creation_equipe();
  	liste_perso_t * equipe_bleue = liste_perso_creer(NB_PERS); // = fonction_creation_equipe();

	int tour_pas_fini = 1;
	int compteur_cycle = 1;
	int continuerJeu = 1;
	int p_bleu = 0;
	int p_rouge = 0;
	personnage_t * joueur_courant; //Contient le joueur courant
	int nb_tours = 1;

	equipe_t bleu = Bleu;
	equipe_t rouge = Rouge;

	//Initialisation des équipes, personnages et races : PIERRE
	liste_perso_equipe_creer(equipe_bleue, bleu);
	liste_perso_equipe_creer(equipe_rouge, rouge);

	liste_malus_initialiser();
	race_activer(equipe_rouge);
	race_activer(equipe_bleue);
	/*Pour mettre à jour les races, faire : race_maj_equipe(equipe_bleu, equipe_rouge, cycle_jour_nuit)*/
	/*Pour mettre à jour les malus, faire : liste_malus_actualiser()*/

	//Placement des personnages sur la carte et affichage de la carte
	if(chargement_carte() != 0)
	{
		fprintf(stderr, "Failed to load map.\n");
		return EXIT_FAILURE;
	}

	placer_equipes(equipe_rouge, equipe_bleue);
	terminal_afficher_direction_persos(equipe_rouge, equipe_bleue);
	afficher_plateau(VISUEL);


	while(continuerJeu == 1)
	{
		printf("Tour n°: %i \n", nb_tours);
		tour_pas_fini = 1;
		//Cycle jour Nuit
		if(compteur_cycle == 5)
		{
			compteur_cycle = 0;
			switch(cycle_jour_nuit)
			{
				case jour:
					printf("C'est le jour !\n");
					cycle_jour_nuit = nuit;
					break;
				case nuit:
					printf("C'est la nuit !\n");
					cycle_jour_nuit = jour;
					break;
			}
		}
		compteur_cycle++;

		//Gestion des Malus
		liste_malus_actualiser();


		printf("Equipe Rouge : %i\n", equipe_rouge->nb_vivants);
		printf("Equipe Bleue : %i\n", equipe_bleue->nb_vivants);
		switch(tour_en_cours)
		{
			case Bleu:
				//Donner le tour à un joueur bleu
				joueur_courant = equipe_bleue->liste[p_bleu];

				//Proposer des déplacements et attaques
				if(joueur_courant->etat == Vivant)//Passer au tour de l'équipe suivante si le joueur est mort
				{
					while(tour_pas_fini == 1)
					{
						terminal_choix_action(joueur_courant, &tour_pas_fini);
						afficher_plateau(VISUEL);

					}
					joueur_courant->pt_attaque = 1;
					joueur_courant->pt_deplacement = 1;
				}



				p_bleu++;
				if(p_bleu == 5)
				{
					p_bleu = 0;
				}
				tour_en_cours = Rouge;
				break;

			case Rouge:
				//Donner le tour à un joueur rouge
				joueur_courant = equipe_rouge->liste[p_rouge];

				//Proposer des déplacements et attaques
				if(joueur_courant->etat == Vivant)//Passer au tour de l'équipe suivante si le joueur est mort
				{
					while(tour_pas_fini == 1)
					{

						terminal_choix_action(joueur_courant, &tour_pas_fini);
						afficher_plateau(VISUEL);

					}
					joueur_courant->pt_attaque = 1;
					joueur_courant->pt_deplacement = 1;
				}


				p_rouge++;
				if(p_rouge == 5)
				{
					p_rouge = 0;
				}
				tour_en_cours = Bleu;
				break;
		}




		nb_tours++;

		//CHECK GAGNANT
		if(equipe_bleue->nb_vivants == 0 || equipe_rouge->nb_vivants == 0)
		{
			if(equipe_bleue->nb_vivants == 0)
			{
				printf("Victoire de l'équipe bleue\n");
				continuerJeu = 0;
			}
			if(equipe_rouge->nb_vivants == 0)
			{
				printf("Victoire de l'équipe rouge\n");
				continuerJeu = 0;
			}
		}
		//printf("TOUR BOUCLE RENDU \n");
	}

	liste_detruire(&equipe_bleue);
	liste_detruire(&equipe_rouge);

	return EXIT_SUCCESS;
}


void terminal_choix_action(personnage_t * joueur, int * tour_pas_fini) //Faire un choix d'action à partir du terminal
{
	t_pos position;
	int choix_menu = 0;
	int choix_x, choix_y;
	printf("Tour du joueur en [%i,%i]\n", joueur->pos_x, joueur->pos_y);
	printf("Nom : %s, PV : %i", joueur->nom, joueur->pv);

	if(joueur->equipe == Bleu) printf("Equipe bleue\n");
	else printf("Equipe rouge\n");

	printf("1. Effectuer un déplacement\n");
	printf("2. Effectuer une attaque\n");

	printf("3. Terminer son tour\n");
	printf("4. Abandonner\n");

	do
	{
		printf("Action à effectuer :\n");
		scanf("%i", &choix_menu);
	}while(choix_menu < 1 || choix_menu > 4);

	switch(choix_menu)
	{
		case 1:
			if(joueur->pt_deplacement == 1)
			{
				printf("Liste des coordonnées disponibles pour le déplacement\n");
				position.x = joueur->pos_x;
				position.y = joueur->pos_y;
				liste_pos_t * positions_valides = deplacement_valide(joueur);
				ajouter_marqueurs_deplacements_carte(positions_valides);
				//afficher_liste_positions_terminal(positions_valides);
				afficher_plateau(VISUEL);
				printf("Choisir les coordonnées d'une case cible pour le déplacements\n");
				printf("Coordonnée x : "); scanf("%i", &choix_x);
				printf("Coordonnée y : "); scanf("%i", &choix_y);

				position.x = choix_x;
				position.y = choix_y;
				if( effectuer_deplacement(joueur, position) == 0 )
				{
					joueur->pt_deplacement = 0;
					terminal_afficher_direction_perso(joueur);
				}
				else printf("La case cible n'était pas valide\n");
				retirer_marqueurs_deplacements_carte(positions_valides);
			}
			else
			{
				printf("Points de déplacements insuffisants \n");
			}

			break;
		case 2:
			//SI LES POINTS D'ATTAQUES SONT SUFFISANT
			if(joueur->pt_attaque == 1)
			{
				//Choix des attaques
				printf("\n	1. Effectuer l'attaque 1 : \n"); //Afficher description avec infoatt
				afficher_description_attaque(joueur->info_comp1);
				printf("\n	2. Effectuer l'attaque 2 :\n");
				afficher_description_attaque(joueur->info_comp2);
				if(joueur->comp_special == true) //La compétence spéciale n'a pas encore été utilisée
				{
					printf("\n	3. Effectuer la competence spéciale\n");
					afficher_description_attaque(joueur->info_comp3);
				}

				printf("Attaque à effectuer :\n");
				scanf("%i", &choix_menu);

				//Récupération des informations de l'attaque choisie
				info_att info_attaque;
				switch(choix_menu)
				{
					case 1:
						info_attaque = joueur->info_comp1();
						break;
					case 2:
						info_attaque = joueur->info_comp2();
						break;
					case 3:
						if(joueur->comp_special == true) //La compétence spéciale n'a pas encore été utilisée
						{
							info_attaque = joueur->info_comp3();
						}
						else
						{
							return;
						}
						break;
					default:
						return;
						break;
				}
				int portee_attaque = info_attaque.portee;


				//Controle de validité de l'attaque
				liste_pos_t * positions_valides = attaque_valide(joueur, portee_attaque);
				ajouter_marqueurs_attaque_carte(positions_valides);
				afficher_plateau(VISUEL);
				int demanderCoordonnees = 1;
				do
				{
					//Choisir une case cible
					printf("Choisir les coordonnées d'une case cible pour l'attaque\n");
					printf("Coordonnée x : "); scanf("%i", &choix_x);
					printf("Coordonnée y : "); scanf("%i", &choix_y);
					position.x = choix_x;
					position.y = choix_y;
					demanderCoordonnees = controler_coordonnees_attaque(position, positions_valides);

				}while(demanderCoordonnees == 1);
				retirer_marqueurs_attaque_carte(positions_valides);
				liste_positions_detruire( &positions_valides );


				//Effectuer l'attaque
				statut_att nouvelle_attaque;
				switch(choix_menu)
				{
					case 1: //Attaque 1
						nouvelle_attaque = joueur->competence1(joueur, position);
						break;
					case 2: //Attaque 2
						nouvelle_attaque = joueur->competence2(joueur, position);
						break;
					case 3: //Compétence spéciale
						nouvelle_attaque = joueur->competence3(joueur, position);
						break;
				}
				afficher_statut_attaque(nouvelle_attaque);
				joueur->pt_attaque = 0;
			}


			break;
		case 3:
			printf("Le joueur passe son tour\n");
			*tour_pas_fini = 0;
			break;
		case 4:
			printf("Le joueur abandonne\n");
			*tour_pas_fini = 0;

			joueur->etat = Mort;
			int x = joueur->pos_x;
			int y = joueur->pos_y;
			map[x][y].type_emplacement = VIDE;
			map[x][y].perso = NULL;
			personnage_MAJ_equipe_mort(joueur);

			break;
	}
}


void terminal_afficher_direction_perso(personnage_t * perso)
{
	printf("Perso %s : [%i,%i] ", perso->nom, perso->pos_x, perso->pos_y);
	switch(perso->direction)
	{
		case haut: printf("Regarde en haut"); break;
		case bas: printf("Regarde en bas"); break;
		case droite: printf("Regarde à droite"); break;
		case gauche: printf("Regarde à gauche"); break;
		default: /* Aucune action */ break;
	}
	printf("\n");
}

void terminal_afficher_direction_persos(liste_perso_t * equipe_rouge, liste_perso_t * equipe_bleue)
{
	int i;

	printf("Afficher nb_pers rouge : %i, Afficher nb_pers rouge : %i\n", equipe_rouge->nb, equipe_bleue->nb );

	printf("Equipe Bleue\n");
	for(i = 0; i < equipe_bleue->nb; i++)
	{
		personnage_t * perso = equipe_bleue->liste[i];
		terminal_afficher_direction_perso(perso);
	}

	printf("Equipe Rouge\n");
	for(i = 0; i < equipe_rouge->nb; i++)
	{

		personnage_t * perso = equipe_rouge->liste[i];
		terminal_afficher_direction_perso(perso);
	}
}
