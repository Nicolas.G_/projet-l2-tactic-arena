
/**
 * \file test_map.c
 * \brief Fichier pour tester les combats
 * \author 
 * \version 1.0
 * \date 
 *
 * Permet de s'assurer que les fonction charger la map fonctionne
 *
 */


#include "../include/map.h"




/**
 * \fn int main(void)
 * \brief fonction main
 * 
 * \param self
 * 
 * \return int 0
 * 
 */
int main()
{
	srand (time (NULL));
	int choix;
	printf("Test d'affichage de la carte\n");
	
	do
	{
		printf("Mode d'affichage de la carte : Binaire (0) - Visuel (1) : ");
		scanf("%i", &choix);
	}while(choix < 0 || choix > 1);
	
	if(	chargement_carte() == 0 )
	{
		if(choix == 1)
		{
			afficher_plateau(VISUEL);
		}	
		else
		{
			afficher_plateau(BINAIRE);
		}
	}
	return 0;
}