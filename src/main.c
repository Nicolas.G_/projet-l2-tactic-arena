/**
 * \file main.c
 * \brief main
 * \author Pierre Garçon, Evan Rebour, Nicolas Gouget, Yohann Delacroix
 * \version 0.1
 * \date 26 Janvier 2021
 *
 * Main appelle le menu
 *
 */

#include <stdio.h>
#include "../include/menu_terminale.h"


/**
 * \fn int main(void)
 * \brief fonction main
 * 
 * \param self
 * 
 * \return int 0
 * 
 */
int main(){
    menu_terminale();
    return 0;
}
