#include "../include/attaques.h"
#include "../include/map.h"
#include "../include/malus.h"
#include "../include/liste_malus.h"
#include "../include/liste_positions.h"
#include "../include/deplacements.h"
#include "../include/lignedevue.h"

/**
 * \file fonction_attaques.c
 * \brief Fonctions et mécanismes de controles de validité des attaques
 * \author Yohann Delacroix
 * \version 1.0
 * \date 18 Février 2021
 */



/**
 * \fn int cible_corps_a_corps(t_pos joueur, t_pos cible)
 * \brief Détermine si deux positions sont adjacentes
 * \param joueur Position du premier personnage
 * \param cible Position du deuxième personnage
 * \return Retourne 0 si la cible est au corps à corps, sinon 1
 */
int cible_corps_a_corps(t_pos joueur, t_pos cible)
{
	if(joueur.x + 1 == cible.x || joueur.x - 1 == cible.x || joueur.y - 1 == cible.y || joueur.y + 1 == cible.y) return 0;
	return 1;
}

//Controle si les coordonnées données sont valides pour l'attaque selon la pos


/**
 * \fn int controler_coordonnees_attaque(t_pos case_cible, liste_pos_t * liste_de_positions) 
 * \brief Controle si la position renseignée fait bien partie de la liste des positions atteignables
 * \param case_cible Position de la case ciblée
 * \param liste_de_positions Liste de positions
 * \return Retourne 0 si les coordonnées sont valides, sinon 1
 */
int controler_coordonnees_attaque(t_pos case_cible, liste_pos_t * liste_de_positions) 
{
	int i;	
	t_pos position_possible;	

	for(i = 0; i < liste_de_positions->nb_elem; i++)
	{
		position_possible = liste_position_lire(liste_de_positions, i);

		if(position_possible.x == case_cible.x && position_possible.y == case_cible.y)//Si la position est valide
		{	
			return 0;			
		}
	}

	return 1;
}


/**
 * \fn int compter_cases_valides_attaque(int taille_zone, t_pos case_cible)
 * \brief Détermine le nombre de cases atteignables pour une attaque
 * \param taille_zone Portée de l'attaque
 * \param case_cible Position du personnage
 * \return Integer représentant le nombre de cases atteignables
 */
int compter_cases_valides_attaque(int taille_zone, t_pos case_cible)
{
	int cov_y;
	int seek_x;
	int seek_y;
	int x, y;
	int nombre_de_cases_possibles = 0;
	for(x = -taille_zone; x <= taille_zone; x++)
	{
		cov_y = taille_zone - abs(x);
		for(y = -cov_y; y <= cov_y ; y++)
		{
			seek_x = case_cible.x - x;
			seek_y = case_cible.y - y;

			if(seek_x >= 0 && seek_x < N && seek_y >=0 && seek_y < M)//Controler si les cases ne sont pas hors de la map
			{
				if(map[seek_x][seek_y].type_emplacement == VIDE || map[seek_x][seek_y].type_emplacement == PERSO)
				{
					nombre_de_cases_possibles++;
				}
			}
		}
	}
	return nombre_de_cases_possibles;
}


/**
 * \fn int compter_cases_obstacles_attaque(int taille_zone, t_pos case_cible)
 * \brief Détermine le nombre d'obstacles dans la portée d'attaque du joueur
 * \param taille_zone Portée de l'attaque
 * \param case_cible Position du personnage
 * \return Integer représentant le nombre d'obstacles
 */
int compter_cases_obstacles_attaque(int taille_zone, t_pos case_cible)
{
	int cov_y;
	int seek_x;
	int seek_y;
	int x, y;
	int nombre_de_cases_possibles = 0;
	for(x = -taille_zone; x <= taille_zone; x++)
	{
		cov_y = taille_zone - abs(x);
		for(y = -cov_y; y <= cov_y ; y++)
		{
			seek_x = case_cible.x - x;
			seek_y = case_cible.y - y;

			if(seek_x >= 0 && seek_x < N && seek_y >=0 && seek_y < M)//Controler si les cases ne sont pas hors de la map
			{
				if(map[seek_x][seek_y].type_emplacement == MUR || map[seek_x][seek_y].type_emplacement == PERSO)
				{
					nombre_de_cases_possibles++;
				}
			}
		}
	}
	return nombre_de_cases_possibles;
}


/**
 * \fn liste_pos_t* attaque_valide(personnage_t * joueur, int portee_attaque)
 * \brief Recherche les cases atteignables pour une attaque et les insere dans une liste
 * \param joueur Joueur qui demande un déplacement
 * \param portee_attaque Portée de l'attaque
 * \return pointeur sur liste_pos_t contenant les positions atteignables
 *
 * Utilise les méthodes de ligne de vue pour que l'attaque ne puisse pas être portée derrière les murs
 *
 */
liste_pos_t* attaque_valide(personnage_t * joueur, int portee_attaque)
{
	//Initialisations
	liste_pos_t * liste_de_positions = NULL;
	liste_pos_t * positions_obstacles = NULL;
	
	int taille_zone = portee_attaque;    //Portée de l'attaque
	t_pos position_joueur = {joueur->pos_x, joueur->pos_y};
	t_pos nouvelle_position;	
	

	//Recherche du nombre de cases ciblables pour l'allocation mémoire
	int nb_positions = compter_cases_valides_attaque(taille_zone, position_joueur);
	liste_de_positions = liste_positions_initialiser(nb_positions);
	
	int nb_obstacles = compter_cases_obstacles_attaque(taille_zone, position_joueur);
	positions_obstacles = liste_positions_initialiser(nb_obstacles);

	//Remplissage de la liste
	int cov_y;
	int seek_x;
	int seek_y;
	int x, y;
	int indice = 0;
	int indice_obstacle = 0;
	for(x = -taille_zone; x <= taille_zone; x++)
	{
		cov_y = taille_zone - abs(x);
		for(y = -cov_y; y <= cov_y ; y++)
		{
			seek_x = position_joueur.x - x;
			seek_y = position_joueur.y - y;

			if(seek_x >= 0 && seek_x < N && seek_y >=0 && seek_y < M)//Controler si les cases ne sont pas hors de la map
			{
				//Remplissage Liste des positions valides
				if(map[seek_x][seek_y].type_emplacement == VIDE || map[seek_x][seek_y].type_emplacement == PERSO)
				{	
					nouvelle_position.x = seek_x;
					nouvelle_position.y = seek_y;
					liste_positions_ajouter( liste_de_positions, nouvelle_position, indice );

					

					indice++;
				}
				
				//Remplissage Liste des obstacles
				if(map[seek_x][seek_y].type_emplacement == MUR || map[seek_x][seek_y].type_emplacement == PERSO)
				{
					nouvelle_position.x = seek_x;
					nouvelle_position.y = seek_y;
					liste_positions_ajouter( positions_obstacles, nouvelle_position, indice_obstacle );					
					indice_obstacle++;
				}
			}
		}
	}

	//Retirer de la liste les positions masquée par les murs
	int i,j;
	for(i = 0 ; i < taille_liste(positions_obstacles); i++)
	{
		liste_pos_t * positions_inatteignables = ligne_de_vue(position_joueur, positions_obstacles->positions[i], portee_attaque);
		for(j = 0; j < taille_liste(positions_inatteignables); j++)
		{
			t_pos pos_tester = positions_inatteignables->positions[j];
			if(map[pos_tester.x][pos_tester.y].type_emplacement != PERSO)
			{
				liste_de_positions = retirer_une_occurence_liste(liste_de_positions, positions_inatteignables->positions[j]);
				
			}
		}
		liste_positions_detruire(&positions_inatteignables);
	}
	
	liste_positions_detruire(&positions_obstacles);
	return liste_de_positions;
}



/**
 * \fn void ajouter_marqueurs_attaque_carte(liste_pos_t * liste_positions)
 * \brief Ajoute des marqueurs sur la carte qui montrent les cases atteignables pour une attaque
 * \param liste_positions Liste qui contient des positions atteignables
 * \return Aucune valeur de retour
 * Remplit le champ po_attaque de la structure t_carte par la valeur 1
 */
void ajouter_marqueurs_attaque_carte(liste_pos_t * liste_positions)
{
	int i;
	t_pos position;
	for(i = 0; i < liste_positions->nb_elem; i++)
	{
		position = liste_position_lire(liste_positions, i);
		map[position.x][position.y].po_attaque = 1;
	}
}

/**
 * \fn void retirer_marqueurs_attaque_carte(liste_pos_t * liste_positions)
 * \brief Retrait des marqueurs d'attaques sur la carte
 * \param liste_positions Liste qui contient des positions atteignables
 * \return Aucune valeur de retour
 * Remplit le champ po_attaque de la structure t_carte par la valeur 0
 */
void retirer_marqueurs_attaque_carte(liste_pos_t * liste_positions)
{
	int i;
	t_pos position;
	for(i = 0; i < liste_positions->nb_elem; i++)
	{
		position = liste_position_lire(liste_positions, i);
		map[position.x][position.y].po_attaque = 0;
	}
}


//Attaques des archers
/**
 * \fn statut_att tirdelaigle(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque tir de l'aigle
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att tirdelaigle(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Tir de l'aigle", 1, 10, 0, Attaque);

	return attaque;
}

/**
 * \fn statut_att tirdecombat(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque tir de combat
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att tirdecombat(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Tir de Combat", 15, 20, 0, Attaque);

	return attaque;
}

/**
 * \fn statut_att barrageabsolu(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque barrage absolue
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att barrageabsolu(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Barrage Absolu", 30, 30, 4, Attaque);

	lanceur->comp_special = false;
	return attaque;
}

//Attaques des Assassins
/**
 * \fn statut_att coupdeDague(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque coup de dague
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att coupdeDague(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Coup de dague", 10, 20, 0, Attaque);

	return attaque;
}

/**
 * \fn statut_att lancerdedagues(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque lancer de dagues
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att lancerdedagues(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Lancer de dagues", 5, 8, 1, Attaque);

	return attaque;
}

/**
 * \fn statut_att beniparlesdieuxdusang(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque beni par les dieux du sang
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att beniparlesdieuxdusang(personnage_t * lanceur, t_pos case_cible)
{

	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Béni par les Dieux du sang", 40, 40, 0, Attaque);


	liste_malus_ajouter(deplacement, 3, lanceur, -2);//MALUS
	liste_malus_ajouter(degats, 3, lanceur, -5);//MALUS

	lanceur->comp_special = false;
	return attaque;
}


//Attaques des mages
/**
 * \fn statut_att firaball(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque firaball
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att firaball(personnage_t * lanceur, t_pos case_cible)
{
	
	int min = 1;
	int max = 10;
	int random = rand()%(max-min+1)+min;
	statut_att attaque;
	if(random == 1)
	{
		attaque = effectuer_attaque(lanceur, case_cible, "Firaball", 20, 25, 0, Attaque);
	}
	else
	{	
		attaque = effectuer_attaque(lanceur, case_cible, "Firaball", 10, 15, 0, Attaque);
	}
	return attaque;
}

/**
 * \fn statut_att fabriquantdeglacons(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque fabriquant de glaçons
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att fabriquantdeglacons(personnage_t * lanceur, t_pos case_cible)
{
	t_pos pos_lanceur;
	pos_lanceur.x = lanceur->pos_x;
	pos_lanceur.y = lanceur->pos_y;
	statut_att attaque;
	if( cible_corps_a_corps(pos_lanceur, case_cible) )
	{
		attaque = effectuer_attaque(lanceur, case_cible, "Fabriquant de glaçons", 25, 30, 0, Attaque);
	}
	else
	{
		attaque = effectuer_attaque(lanceur, case_cible, "Fabriquant de glaçons", 5, 10, 0, Attaque);
	}
	return attaque;
}

/**
 * \fn statut_att interitumnotitia(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Interitum Notitia
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att interitumnotitia(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Interitum Notitia", 40, 40, 0, Attaque);

	lanceur->comp_special = false;
	return attaque;
}

//Attaques des healers
/**
 * \fn statut_att benedictiondivine(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Bénédiction divine
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att benedictiondivine(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Bénédiction Divine", 10, 15, 0, Soin);

	return attaque;
}

/**
 * \fn statut_att transfertdevitalite(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Transfert de vitalité
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att transfertdevitalite(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Transfert de vitalité", 5, 30, 0, Soin);

	if(attaque.etat == Reussite)
	{
		lanceur->pv = lanceur->pv - attaque.degats[0] / 2;
		attaque.degats_sur_lanceur = attaque.degats[0] / 2;
	}
	return attaque;
}

/**
 * \fn statut_att gracedeladeesse(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Grace de la déesse
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att gracedeladeesse(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Grâce de la déesse", 30, 40, 3, Soin);

	if(attaque.etat == Reussite)
	{
		lanceur->pv = lanceur->pv + attaque.degats[0] ;
		attaque.soins_sur_lanceur = attaque.degats[0];
	}
	lanceur->comp_special = false;
	return attaque;
}

//Attaque des Tanks
/**
 * \fn statut_att coupepeesimple(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Coup d'épée simple
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att coupepeesimple(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Coup d'épée simple", 10, 15, 0, Attaque);

	return attaque;
}

/**
 * \fn statut_att coupdebouclierrecul(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Coup de bouclier recul
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att coupdebouclierrecul(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Coup de bouclier recul", 5, 10, 0, Attaque);
	liste_malus_ajouter(deplacement, 2, attaque.cibles[0], -2);//MALUS
	return attaque;
}

/**
 * \fn statut_att volontedudefenseur(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Volonté du défenseur
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att volontedudefenseur(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Volonté du défenseur", 0, 0, 0, NoDamage);
	//ENVOUTEMENT

	liste_malus_ajouter(bouclier, 3, lanceur, 30);//BONUS
	liste_malus_ajouter(degats, 3, lanceur, 20);//BONUS

	lanceur->comp_special = false;
	return attaque;
}

//Attaque des épéistes
/**
 * \fn statut_att voleedepee(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Volee d'épée
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att voleedepee(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Volée d'épée", 15, 20, 2, Attaque);

	return attaque;
}

/**
 * \fn statut_att ragedudueliste(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Rage du duéliste
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att ragedudueliste(personnage_t * lanceur, t_pos case_cible)
{
	int min = 1;
	int max = 10;
	int random = rand()%(max-min+1)+min;
	statut_att attaque;
	if(random < 3)
	{
		attaque = effectuer_attaque(lanceur, case_cible, "Rage du duéliste", 35, 35, 0, Attaque);
	}
	else
	{
		attaque = effectuer_attaque(lanceur, case_cible, "Rage du duéliste", 20, 30, 0, Attaque);
	}
	return attaque;
}

/**
 * \fn statut_att dansedelamort(personnage_t * lanceur, t_pos case_cible)
 * \brief fonction qui lance l'attaque Danse de la mort
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquel on lance l'attaque
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att dansedelamort(personnage_t * lanceur, t_pos case_cible)
{
	statut_att attaque = effectuer_attaque(lanceur, case_cible, "Danse de la mort", 100, 100, 0, Attaque);

	lanceur->comp_special = false;
	return attaque;
}



/**
 * \fn statut_att effectuer_attaque(personnage_t * lanceur, t_pos case_cible, char * nom_attaque, int dommage_minimum, int dommage_maximum, int zone, type_attaque type)
 * \brief Effectue une attaque sur une case_cible
 *
 * \param lanceur Personnage qui lance l'attaque
 * \param case_cible Case de la map sur laquelle on lance l'attaque
 * \param nom_attaque Nom de l'attaque à lancer
 * \param dommage_minimum Degat minimum de l'attaque
 * \param dommage_maximum Degat maximum de l'attaque
 * \param zone Zone d'impact affectée par l'attaque
 * \param type Type de l'attaque effectuée
 *
 * \return Structure de données statut_att contenant le bilan final de l'attaque
 *
 */
statut_att effectuer_attaque(personnage_t * lanceur, t_pos case_cible, char * nom_attaque, int dommage_minimum, int dommage_maximum, int zone, type_attaque type)
{
	statut_att attaque;
	attaque.lanceur = lanceur;
	attaque.degats_sur_lanceur = 0;
	attaque.soins_sur_lanceur = 0;
	attaque.nom_attaque = nom_attaque;


	attaque.nombre_cibles = 0;

	/* Calcul des esquives parades ou échec */
	if(echec_attaque() == 1)
	{
		attaque.etat = Echec;
		return attaque;
	}
	attaque.etat = Reussite;


	int i = 0;
	int dommage_total = 0;
	int dommage_finaux = 0;
	int soins_total = 0;
	/* Recherche des personnages ciblés */
	personnage_t ** cibles = recherche_cibles_zone(case_cible, zone);

	for(i = 0; i < NB_CASES_TABLEAU_CIBLES; i++)
	{
		if(cibles[i] != NULL)
		{
			attaque.attaque_esquivee[i] = 0;
			attaque.etat_cible[i] = Vivant;
			if(esquiver_attaque(cibles[i]) == 1 && type == Attaque)
			{
				attaque.attaque_esquivee[i] = 1;
				attaque.degats[i] = 0;
				attaque.cibles[i] = cibles[i];
			}
			else
			{

				if(type == Attaque)
				{
					/* Calcul des dommages */
					dommage_total = tirage_jet_dommage(dommage_minimum, dommage_maximum);
					//Application des résistances
					dommage_finaux = dommage_total - cibles[i]->resistance;
					dommage_finaux = (int)(dommage_finaux * SystemePFC(lanceur, cibles[i]));

					//Application des dommages
					cibles[i]->pv = (cibles[i]->pv) - dommage_total;

					if(cibles[i]->pv <= 0)//La cible est elle morte ?
					{
						cibles[i]->pv = 0;
						cibles[i]->etat = Mort;
						int x = cibles[i]->pos_x;
						int y = cibles[i]->pos_y;
						map[x][y].type_emplacement = VIDE;
						map[x][y].perso = NULL;
						personnage_MAJ_equipe_mort(cibles[i]);
						attaque.etat_cible[i] = Mort;
					}


					attaque.degats[i] = dommage_finaux;
					attaque.cibles[i] = cibles[i];
					attaque.type[i] = type;

				}
				else if(type == Soin)
				{
					/* Calcul des soins */
					soins_total = tirage_jet_dommage(dommage_minimum, dommage_maximum);
					//Application des dommages
					if(cibles[i]->etat == Vivant)//Sécurité pour ne pas soigner un personnage mort
					{
						if( (cibles[i]->pv) + soins_total > cibles[i]->pv_max )
						{
							attaque.degats[i] = cibles[i]->pv_max - cibles[i]->pv;
							cibles[i]->pv = cibles[i]->pv_max;
						}
						else 
						{
							cibles[i]->pv = (cibles[i]->pv) + soins_total;
							attaque.degats[i] = soins_total;
						}
					}
					
					
					attaque.cibles[i] = cibles[i];
					attaque.type[i] = type;
				}
				else if(type == NoDamage)
				{
					attaque.cibles[i] = cibles[i];
					attaque.type[i] = type;
				}

			}
			attaque.nombre_cibles++;
		}
		else
		{
			attaque.cibles[i] = NULL;
			attaque.attaque_esquivee[i] = -1;
			attaque.degats[i] = -1;
			attaque.type[i] = -1;
		}
	}

	free(cibles);
	cibles = NULL;

	if(attaque.nombre_cibles == 0)
	{
		attaque.etat = SansCible;
	}

	return attaque;
}


/**
 * \fn int tirage_jet_dommage(int min, int max)
 * \brief fonction qui effectue un jet aléatoir déterminant les dégat de l'attaque
 *
 * \param min Minimum de dégat infliger par l'attaque
 * \param max Maximum de dégat infliger par l'attaque
 *
 * \return entier représentant le jet de dommages
 *
 */
int tirage_jet_dommage(int min, int max)
{
	return rand()%(max-min+1)+min;
}

/**
 * \fn int echec_attaque(void)
 * \brief fonction qui test si l'attaque echoue à faire des dégats
 *
 * \param self
 *
 * \return Aucune valeur de retour
 *
 */
int echec_attaque()
{
	int echec = rand()%(100-0+1)+0; //L'attaque a 1 chance sur 100 d'échouer
	return (echec == 1);
}

/**
 * \fn int esquiver_attaque(personnage_t * cible)
 * \brief fonction qui test si l'adversaire esquive l'attaque
 *
 * \param cible Cible de l'attaque à esquiver
 *
 * \return Integer code Erreur 1, 0 tout s'est bien passé
 *
 */
int esquiver_attaque(personnage_t * cible)
{
	float taux_esquive = cible->esquive;
	float tirage_esquive = rand()%(100-0+1)+0; //Tirer un nombre aléatoire entre 0 et 100
	if(tirage_esquive <= taux_esquive*100)
	{
		return 1;
	}
	return 0;
}

/**
 * \fn int afficher_statut_attaque(statut_att statut_attaque)
 * \brief Affiche le résultat d'une attaque
 *
 * \param statut_attaque Informations sur le résultat de l'attaque
 *
 * \return Integer code Erreur 1, 0 tout s'est bien passé
 *
 */
int afficher_statut_attaque(statut_att statut_attaque) //FONCTION DE TEST
{
	if(statut_attaque.lanceur->nom != NULL)
	{
		printf("\n	%s lance une attaque ! ", statut_attaque.lanceur->nom);
	}
	else 
	{
		printf("\n	%p lance une attaque ! ", statut_attaque.lanceur);
	}

	printf("Nom de l'attaque : %s - \n", statut_attaque.nom_attaque);
	int i;
	switch(statut_attaque.etat)
	{
		case Reussite:

			for(i = 0; i < statut_attaque.nombre_cibles; i++)
			{
				if(statut_attaque.attaque_esquivee[i] != 1)
				{
					if(statut_attaque.type[i] == Attaque)
					{
						printf("Attaque réussie avec succès sur le joueur ");
						if(statut_attaque.cibles[i]->nom != NULL)
						{
							printf("%s", statut_attaque.cibles[i]->nom);
						}
						else
						{
							printf("%p", statut_attaque.cibles[i]);
						}
						printf(" - %i PV", statut_attaque.degats[i]);
						if(statut_attaque.etat_cible[i] == Mort)
						{
							printf("%s est mort.", statut_attaque.cibles[i]->nom);
						}
						printf("\n");
					}
					else if(statut_attaque.type[i] == Soin)
					{
						printf("Soin réussi avec succès sur le joueur ");
						if(statut_attaque.cibles[i]->nom != NULL)
						{
							printf("%s", statut_attaque.cibles[i]->nom);
						}
						else
						{
							printf("%p", statut_attaque.cibles[i]);
						}
						printf(" + %i PV", statut_attaque.degats[i]);
						printf("\n");
					}
					else if(statut_attaque.type[i] == NoDamage)
					{
						printf("Sortilège réussi avec succès sur le joueur ");
						if(statut_attaque.cibles[i]->nom != NULL)
						{
							printf("%s", statut_attaque.cibles[i]->nom);
						}
						else
						{
							printf("%p", statut_attaque.cibles[i]);
						}
						printf("\n");
					}
				}
				else
				{
					printf("Attaque ésquivée par le joueur ");
					if(statut_attaque.cibles[i]->nom != NULL)
					{
						printf("%s", statut_attaque.cibles[i]->nom);
					}
					else
					{
						printf("%p", statut_attaque.cibles[i]);
					}
					printf("\n");
				}
			}
			if(statut_attaque.degats_sur_lanceur > 0)
			{
				printf("Le lanceur perd %i pv\n", statut_attaque.degats_sur_lanceur);
			}
			if(statut_attaque.soins_sur_lanceur > 0)
			{
				printf("Le lanceur gagne %i pv\n", statut_attaque.soins_sur_lanceur);
			}
			break;
		case Echec:
			printf("Echec de l'attaque !");
			break;
		case SansCible:
			printf("L'attaque n'a touché personne ");
			break;
	}
	printf("\n");
	return 0;
}


/**
 * \fn personnage_t** recherche_cibles_zone(t_pos case_cible, int taille_zone)
 * \brief Recherche des cibles présentes dans une zone de taille donnée
 *
 * \param case_cible Coordonnées de la case où l'attaque est lancée
 * \param taille_zone Taille de la zone  ou l'attaque peux faire des degats
 *
 * \return liste de pointeurs sur personnage_t présents dans la zone
 *
 */
personnage_t** recherche_cibles_zone(t_pos case_cible, int taille_zone)
{

	personnage_t ** cibles;
	cibles = malloc(sizeof(personnage_t*) * NB_CASES_TABLEAU_CIBLES);
	int z;
	for(z = 0; z < NB_CASES_TABLEAU_CIBLES; z++)
	{
		cibles[z] = NULL;
	}

	int indice_t_cibles = 0;
	int cov_y;
	int seek_x;
	int seek_y;
	int x, y;
	/* Algorithme de recherche des cases comprises dans la zone */
	for(x = -taille_zone; x <= taille_zone; x++)
	{
		cov_y = taille_zone - abs(x);
		for(y = -cov_y; y <= cov_y ; y++)
		{
			seek_x = case_cible.x - x;
			seek_y = case_cible.y - y;
			//printf("Recherche sur la case [%i,%i]\n", seek_x, seek_y);

			if(seek_x >= 0 && seek_x < N && seek_y >=0 && seek_y < M)
			{
				if(map[seek_x][seek_y].perso != NULL)
				{
					cibles[indice_t_cibles] = map[seek_x][seek_y].perso;
					indice_t_cibles++;
				}
				else
				{
					cibles[indice_t_cibles] = NULL;
				}
			}
		}
	}

	/*
		//SEQUENCE DE TEST DE LA FONCTION
	printf("LISTE DES PERSONNAGES - AFFICHAGE : \n");
	for(int i = 0; i < NB_CASES_TABLEAU_CIBLES; i++)
	{
		if(cibles[i] != NULL)
		{
			printf("	CIBLE : %p \n", cibles[i]);
		}
		else
		{
			printf("	PAS DE CIBLE\n");
		}
	}

	*/

	return cibles;
}

/**
 * \fn int comparaison_position(int p1x, int p1y, int p2x, int p2y)
 * \brief Compare la proximité de deux positions
 * \param p1x Abscisse position 1
 * \param p1y Ordonnée position 1
 * \param p2x Abscisse position 2
 * \param p2y Ordonnée position 2
 * \return Entier [0: La proximité est égale] [1: La position 1 est plus proche que la 2] [-1: La positions 2 est plus proche que la 1]
 */
int comparaison_position(int p1x, int p1y, int p2x, int p2y)
{
	int chemin1 = abs(p1x) + abs(p1y);
	int chemin2 = abs(p2x) + abs(p2y);

	if(chemin1 == chemin2) return 0;
	else if(chemin1>chemin2) return 1;
	else return -1;
}


/*  */

/**
 * \fn personnage_t** trouver_la_cible_la_plus_proche(t_pos case_cible, int taille_zone)
 * \brief Trie une liste de personnage pour trouver la cible la plus proche
 * \param case_cible Abscisse position 1
 * \param taille_zone taille de la zone de l'attaque
 * \return Entier [0: La proximité est égale] [1: La position 1 est plus proche que la 2] [-1: La positions 2 est plus proche que la 1]
 */
personnage_t** trouver_la_cible_la_plus_proche(t_pos case_cible, int taille_zone)
{
	personnage_t** liste_perso = recherche_cibles_zone(case_cible, taille_zone);


	int nb_elem_liste = 0;
	int z;
	for(z = 0; z < NB_CASES_TABLEAU_CIBLES; z++)
	{
		if(liste_perso[z] != NULL)
		{
			nb_elem_liste++;
		}
	}


	//TRI DE LA LISTE
	int i;
	int continuer = 1;
	personnage_t * save;
	while(continuer)
	{
		continuer = 0;
		for(i = 0; i < nb_elem_liste - 1; i++)
		{
			if(comparaison_position( liste_perso[i]->pos_x, liste_perso[i]->pos_y,  liste_perso[i+1]->pos_x, liste_perso[i+1]->pos_y ) == -1)
			{
				//PERMUTATION
				continuer = 1;
				save = liste_perso[i];
				liste_perso[i] = liste_perso[i+1];
				liste_perso[i+1] = save;
			}
		}
	}

	return liste_perso;
}

/**
 * \fn float SystemePFC(personnage_t * perso1, personnage_t * perso2)
 * \brief Calcul un coefficient d'augmentation ou de diminution en fonction des classes de deux personnages
 *
 * \param perso1 Perssonnage sur lequel est basé la comparaison
 * \param perso2 Personnage Adverse
 *
 * \return float 0.80 | 1 | 1.20 Coefficien de modification des dégâts
 *
 */
float SystemePFC(personnage_t * perso1, personnage_t * perso2)
{
	if (perso1->classe==archer)
	{
		if(perso2->classe==assassin)
			return 0.80;
		else if (perso2->classe==epeiste)
		{
			return 1.20;
		}
		else
			return 1;
	}
	else if (perso1->classe==mage)
	{
		if(perso2->classe==epeiste)
		    return 0.80;
		else if (perso2->classe==tank)
		{
		    return 1.20;
		}
		else
		    return 1;
	}
	else if (perso1->classe==tank)
	{
		if(perso2->classe==mage)
		    return 0.80;
		else if (perso2->classe==assassin)
		{
		    return 1.20;
		}
		else
		    return 1;
	}

	else if (perso1->classe==assassin)
	{
		if(perso2->classe==tank)
		    return 0.80;
		else if (perso2->classe==archer)
		{
		    return 1.20;
		}
		else
		    return 1;
	}


	else if (perso1->classe==healer)
	{
		return 1;
	}

	else if (perso1->classe==epeiste)
	{
		if(perso2->classe==archer)
		    return 0.80;
		else if (perso2->classe==mage)
		{
		    return 1.20;
		}
		else
		    return 1;
	}
	else //Cas d'erreur
	{
		return -1;
	}
}

/**
 * \fn void afficher_description_attaque(info_att (*attaque)())
 * \brief Affiche la description d'une attaque en version terminale
 *
 * \param attaque Une fonction du dictionnaire d'attaque

 *
 * \return Aucune valeur de retour
 *
 */
void afficher_description_attaque(info_att (*attaque)())
{
	info_att info = attaque();
	printf("\n\n---DESCRIPTION DE L'ATTAQUE---\n\n");
	printf("Nom : %s\n", info.nom);
	printf("Zone : %i\n", info.zone);
	printf("Portée : %i\n", info.portee);
	switch(info.type)
	{
		case Attaque: printf("Type : Attaque"); break;
		case Soin: printf("Type : Soin"); break;
		case NoDamage: printf("Type : Sortilège"); break;
	}
	printf("\n");
	printf("Degats minimum : %i\n", info.degats_min);
	printf("Degats maximum : %i\n", info.degats_max);
	printf("Description : %s\n", info.description);
}