/**
 * \file menu.c
 * \brief Fichier qui affiche les menue
 * \author Evan Rebours, Nicolas G
 * \version 1.0
 * \date 11 Mars 2021
 *
 * Affiche le menue et les option séléctionnable menant vers les differente partie du jeu
 *
 */

#include "../include/menu.h"

/*
> GNU/Linux, MacOs : gcc menu.c $(sdl2-config --cflags --libs) -o prog
> Windows          : gcc src/menu.c -o bin/menu -I include -L lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_image
> Windows          : gcc src/menu.c -o bin/menu -I include -L lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -mwindows --> pareil mais sans terminal
*/

/**
 * \fn int menu (void)
 * \brief Affichage du menu.
 *
 * \return EXIT_SUCCESS - Arrêt normal du programme.
 */
int menu(){
    /**_*/
    int intervalleEcran=0;/* Flag qui Sert a determiner si on est sur le rectangle de Selection Ecran Partager */
    int intervalleReseaux=0;/* Flag qui Sert a determiner si on est sur le rectangle de Selection de jeux en Resaux */
    int intervalleQuitter=0;/* Flag qui Sert a determiner si on est sur le rectangle de Selection pour Quitter */

    SDL_Window *window = NULL;/* Fenetre dans laquel on affiche le jeu */
    SDL_Renderer *renderer = NULL;/*  Rendu moteur de rendu de fenetre */
    SDL_Surface *image = NULL;/* Surface pour afficher le fond */
    SDL_Surface *curseur = NULL;/* Surface pour afficher les images avec curseurs */
    SDL_Surface *screen = NULL;/* Surface pour recuperer le contenue de la fenetre */
    SDL_Surface *icon = NULL;/* Surface ou sera charger l'icone de la fenetre */
    SDL_Texture *texture = NULL;/* Texture pour placer les events */

    //Initialisation de la SDL
    if(SDL_Init(SDL_INIT_VIDEO)!=0)
        SDLError("Initalisation SDL");  

    //Initialisation de la SDL image
    if(IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG){
        SDLError("Failed initialisation SDL Image");
        return EXIT_FAILURE;
    }

    //Création de la fenêtre et du rendu
    //window = SDL_CreateWindow("Tactics Arena",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,480,600,0);
    if(SDL_CreateWindowAndRenderer(WINDOW_WIDTH_MENU,WINDOW_HEIGHT_MENU,0,&window,&renderer)!=0)
        SDLError("Impossible de creer le rendue et la fenêtre");

    //Test d'erreur de creation de la fenetre
    if(window==NULL)
        SDLError("Creation de la fenetre");

    //Definition du titre de la fenetre
    SDL_SetWindowTitle(window,"Tactis Arena");

    //Chargement et mis en place de l'icone de la fenetre
    icon = IMG_Load("../images/icon.png");
    if(!icon){
        SDLError("Failed to Load Image Icon");
    }
    SDL_SetWindowIcon(window,icon);

    //*******************************************************
    //renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_SOFTWARE);
    //Test d'erreur de creation du rendu
    if(renderer==NULL)
        SDLError("Creation Rendue Echoue");

    //Recuperation de ce qui est afficher
    screen = SDL_GetWindowSurface(window);
    if(!screen){
        SDLError("Failed to Get Window");
    }

    //SDL_RendererClear(renderer);
    //Chargement de l'image de fond
    image=SDL_LoadBMP("../images/menu/Menu.bmp");
    if(image==NULL)
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDLError("Erreur lors du chargement de l'image");
    }

    //Affectation de la texture
    texture=SDL_CreateTextureFromSurface(renderer,image);

    //libération de la surface car plus besoin
    SDL_FreeSurface(image);

    //Test d'erreur de la structure
    if(texture==NULL)
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDLError("Erreur lors de la création de la texture");
    }
    //l'image est chargé en mémoire



    //chargement de la texture
    SDL_Rect rectangle;
    if(SDL_QueryTexture(texture,NULL,NULL,&rectangle.w,&rectangle.h)!=0)
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDLError("Erreur lors de l'affichage de l'image");
    }

    //centrage de l'image
    rectangle.x=(WINDOW_WIDTH_MENU-rectangle.w)/2;
    rectangle.y=(WINDOW_HEIGHT_MENU-rectangle.h)/2;
    //
    //affichage de l'image
    if(SDL_RenderCopy(renderer,texture,NULL, &rectangle)!=0)
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDLError("Erreur lors de l'affichage de l'image");
    }

    SDL_RenderPresent(renderer);


    //
    //Gestion des évènements*********************************
    SDL_bool program_launched=SDL_TRUE;
    SDL_Event event;

    //Définition des intervalles de position des différents onglets du menu
    t_position EcranPartage={164,308,134,155};
    t_position ReseauLocal={162,308,226,243};
    t_position Quitter={196,272,342,358};


    //On execute le programme tant que l'utilisateur ne quitte pas
    while (program_launched)
    {
        while (SDL_PollEvent(&event))
        {


            switch (event.type)
            {
            //Ferme le programme si on appuis sur la croix
            case SDL_QUIT:
                program_launched=SDL_FALSE;
                intervalleEcran=intervalleReseaux=intervalleQuitter=0;
                break;
            //gestion clavier pour quitter touche echap
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        program_launched=SDL_FALSE;
                        intervalleEcran=intervalleReseaux=intervalleQuitter=0;
                        break;

                    default:
                        continue;
                }
            break;
            //
            //Gestion souris

            //Test de détéction des rectangle quand on passe dessus
            case SDL_MOUSEMOTION:
                if(((event.motion.x>=EcranPartage.minX)&&(event.motion.x<=EcranPartage.maxX))&&((event.motion.y>=EcranPartage.minY)&&(event.motion.y<=EcranPartage.maxY)))
                    if(intervalleEcran==0)
                    {
                        intervalleEcran=1;
                        intervalleReseaux=0;
                        intervalleQuitter==0;
                        printf("Ecran partager\n");
                        
                        //Charge l'image avec les curseur quand on est passer sur Ecran Partager
                        curseur = IMG_Load("../images/menu/CursEcran.png");
                        if(!curseur){
                            SDLError("Failed to Load Image");
                        }

                        //Affiche l'image charger sur le fond
                        SDL_BlitSurface(curseur,NULL,screen,NULL);

                        SDL_FreeSurface(curseur);
                        SDL_UpdateWindowSurface(window);                        
                    }

                if(((event.motion.x>=ReseauLocal.minX)&&(event.motion.x<=ReseauLocal.maxX))&&((event.motion.y>=ReseauLocal.minY)&&(event.motion.y<=ReseauLocal.maxY)))
                    if(intervalleReseaux==0)//si la souris se trouve pas déja dans un intervalle
                    {
                        intervalleEcran=0;
                        intervalleReseaux=1;
                        intervalleQuitter==0;
                        printf("Reseaux local\n");

                        //Charge l'image avec les curseur quand on est passer sur Resaux local
                        curseur = IMG_Load("../images/menu/CursResaux.png");
                        if(!curseur){
                            SDLError("Failed to Load Image");
                        }

                        //Affiche l'image charger sur le fond
                        SDL_BlitSurface(curseur,NULL,screen,NULL);

                        SDL_FreeSurface(curseur);
                        SDL_UpdateWindowSurface(window);                        
                    }
                if(((event.motion.x>=Quitter.minX)&&(event.motion.x<=Quitter.maxX))&&((event.motion.y>=Quitter.minY)&&(event.motion.y<=Quitter.maxY)))
                    if(intervalleQuitter==0)//si la souris se trouve pas déja dans un intervalle
                    {
                        intervalleEcran=0;
                        intervalleReseaux=0;
                        intervalleQuitter==1;

                        //Charge l'image avec les curseur quand on est passer sur Quitter
                        curseur = IMG_Load("../images/menu/CursQuit.png");
                        if(!curseur){
                            SDLError("Failed to Load Image");
                        }

                        //Affiche l'image sur le fond
                        SDL_BlitSurface(curseur,NULL,screen,NULL);

                        SDL_FreeSurface(curseur);
                        SDL_UpdateWindowSurface(window);
                    }

            break;
            case SDL_MOUSEBUTTONUP:
                if(((event.button.x>=EcranPartage.minX)&&(event.button.x<=EcranPartage.maxX))&&((event.button.y>=EcranPartage.minY)&&(event.button.y<=EcranPartage.maxY))){
                     //Quand on clique sur ecran partager ...
                    printf("Ecran partager selectionner\n");
                    program_launched=SDL_FALSE;
                    
                }

                if(((event.motion.x>=ReseauLocal.minX)&&(event.motion.x<=ReseauLocal.maxX))&&((event.motion.y>=ReseauLocal.minY)&&(event.motion.y<=ReseauLocal.maxY))){
                    //Quand on clique sur resaux local ...
                    printf("Reseau local Selectionner\n");
                }

                if(((event.motion.x>=Quitter.minX)&&(event.motion.x<=Quitter.maxX))&&((event.motion.y>=Quitter.minY)&&(event.motion.y<=Quitter.maxY))){
                    //Quand on clique sur quitter ferme le programme
                    program_launched=SDL_FALSE;
                    intervalleEcran=intervalleReseaux=intervalleQuitter=0;
                }
                    
            break;

            default:

                continue;
            }
        }

    }


    //*******************************************************
    //*******************************************************
    //Destruction
    SDL_FreeSurface(screen);
    SDL_FreeSurface(icon);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
    //*******************************************************

    if(intervalleEcran)
        selection(Partage);
    else if(intervalleReseaux)
        return EXIT_SUCCESS;//return 0;

    
}