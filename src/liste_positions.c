
#include "../include/liste_positions.h"


/**
 * \file liste_positions.c
 * \brief Fichier qui gere les liste de positions
 * \author Yohann Delacroix
 * \version 1.0
 * \date 22 Mars 2021
 *
 * Divers fonction sur la liste des malus
 *
 */


/**
 * \fn liste_pos_t * liste_positions_initialiser(int nb_elements)
 * \brief Initialise une liste de positions de n élements
 * \param nb_elements Nombre d'éléments de la liste
 * \return pointeur sur une liste de position
 */
extern
liste_pos_t * liste_positions_initialiser(int nb_elements)
{
	liste_pos_t * nouvelle_liste = malloc(sizeof(liste_pos_t));
	if(nouvelle_liste == NULL)//Contrôle d'erreur : la liste est elle bien définie ?
	{
		fprintf(stderr, "Erreur : Problème avec l'initialisation de la liste de positions\n");
		exit(1);  
	}

	nouvelle_liste->positions = NULL;
	nouvelle_liste->positions = malloc(sizeof(t_pos)*nb_elements);
	if( (nouvelle_liste->positions) == NULL)
	{
		fprintf(stderr, "Erreur : Problème avec l'initialisation des positions t_pos dans la liste de position qui a déjà été définie.\n");
		exit(1);  
	}
	
	nouvelle_liste->nb_elem = nb_elements;
	
	return (nouvelle_liste);
}


/**
 * \fn err_t liste_positions_detruire( liste_pos_t ** liste )
 * \brief Détruit une liste de positions
 * \param liste Adresse de la liste à détruire
 * \return Code erreur de type err_t
 */
extern
err_t liste_positions_detruire( liste_pos_t ** liste )
{
	if(liste_positions_existe(*liste))
	{
		free((*liste)->positions);
		(*liste)->positions = NULL;

		free(*liste);
		*liste = NULL;
		return(Ok);
	}
	return(Erreur);
}


/**
 * \fn err_t liste_positions_ajouter( liste_pos_t * liste_positions, t_pos position, int indice )
 * \brief Ajoute une position à la liste
 * \param liste_positions Liste dans laquelle ajouter la position
 * \param position Position à ajouter
 * \param indice Indice de la liste dans lequel l'élement sera ajouté
 * \return Code erreur de type err_t
 */
extern
err_t liste_positions_ajouter( liste_pos_t * liste_positions, t_pos position, int indice )
{
	if(liste_positions_existe(liste_positions))
	{
		if(indice < 0 || indice >= liste_positions->nb_elem)
		{
			fprintf(stderr, "Erreur, l'indice en paramètre de la fonction liste_positions_ajouter n'est pas valide indice = %i \n", indice);
			return(Erreur);
		}
		liste_positions->positions[indice] = position;	
		return (Ok);
	}
	return(Erreur);
}

/**
 * \fn t_pos liste_position_lire(liste_pos_t * liste_positions, int indice)
 * \brief Lit une position dans la liste à l'indice donné
 * \param liste_positions Liste dans laquelle lire la position
 * \param indice Indice de la liste ou l'élement sera lu
 * \return position type t_pos
 */
extern
t_pos liste_position_lire(liste_pos_t * liste_positions, int indice)
{
	if(liste_positions_existe(liste_positions))
	{
		if(indice < 0 || indice >= liste_positions->nb_elem)
		{
			printf("Erreur, l'indice en paramètre de la fonction liste_elem_lire n'est pas valide");
			exit(1);
		}
		return liste_positions->positions[indice];
	}
	else
	{
		fprintf(stderr, "Dans liste_position_lire, la liste n'existait pas, programme arrêté");
		exit(1);
	}
}

/**
 * \fn void afficher_liste_positions_terminal(liste_pos_t * liste_positions)
 * \brief Afficher une liste de position sur la console, utilisée pour les test
 * \param liste_positions Liste dans laquelle lire la position
 * \return Aucun retour
 */
extern
void afficher_liste_positions_terminal(liste_pos_t * liste_positions)
{
	int i;
	t_pos position;

	if(liste_positions_existe(liste_positions))
	{
		if(taille_liste(liste_positions) == 0) printf("La liste est vide\n"); 
		else
		{
			for( i = 0; i < liste_positions->nb_elem ; i++)
			{
				position = liste_position_lire( liste_positions, i);
				//printf("Position %i : [%i,%i]\n", i, liste_positions->positions[i].x, liste_positions->positions[i].y);
				printf("Position %i : [%i,%i]\n", i, position.x, position.y);
			}
		}
	}

}




/**
 * \fn liste_pos_t* concatener_listes_positions(liste_pos_t * liste_1, liste_pos_t * liste_2)
 * \brief Cree une nouvelle liste à partir de deux fournie en parametre sans faire de doublons
 * \param liste_1 Liste de base
 * \param liste_2 Liste à ajouter
 * \return pointeur sur une liste de positions
 */
liste_pos_t* concatener_listes_positions(liste_pos_t * liste_1, liste_pos_t * liste_2)
{
	if(!liste_positions_existe(liste_1) && liste_positions_existe(liste_2) )
	{
		return liste_2;
	}
	
	if(liste_positions_existe(liste_1) && !liste_positions_existe(liste_2) )
	{
		return liste_1;
	}
	
	if(!liste_positions_existe(liste_1) && !liste_positions_existe(liste_2) )
	{
		return NULL;
	}

	//Compter les doublons
	int compteur_doublons = 0;
	int i;
	for(i = 0; i < liste_2->nb_elem; i++)
	{
		if(detecter_doublon(liste_2->positions[i], liste_1) == 1)
		{
			compteur_doublons++;
		}
	}

	//Création de la nouvelle liste
	liste_pos_t * nouvelle_liste;
	int taille_liste = liste_1->nb_elem + liste_1->nb_elem - compteur_doublons;
	nouvelle_liste = liste_positions_initialiser(taille_liste);

	int indice_nouvelle_liste = 0;

	//Ajout de la liste 1
	for(i = 0; i < liste_1->nb_elem; i++)
	{
		//printf("Ajout de l'indice %i \n", indice_nouvelle_liste);
		liste_positions_ajouter( nouvelle_liste, liste_1->positions[i], indice_nouvelle_liste );
		indice_nouvelle_liste++;
	}

	//Ajout de la liste 2
	for(i = 0; i < liste_2->nb_elem; i++)
	{
		
		if(detecter_doublon(liste_2->positions[i], liste_1) != 1)
		{
			//printf("Ajout de l'indice %i \n", indice_nouvelle_liste);
			liste_positions_ajouter( nouvelle_liste, liste_2->positions[i], indice_nouvelle_liste );
			indice_nouvelle_liste++;
		}
	}
	
	return nouvelle_liste;
}


/**
 * \fn int detecter_doublon(t_pos position, liste_pos_t * liste_positions)
 * \brief Détecter une occurence dans la liste
 * \param position Position à detecter
 * \param liste_positions Liste à scanner
 * \return Code Erreur 0 : Aucun doublon détecté, sinon 1
 */
int detecter_doublon(t_pos position, liste_pos_t * liste_positions)
{
	int i;
	t_pos position_courante;
	//printf("liste_positions->nb_elem = %i \n", liste_positions->nb_elem);   //Debug
	
	if(liste_positions_existe(liste_positions))
	{
		for(i = 0; i < liste_positions->nb_elem; i++)
		{
			position_courante = liste_positions->positions[i];
			if(position_courante.x == position.x &&  position_courante.y == position.y)
			{
				return 1; //Doublon détecté
			}
		}
	}
	return 0;//Aucun doublon détecté
}

/**
 * \fn liste_pos_t * retirer_occurences_liste(liste_pos_t * liste_initiale, liste_pos_t * liste_occurences)
 * \brief Retire toutes les occurences d'une liste à une autre liste
 * \param liste_initiale Liste dans laquelle retirer les occurences
 * \param liste_occurences Liste des occurences à retirer
 * \return pointeur sur une liste de positions
 */
liste_pos_t * retirer_occurences_liste(liste_pos_t * liste_initiale, liste_pos_t * liste_occurences)
{
	liste_pos_t * nouvelle_liste = liste_positions_initialiser(liste_initiale->nb_elem);
	int i;
	int indice_nouvelle_liste = 0;
	
	for(i = 0 ; i < liste_initiale->nb_elem ; i++)
	{
		if(detecter_doublon(liste_initiale->positions[i], liste_occurences) == 0)
		{
			liste_positions_ajouter( nouvelle_liste , liste_initiale->positions[i], indice_nouvelle_liste);
			indice_nouvelle_liste++;
		}
	}	

	liste_positions_detruire(&liste_initiale);
	nouvelle_liste = ajuster_taille_liste(nouvelle_liste, indice_nouvelle_liste);
	return nouvelle_liste;
}

/**
 * \fn liste_pos_t * retirer_une_occurence_liste(liste_pos_t * liste_initiale, t_pos position)
 * \brief Retire une occurence à une autre liste
 * \param liste_initiale Liste dans laquelle retirer l'occurence
 * \param position occurence à retirer
 * \return pointeur sur une liste de positions
 */
liste_pos_t * retirer_une_occurence_liste(liste_pos_t * liste_initiale, t_pos position)
{
	liste_pos_t * nouvelle_liste = liste_positions_initialiser(liste_initiale->nb_elem);
	int i;
	int indice_nouvelle_liste = 0;
	
	for(i = 0 ; i < liste_initiale->nb_elem ; i++)
	{
		if( liste_initiale->positions[i].x != position.x || liste_initiale->positions[i].y != position.y )
		{
			liste_positions_ajouter( nouvelle_liste , liste_initiale->positions[i], indice_nouvelle_liste);
			indice_nouvelle_liste++;
		}
	}

	liste_positions_detruire(&liste_initiale);
	nouvelle_liste = ajuster_taille_liste(nouvelle_liste, indice_nouvelle_liste);
	return nouvelle_liste;
}


/**
 * \fn liste_pos_t* ajuster_taille_liste(liste_pos_t * liste, int taille_liste)
 * \brief Crée une nouvelle liste avec la bonne taille
 * \param liste liste à modifier
 * \param taille_liste nouvelle taille de liste
 * \return pointeur sur une liste de positions
 */
liste_pos_t* ajuster_taille_liste(liste_pos_t * liste, int taille_liste)
{
	int i;
	liste_pos_t * liste_optimisee = liste_positions_initialiser(taille_liste);
	for(i = 0; i < taille_liste; i++)
	{
		//printf("Ajout de l'indice %i \n", indice_nouvelle_liste);
		liste_positions_ajouter( liste_optimisee, liste->positions[i] , i );
	}
	liste_positions_detruire( &liste );
	
	return liste_optimisee;
}


/**
 * \fn int taille_liste(liste_pos_t * liste)
 * \brief Renvoie la taille de la liste
 * \param liste Liste à utiliser
 * \return Integer qui indique le nombre de positions dans la liste
 */
int taille_liste(liste_pos_t * liste)
{
	if(liste_positions_existe(liste))
	{
		return liste->nb_elem;
	}
	return 0;
}


/**
 * \fn int liste_positions_existe(liste_pos_t * liste)
 * \brief Détecte si la liste existe
 * \param liste Liste à utiliser
 * \return Code erreur 1 , si tout s'est bien passé : 0
 */
int liste_positions_existe(liste_pos_t * liste)
{
	if(liste != NULL) return 1;
	else return 0;
}
