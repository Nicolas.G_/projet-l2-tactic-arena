/**
*  \file mage.c
*  \brief clmagee mage
*  \details cette clmagee contient les fonctions et methodes disponibles pour la clmagee mage
*  Un mage contient les caractéristiques suivantes :
*   - pv = 45;
*   - degat = 10;
*   - portee de deplacement = 3
*   - portee d'attaques = 5
*   - esquive = 0.60
*   - crit = 0.25
*   - resistance = 3
*   - precision = 1.00
*  \author Pierre garcon
*  \date 06 fevrier
*  \fn int existe_mage, void afficher_mage, void position_mage, err_t detruire_mage, mage_t creer_mage
*/




/*

  IMPORTANT : definir une fonction qui remet à 1 les points d'attaques et de deplacements

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/mage.h"
#include "../../include/attaques.h"
#include "../../include/nom_alea.h"
/*On défini la portée maximale des attaques et des déplacements*/
#define DEP 3
#define ATT 5


/*Définition des fonctions et méthodes*/
/**
 * \fn int existe_mage(mage_t * mage)
 * \brief fonction qui test si un mage existe
 *
 * \param mage Mage à tester
 *
 * \return int 0 | 1
 *
 */
extern
int existe_mage(mage_t * mage)
{
  if(mage == NULL)
    return(0);
  else
    return(1);
}

/*S'occupe de l'affuche de tous les parametres d'un mage*/
/**
 * \fn void afficher_mage(mage_t * mage)
 * \brief fonction qui affiche toute les caracteristique d'un mage
 *
 * \param mage Mage à afficher
 *
 * \return void pas de retour
 *
 */
static
void afficher_mage(mage_t * mage)
{
  if(existe_mage(mage))
  {
    if(mage->etat == Vivant)
    {
      printf("\nNom classe : mage");
      printf("\nNom : %s", mage->nom);
      printf("\nPV : %d", mage->pv);
      printf("\nDegats : %d", mage->degat);
      printf("\nDeplacements : %d", mage->portee.deplacement);
      printf("\nattaque : %d", mage->portee.attaques);
      printf("\nEsquive : %f", mage->esquive);
      printf("\ncritique : %f", mage->crit);
      printf("\nResistance : %d", mage->resistance);
      printf("\nPrecision : %f", mage->precision);
      if(mage->equipe == Bleu)
      {
        printf("\nL'mage est dans l'equipe des bleus");
      }
      else
      {
        printf("\nL'mage est dans l'equipe des rouges");
      }
      printf("\nFin des stat\n");
    }
    else
    {
      printf("\n\nL'mage est mort \n\n");
    }
  }
  else
  {
    printf("\nL'mage n'existe plus");
  }

}




/*Récuperer la vie d'un mage*/
/**
 * \fn int vie_mage(mage_t * mage)
 * \brief fonction qui recupere le nombre de pv d'un mage
 *
 * \param mage Mage à tester
 *
 * \return int mage->pv
 *
 */
static
int vie_mage(mage_t * mage)
{
  return mage->pv;
}




/*Récuperer les points d'attaque*/
/**
 * \fn int points_attaque_mage(mage_t * mage)
 * \brief fonction qui recupere le nombre de point d'attaque d'un mage
 *
 * \param mage Mage à tester
 *
 * \return int mage->pt_attaque
 *
 */
static
int points_attaque_mage(mage_t * mage)
{
  return mage->pt_attaque;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int points_deplacement_mage(mage_t * mage)
 * \brief fonction qui recupere le nombre de point de deplacement d'un mage
 *
 * \param mage Mage à tester
 *
 * \return int mage->pt_deplacement
 *
 */
static
int points_deplacement_mage(mage_t * mage)
{
  return mage->pt_deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int etat_mage(mage_t * mage)
 * \brief fonction qui recupere si un mage est vivant ou pas
 *
 * \param mage Mage à tester
 *
 * \return int mage->etat
 *
 */
static
int etat_mage(mage_t * mage)
{
  return mage->etat;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int portee_deplacement_mage(mage_t * mage)
 * \brief fonction qui recupere la portee de deplacement d'un mage
 *
 * \param mage Mage à tester
 *
 * \return int mage->portee.deplacement
 *
 */
static
int portee_deplacement_mage(mage_t * mage)
{
  return mage->portee.deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int portee_attaque_mage(mage_t * mage)
 * \brief fonction qui recupere la portee de d'attaque d'un mage
 *
 * \param mage Mage à tester
 *
 * \return int mage->portee.attaques
 *
 */
static
int portee_attaque_mage(mage_t * mage)
{
  return mage->portee.attaques;
}



/*Récuperer l'esquive de l'mage*/
/**
 * \fn float esquive_mage(mage_t * mage)
 * \brief fonction qui recupere la chance d'esquive d'un mage
 *
 * \param mage Mage à tester
 *
 * \return float mage->esquive
 *
 */
static
float esquive_mage(mage_t * mage)
{
  return mage->esquive;
}


/*
*Fonctions pour changer la direction des personnages
*/

static
void mage_haut(mage_t * mage)
{
  mage->direction = haut;
}

static
void mage_bas(mage_t * mage)
{
  mage->direction = bas;
}

static
void mage_droite(mage_t * mage)
{
  mage->direction = droite;
}

static
void mage_gauche(mage_t * mage)
{
  mage->direction = gauche;
}




/*savoir si la compétence spéciale de l'mage est disponible*/
/**
 * \fn bool comp_spe_mage(mage_t * mage)
 * \brief fonction qui recupere si la compétance spéciale d'un mage est disponible
 *
 * \param mage Mage à tester
 *
 * \return bool mage->comp_special
 *
 */
static
bool comp_spe_mage(mage_t * mage)
{
  return mage->comp_special;
}








/*Récuperer la position de l'acher*/
/**
 * \fn void position_mage(int *x, int *y, mage_t * mage)
 * \brief fonction qui recupere la position d'un mage
 *
 * \param x Position X d'un mage
 * \param y Position Y d'un mage
 * \param mage Mage à tester
 *
 * \return void pas de retour
 *
 */
static
t_pos position_mage(mage_t * mage)
{
  t_pos pos;
  pos.x = 0;
  pos.y = 0;
  if(existe_mage(mage))
  {
    if(mage->etat == Vivant)
    {
      pos.x = mage->pos_x;
      pos.y = mage->pos_y;
    }
  }
  return pos;
}





/*Récuperer l'équipe d'un mage'*/
/**
 * \fn equipe_t * equipe_mage(mage_t * mage)
 * \brief fonction qui recupere l'équipe d'un mage
 *
 * \param mage Mage à tester
 *
 * \return mage->equipe
 *
 */
static
equipe_t equipe_mage(mage_t * mage)
{
  return mage->equipe;
}



/*Récuperer la precision d'un mage*/
/**
 * \fn float precision_mage(mage_t * mage)
 * \brief fonction qui recupere la precision d'un mage
 *
 * \param mage Mage à tester
 *
 * \return float mage->precision
 *
 */
static
float precision_mage(mage_t * mage)
{
  return mage->precision;
}







/*Récuperer la position de l'acher*/
/**
 * \fn void modifier_position_mage(int x, int y, mage_t * mage)
 * \brief fonction qui permet de deplacer un mage
 *
 * \param x Position X d'un mage
 * \param y Position Y d'un mage
 * \param mage Mage à tester
 *
 * \return void pas de retour
 *
 */
static
void modifier_position_mage(int x, int y, mage_t * mage)
{
  if(existe_mage(mage))
  {
    if(mage->etat == Vivant)
    {
      if(mage->pt_deplacement != 0)
      {
        mage->pos_x = x;
        mage->pos_y = y;
        mage->pt_deplacement--;
      }
    }
  }
}








/*Diminue les PV d'un mage selon les dégats subis*/
/**
 * \fn etat_t perdre_vie_mage(int degat, mage_t * mage)
 * \brief fonction qui effectue des degat sur un mage et diminue leur pv
 *
 * \param degat Nombre de degat a inflicher au mage
 * \param mage Mage à tester
 *
 * \return Vivant | Mort | Rien
 *
 */
static
etat_t perdre_vie_mage(int degat, mage_t * mage)
{
  if(existe_mage(mage))
  {
    mage->pv = (mage->pv)-(degat-(mage->resistance));
    if((mage->pv)<=0)
    {
        mage->etat = Mort;
        return Mort;
    }
    else
    {
        return Vivant;
    }

  }
  return  Rien;
}

/*permet de detruire un mage*/
/**
 * \fn err_t detruire_mage(mage_t ** mage)
 * \brief fonction qui detruit une structure mage
 *
 * \param mage Mage à detruire
 *
 * \return Ok
 *
 */
static
err_t detruire_mage(mage_t ** mage)
{

  free((*mage));
  (*mage) = NULL;

  return(Ok);
}


/*On créer un nouvel mage, il n'y a pas de parametres car toutes les stats sont pré-défini*/
/**
 * \fn mage_t * creer_mage(equipe_t * equipe, race_t * race)
 * \brief fonction qui creer une structure mage
 *
 * \param equipe Dans lequel ajouter le mage
 * \param race Du mage
 *
 * \return nouveau_mage
 *
 */
extern
mage_t * creer_mage(equipe_t equipe, race_t * race, liste_perso_t * liste)
{
  mage_t * nouveau_mage = malloc(sizeof(mage_t));
  char nom[25];

  donner_nom(nom);



  /*Création des caractéristiques des mages*/
  nouveau_mage->classe = mage;
  strcpy(nouveau_mage->nom, nom);
  nouveau_mage->pv = 45;
  nouveau_mage->pv_max = 45;
  nouveau_mage->degat = 10;
  nouveau_mage->portee.deplacement = DEP;
  nouveau_mage->portee.attaques = ATT;
  nouveau_mage->esquive = 0.10;
  nouveau_mage->crit = 0.25;
  nouveau_mage->resistance = 3;
  nouveau_mage->precision = 1.00;
  nouveau_mage->pos_x = 0;
  nouveau_mage->pos_y = 0;
  nouveau_mage->parade = 0.0;
  nouveau_mage->etat = Vivant;
  nouveau_mage->equipe = equipe;
  nouveau_mage->pt_deplacement = 1;
  nouveau_mage->pt_attaque = 1;
  nouveau_mage->comp_special = true;
  nouveau_mage->race = race;
  nouveau_mage->direction = haut;
  nouveau_mage->liste = liste;
  nouveau_mage->type_race = race->race;

  /*Définition des compétences*/
  nouveau_mage->competence1 = firaball;
  nouveau_mage->competence2 = fabriquantdeglacons;
  nouveau_mage->competence3 = interitumnotitia;

  nouveau_mage->info_comp1 = firaball_info;
  nouveau_mage->info_comp2 = fabriquantdeglacons_info;
  nouveau_mage->info_comp3 = interitumnotitia_info;



  /*j'ai défini le type personnage_t qui va regrouper tous les types de personnages et permettre de faire des listes*/
  nouveau_mage->print =(void(*)(personnage_t* const)) afficher_mage;
  nouveau_mage->detruire = (err_t(*)(personnage_t**))detruire_mage;
  nouveau_mage->position_pers = (t_pos(*)(personnage_t*))position_mage;
  nouveau_mage->deplacer = (void(*)(int , int , personnage_t*))modifier_position_mage;
  nouveau_mage->f_subir_degats =(etat_t(*)(int, personnage_t*)) perdre_vie_mage;
  nouveau_mage->f_pt_deplacement = (int(*)(personnage_t *)) points_deplacement_mage;
  nouveau_mage->f_pt_attaque = (int(*)(personnage_t *)) points_attaque_mage;
  nouveau_mage->f_etat_pers = (etat_t(*)(personnage_t *)) etat_mage;
  nouveau_mage->f_vie = (int(*)(personnage_t *))vie_mage;
  nouveau_mage->f_esquive_parade = (float(*)(personnage_t *))esquive_mage;
  nouveau_mage->f_portee_attaque = (int(*)(personnage_t *))portee_attaque_mage;
  nouveau_mage->f_portee_deplacement = (int(*)(personnage_t *))portee_deplacement_mage;
  nouveau_mage->f_equipe = (equipe_t(*)(personnage_t *))equipe_mage;
  nouveau_mage->f_precision = (float(*)(personnage_t *))precision_mage;
  nouveau_mage->f_comp_spe = (bool(*)(personnage_t *))comp_spe_mage;
  nouveau_mage->tourner_haut = (void(*)(personnage_t *))mage_haut;
  nouveau_mage->tourner_bas = (void(*)(personnage_t *))mage_bas;
  nouveau_mage->tourner_droite = (void(*)(personnage_t *))mage_droite;
  nouveau_mage->tourner_gauche = (void(*)(personnage_t *))mage_gauche;



  return(nouveau_mage);
}
