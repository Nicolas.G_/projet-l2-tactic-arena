#include <stdio.h>
#include "../include/commun.h"
#include "../include/equipe.h"
#include "../include/map.h"
#include "../include/liste_malus.h"
#include "../include/malus.h"



void terminal_choix_action(personnage_t * joueur, int * tour_pas_fini); //Faire un choix d'action à partir du terminal


void main()
{
	t_temps cycle_jour_nuit = jour;
	
	equipe_t tour_en_cours = Bleu;	
	
	liste_perso_t * equipe_r; // = fonction_creation_equipe();
  	liste_perso_t * equipe_b; // = fonction_creation_equipe();

	int tour_en_cour = 1;
	int tour_pas_fini = 1;
	int compteur_cycle = 1;
	int continuerJeu = 1;
	int p_bleu = 0;
	int p_rouge = 0;
	personnage_t * joueur_courant; //Contient le joueur courant
	int choix_menu = -1;
	int nb_tours;
	

	while(continuerJeu)
	{
		//Cycle jour Nuit
		if(compteur_cycle == 5)
		{
			compteur_cycle = 0;
			switch(cycle_jour_nuit)
			{
				case jour:
					cycle_jour_nuit = nuit;
					break;
				case nuit:
					cycle_jour_nuit = jour;
					break;
			}
		}
		compteur_cycle++;

		//Gestion des Malus
		malus_actualiser();

			
		switch(tour_en_cours)
		{
			case Bleu:
				//Donner le tour à un joueur bleu
				joueur_courant = equipe_b[p_bleu];
				//Proposer des déplacements et attaques
				
				p_bleu++;
				if(p_bleu == 5)
				{
					p_bleu = 0;	
				}
				tour_en_cours = Rouge;
				break;
			case Rouge:	
				//Donner le tour à un joueur rouge
				joueur_courant = equipe_r[p_rouge];
				
				
				p_rouge++;
				if(p_rouge == 5)
				{
					p_rouge = 0;	
				}
				tour_en_cours = Bleu;
				break;
		}

		//Proposer des déplacements et attaques
		while(tour_pas_fini)
		{
			terminal_choix_action(joueur_courant, &tour_pas_fini); 
		}
		tour_en_cour = 1;
		joueur->pt_attaque = 1;
		joueur->pt_deplacements = 1;	
		nb_tours = 0;

		//CHECK GAGNANT
		
		
	}	
}


void terminal_choix_action(personnage_t * joueur, int * tour_pas_fini) //Faire un choix d'action à partir du terminal
{
	t_pos position;
	int i;
	printf("1. Effectuer un déplacement\n");
	printf("2. Effectuer une attaque\n");
	
	printf("3. Terminer son tour\n");
	printf("4. Abandonner\n");
	
	do
	{	
		printf("Action à effectuer :\n");
		scanf("%i", &choix_menu);
	}while(choix_menu < 1 || choix_menu > 4);

	switch(choix_menu)
	{
		case 1:
			if(joueur->pt_deplacements == 1)
			{
				printf("Liste des coordonnées disponibles pour le déplacement");
				position.x = joueur->pos_x;
				position.y = joueur->pos_y;
				liste_t_pos* positions_valides = deplacement_valide(joueur, position);
				printf("Choisir les coordonnées d'une case cible pour le déplacements");
				for(i = 0; i < positions_valides.nb_elem; i++)
				{
					
				}
			}
			joueur->pt_deplacements = 0;
			break;
		case 2:
			//SI LES POINTS D'ATTAQUES SONT SUFFISANT
			if(joueur->pt_attaque == 1)
			{
				printf("	1. Effectuer l'attaque 1 : \n"); //Afficher description avec infoatt
				printf("	2. Effectuer l'attaque 2\n");
				if(joueur->comp_special == true) //La compétence spéciale n'a pas encore été utilisée
				{
					printf("	3. Effectuer la competence spéciale\n");
					do
					{	
						printf("Attaque à effectuer :\n");
						scanf("%i", &choix_menu);
					}while(choix_menu < 1 || choix_menu > 3);
				}
				else
				{
					do
					{	
						printf("Attaque à effectuer :\n");
						scanf("%i", &choix_menu);
					}while(choix_menu < 1 || choix_menu > 2);
				}
				//Effectuer les attaques
				joueur->pt_attaque = 0;
			}
			

			break;
		case 3:
			printf("Le joueur passe son tour");
			*tour_pas_fini = 0;
			break;
		case 4:
			printf("Le joueur abandonne");
			*tour_pas_fini = 0;
			//ABANDON , MORT DE l'EQUIPE
			break;
	}
}