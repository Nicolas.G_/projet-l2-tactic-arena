#include "../include/reseau.h"

/**
 * \fn int reseau_demarrer(void)
 * \brief fonction qui démarre le reseau sous n'importe quel systeme d'exploitation
 * \param  void
 * \return int
 *
*/
extern
int reseau_demarrer(void)
{
  int erreur = 0;
  #if defined (WIN32)
    WSADATA WSAData;
    erreur = WSAStartup(MAKEWORD(2,2), &WSAData);
  #endif
  return erreur;
}

/**
 * \fn void reseau_fermer(void)
 * \brief fonction qui ferme le reseau sous n'importe quel systeme d'exploitation
 * \param  void
 * \return self
 *
*/
extern
void reseau_fermer(void)
{
  /*Si on est sous windows, il faut utiliser cette fonction*/
  #if defined (WIN32)
    WSACleanup();
  #endif
}

/**
 * \fn SOCKADDR_IN reseau_init_sin_serveur()
 * \brief fonction qui initialise le serveur
 * \param  void
 * \return SOCKADDR_IN
 *
*/
extern
SOCKADDR_IN reseau_init_sin_serveur()
{
  SOCKADDR_IN sin;
  sin.sin_addr.s_addr = htonl(INADDR_ANY); /*J'initialise les valeurs pour l'adresse*/
  sin.sin_family = AF_INET; /*AF_INET car TCP_IP*/
  sin.sin_port = htons(PORT); /*Numéro de port (30000 pour être sur de ne pas en utiliser un qui est déja pris)*/

  return sin;
}

/**
 * \fn SOCKADDR_IN reseau_init_sin_client(char *  adress_ip)
 * \brief fonction qui initialise le client
 * \param  adress_ip adresse ip du client
 * \return SOCKADDR_IN
 *
*/
extern
SOCKADDR_IN reseau_init_sin_client(char *  adress_ip)
{
  SOCKADDR_IN sin;
  sin.sin_addr.s_addr = inet_addr(adress_ip); /*J'initialise les valeurs pour l'adresse*/
  sin.sin_family = AF_INET; /*AF_INET car TCP_IP*/
  sin.sin_port = htons(PORT); /*Numéro de port (30000 pour être sur de ne pas en utiliser un qui est déja pris)*/

  return sin;
}

/**
 * \fn SOCKET reseau_connection_serveur(SOCKET sock, SOCKADDR_IN * adr, socklen_t * size)
 * \brief fonction qui établie la connexion entre un client et un serveur coté serveur
 * \param  socket socket du client
 * \param  SOCKADDR_IN adresse du client
 * \param  socklen_t taille de la socket
 * \return SOCKET
 *
*/
extern
SOCKET reseau_connection_serveur(SOCKET sock, SOCKADDR_IN * adr, socklen_t * size)
{
  int sock_err;
  SOCKET csock = -1;

  sock_err = listen(sock, 5);
  printf("\nEcoute du port %d\n", PORT);

  if(sock_err != SOCKET_ERROR)
  {
    /*Attendre que le client se connecte*/
    printf("\n.....Patientez, le client se connecte...\n\n");
    csock = accept(sock, (SOCKADDR*)adr, size);
    printf("Un client se connecte avec la socket %d de %s:%d\n", csock, inet_ntoa(adr->sin_addr), htons(adr->sin_port));
  }

  return(csock);

}


/**
 * \fn SOCKET reseau_connection_client(SOCKET sock, SOCKADDR_IN * adr, socklen_t * size)
 * \brief fonction qui établie la connexion entre un client et un serveur coté client
 * \param  socket socket du serveur
 * \param  SOCKADDR_IN adresse du client
 * \param  int taille de la socket
 * \return SOCKET
 *
*/
extern
int reseau_connection_client(SOCKET sock, SOCKADDR_IN * adr, int size)
{
  return (connect(sock, (SOCKADDR*)adr, size));
}



/**
 * \fn int reseau_personnage_envoyer(SOCKET csock, personnage_t * personnage)
 * \brief fonction qui permet l'envoie d'un personnage
 * \param  socket socket de destination du message
 * \param  personnage_t personnage qu'on envoie
 * \return SOCKET
 *
*/
extern
int reseau_personnage_envoyer(SOCKET csock, personnage_t * personnage)
{

  /*Envoie des pv*/
  if(send(csock, (char*)&(personnage->pv), sizeof(int), 0) == SOCKET_ERROR)
  {
    return (-1);
  }

  /*Envoie du nom*/
  if(send(csock, (char*)&(personnage->nom), 60, 0) == SOCKET_ERROR)
  {
    printf("\nLe nom n'est pas partie\n");
    return (-1);
  }

  /*Envoie de la classe*/
  if(send(csock, (char*)&(personnage->classe), sizeof(class_t), 0) == SOCKET_ERROR)
  {
    printf("\nla classe n'est pas partie\n");
    return (-1);
  }

  /*envoie des degats*/
  if(send(csock, (char*)&(personnage->degat), sizeof(int), 0) == SOCKET_ERROR)
  {

    printf("\nLes dégats ne sont pas partie\n");
    return (-1);
  }

  /*envoie de la portee*/

  /*Envoie de l'esquive*/
  if(send(csock, (char*)&(personnage->esquive), sizeof(float), 0) == SOCKET_ERROR)
  {
    printf("\nL'esquive n'est pas partie\n");
    return (-1);
  }

  /*Envoie des critiques*/
  if(send(csock, (char*)&(personnage->crit), sizeof(float), 0) == SOCKET_ERROR)
  {
    printf("\nLa critique n'est pas partie\n");
    return (-1);
  }

  /*envoie de la resistance*/
  if(send(csock, (char*)&(personnage->resistance), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nLa résistance n'est pas partie\n");
    return (-1);
  }

  /*envoie de la precision*/
  if(send(csock, (char*)&(personnage->precision), sizeof(float), 0) == SOCKET_ERROR)
  {
    printf("\nLa précision n'est pas partie\n");
    return (-1);
  }

  /*envoie de la position X*/
  if(send(csock, (char*)&(personnage->pos_x), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nLa position en X n'est pas partie\n");
    return (-1);
  }

  /*Envoie de la position Y*/
  if(send(csock, (char*)&(personnage->pos_y), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nLa positon en Y n'est pas partie\n");
    return (-1);
  }

  /*Envoie de la parade*/
  if(send(csock, (char*)&(personnage->parade), sizeof(float), 0) == SOCKET_ERROR)
  {
    printf("\nLa parade n'est pas partie\n");
    return (-1);
  }

  /*Envoie de l'etat*/
  if(send(csock, (char*)&(personnage->etat), sizeof(etat_t), 0) == SOCKET_ERROR)
  {
    printf("\nL'état n'est pas partie\n");
    return (-1);
  }

  /*-------------------------------------------Envoie de la race-----------------------------------------------------*/
  /*envoie Nom de la race*/


  if(send(csock, (char*)&(personnage->race->nom_race), 50, 0) == SOCKET_ERROR)
  {
    printf("\nLe nom de la race n'est pas partie\n");
    return (-1);
  }


  /*Envoie malus*/

  if(send(csock, (char*)&(personnage->race->malus), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nLe Malus de la race n'est pas partie\n");
    return (-1);
  }


  /*Envoie bonus*/

  if(send(csock, (char*)&(personnage->race->bonus), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nLe Bonus de la race n'est pas partie\n");
    return (-1);
  }

  /*Envoie type de race*/

  if(send(csock, (char*)&(personnage->race->race), sizeof(type_race_t), 0) == SOCKET_ERROR)
  {
    printf("\nLe type de la race n'est pas partie\n");
    return (-1);
  }

  /*Envoie equipe*/
  if(send(csock, (char*)&(personnage->equipe), sizeof(equipe_t), 0) == SOCKET_ERROR)
  {
    printf("\nL'equipe' n'est pas partie\n");
    return (-1);
  }

  /*Envoie PT deplacement*/
  if(send(csock, (char*)&(personnage->pt_deplacement), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nLes points de déplacements n'est pas partie\n");
    return (-1);
  }

  /*Envoie PT attaques*/
  if(send(csock, (char*)&(personnage->pt_attaque), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nLes points d'attaques ne sont pas partie\n");
    return (-1);
  }

  /*envoie competence special*/
  if(send(csock, (char*)&(personnage->comp_special), sizeof(bool), 0) == SOCKET_ERROR)
  {
    printf("\nLa compétencce spéciale n'est pas partie\n");
    return (-1);
  }

  /*Envoie direction*/
  if(send(csock, (char*)&(personnage->direction), sizeof(direction_t), 0) == SOCKET_ERROR)
  {
    printf("\nLa direction n'est pas partie\n");
    return (-1);
  }

  return(1);
}

/**
 * \fn personange_t * reseau_personnage_receptionner(SOCKET sock)
 * \brief fonction qui permet la reception d'un personnage
 * \param  socket socket de la source du message
 * \return personnage_t *
 *
*/
extern
personnage_t * reseau_personnage_receptionner(SOCKET sock /*, personnage_t * perso*/)
{
  race_t * race = malloc(sizeof(race_t));
  personnage_t * pers = malloc(sizeof(personnage_t));

  /*reception des PV*/
  if(recv(sock, (char*)&(pers->pv), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nPV non recu\n");
    return (NULL);
  }

  /*recepetion des noms*/
  if(recv(sock, (char*)&(pers->nom), 60, 0) == SOCKET_ERROR)
  {
    printf("\nPrenom non recu\n");
    return (NULL);
  }

  /*Reception classe*/
  if(recv(sock, (char*)&(pers->classe), sizeof(class_t), 0) == SOCKET_ERROR)
  {
    printf("\nClasse non recu\n");
    return (NULL);
  }
  /*Reception degats*/
  if(recv(sock, (char*)&(pers->degat), sizeof(int), 0) == SOCKET_ERROR)
  {
    //printf("\nDegats : %d", pers->degat);
  }
  else
  {
    printf("\nDegat non recu\n");
    return (NULL);
  }
  /*Reception Portee*/
  /*Reception Esquive*/
  if(recv(sock, (char*)&(pers->esquive), sizeof(float), 0) == SOCKET_ERROR)
  {
    //printf("\nEsquives : %f", pers->esquive);
  }
  else
  {
    printf("\nEsquive non recu\n");
    return (NULL);
  }
  /*Reception Critique*/
  if(recv(sock, (char*)&(pers->crit), sizeof(float), 0) == SOCKET_ERROR)
  {
    //printf("\nCritique: %f", pers->crit);
  }
  else
  {
    printf("\nCritique non recu\n");
    return (NULL);
  }
  /*Reception Resistance*/
  if(recv(sock, (char*)&(pers->resistance), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nResistance non recu\n");
    return (NULL);
  }
  /*Reception Precision*/
  if(recv(sock, (char*)&(pers->precision), sizeof(float), 0) == SOCKET_ERROR)
  {
    printf("\nPrecision non recu\n");
    return (NULL);
  }
  /*Reception Position X*/
  if(recv(sock, (char*)&(pers->pos_x), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nPosition x non recu\n");
    return (NULL);
  }
  /*Reception Position Y*/
  if(recv(sock, (char*)&(pers->pos_y), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nPosition y non recu\n");
    return (NULL);
  }


  /*Reception Parade*/
  if(recv(sock, (char*)&(pers->parade), sizeof(float), 0) == SOCKET_ERROR)
  {
    printf("\nparade non recu\n");
    return (NULL);
  }

  /*Reception Etat*/
  if(recv(sock, (char*)&(pers->etat), sizeof(etat_t), 0) == SOCKET_ERROR)
  {
    printf("\netat non recu\n");
    return (NULL);
  }


  /*---------------------------------------------------Reception Race------------------------------------------------------------*/
  /*Récéption Nom de la race*/


  //printf("\nRace\n");
  if(recv(sock, (char*)&(race->nom_race), 50, 0) == SOCKET_ERROR)
  {
    printf("\nAucun message reçu\n");
    return (NULL);
  }


  /*Réception malus*/

  if(recv(sock, (char*)&(race->malus), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nAucun message reçu\n");
    return (NULL);
  }

  /*Réception bonus*/

  if(recv(sock, (char*)&(race->bonus), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nAucun message reçu\n");
    return (NULL);
  }

  /*Réception type de race*/

  if(recv(sock, (char*)&(race->race), sizeof(type_race_t), 0) == SOCKET_ERROR)
  {
    printf("\nAucun message reçu\n");
    return (NULL);
  }

  /*Reception Equipe*/
  if(recv(sock, (char*)&(pers->equipe), sizeof(equipe_t), 0) == SOCKET_ERROR)
  {
    printf("\nequipe non recu\n");
    return (NULL);
  }
  /*Reception Point deplacement*/

  if(recv(sock, (char*)&(pers->pt_deplacement), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\npt deplacement non recu\n");
    return (NULL);
  }

  /*Reception Point d'attaque*/
  if(recv(sock, (char*)&(pers->pt_attaque), sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\npt attaque non recu\n");
    return (NULL);
  }
  /*Reception Competence speciale*/
  if(recv(sock, (char*)&(pers->comp_special), sizeof(bool), 0) == SOCKET_ERROR)
  {
    printf("\nCompetence special non recu\n");
    return (NULL);
  }
  /*Reception direction*/
  if(recv(sock, (char*)&(pers->direction), sizeof(direction_t), 0) == SOCKET_ERROR)
  {
    printf("\ndirection non recu\n");
    return (NULL);
  }

  pers->race = race;
  return(pers);
}


/**
 * \fn int reseau_map_envoyer(SOCKET sock, t_carte map[N][M])
 * \brief fonction qui permet l'envoie d'une carte
 * \param  socket socket de la destination du message
 * \param  map carte à envoyer
 * \return int
 *
*/
extern
int reseau_map_envoyer(SOCKET sock, t_carte map[N][M])
{
  int i;
  int j;
  /*Envoie de toutes les cases de la carte*/
  for(i=0; i<N; i++)
  {
    for(j=0; j<M; j++)
    {
      if(send(sock, (char*)&(map[i][j].type_emplacement), sizeof(int), 0) == SOCKET_ERROR)
      {
        printf("\ntype_emplacement n'est pas partie\n");
        return (-1);
      }

      //if(reseau_personnage_envoyer(sock, map[i][j].perso) == -1);
      //else printf("\n--------------------------------\nPersonnage envoyé\n-----------------------------------\n");


      if(send(sock, (char*)&(map[i][j].po_deplacement), sizeof(int), 0) == SOCKET_ERROR)
      {
        printf("\npoint déplacement n'est pas partie\n");
        return (-1);
      }

      if(send(sock, (char*)&(map[i][j].po_attaque), sizeof(int), 0) == SOCKET_ERROR)
      {
        printf("\npoint attaques n'est pas partie\n");
        return (-1);
      }

    }
  }
  return(1);
}

/**
 * \fn int reseau_map_envoyer(SOCKET sock, t_carte map[N][M])
 * \brief fonction qui permet l'envoie d'une carte
 * \param  socket socket de la source du message
 * \param  map carte à recevoir
 * \return int
 *
*/
extern
int reseau_map_recevoir(SOCKET sock, t_carte map[N][M])
{
  int i;
  int j;
  /*Envoie de toutes les cases de la carte*/
  for(i=0; i<N; i++)
  {
    for(j=0; j<M; j++)
    {
      if(recv(sock, (char*)&(map[i][j].type_emplacement), sizeof(int), 0) == SOCKET_ERROR)
      {
        printf("\ntype_emplacement n'est pas recu\n");
        return (-1);
      }

      //map[i][j].perso = reseau_personnage_receptionner(sock);
      map[i][j].perso = NULL;

      if(recv(sock, (char*)&(map[i][j].po_deplacement), sizeof(int), 0) == SOCKET_ERROR)
      {
        printf("\npoint déplacement n'est pas recu\n");
        return (-1);
      }

      if(recv(sock, (char*)&(map[i][j].po_attaque), sizeof(int), 0) == SOCKET_ERROR)
      {
        printf("\npoint attaques n'est par recu\n");
        return (-1);
      }
    }
  }

  printf("\n\n----------------------------------------------------\nMap recu\n----------------------------------------------------");
  return(1);
}

/**
 * \fn void reseau_coordonnees_envoye(personnage_t * pers, SOCKET sock)
 * \brief fonction qui permet l'envoie des coordonnées d'un personnage
 * \param  socket socket de la source du message
 * \param  personnage_t eprsonnage dont on envoie les coordonnées
 * \return self
 *
*/
extern
void reseau_coordonnees_envoye(personnage_t * pers, SOCKET sock)
{
  if(send(sock, (char*)&pers->pos_x, sizeof(int), 0) == SOCKET_ERROR)
    printf("\nEnvoie position X echoué\n");

  if(send(sock, (char*)&pers->pos_y, sizeof(int), 0) == SOCKET_ERROR)
    printf("\nEnvoie position Y echoué\n");

}

/**
 * \fn void reseau_coordonnees_recevoir(int * x, int * y, SOCKET sock)
 * \brief fonction qui permet la reception des coordonnées d'un personnage
 * \param  socket socket de la source du message
 * \param  int x position x du personnage
 * \param int y position y du personnage
 * \return self
 *
*/
extern
void reseau_coordonnees_recevoir(int * x, int * y, SOCKET sock)
{
  if(recv(sock, (char*)x, sizeof(int), 0) != SOCKET_ERROR)
  {
    printf("\nnReception position X réussi\n");
  }
  else
    printf("\nReception position X echoué\n");

  if(recv(sock, (char*)y, sizeof(int), 0) != SOCKET_ERROR)
  {
    printf("\nReception position Y réussi\n");
  }
  else
    printf("\nReception position Y echoué\n");
}


/**
 * \fn void reseau_mettre_a_jour_carte(liste_perso_t * liste, t_carte map[N][M])
 * \brief fonction qui met à jour la carte avec une liste de personnage
 * \param  liste_perso equipe
 * \param  carte map ou il faut placer l'équipe
 * \return self
 *
*/
extern
void reseau_mettre_a_jour_carte(liste_perso_t * liste, t_carte map[N][M])
{
  int i;
  for(i=0; i<liste->nb; i++)
  {
    map[liste->liste[i]->pos_x][liste->liste[i]->pos_y].perso = liste->liste[i];
  }
}

/**
 * \fn void reseau_equipe_recevoir(SOCKET socket, liste_perso_t * equipe)
 * \brief fonction qui permet la reception d'une equipe
 * \param  socket socket de la source
 * \param  liste_perso équipe à receptionner
 * \return self
 *
*/
extern
void reseau_equipe_recevoir(SOCKET socket, liste_perso_t * equipe)
{
  int recu = 0;
  printf("\nDebut de la fonction de reception d'une equipe\n");
  for(int i =0; i<equipe->nb; i++)
  {
     equipe->liste[i] = reseau_personnage_receptionner(socket);
  }
}

/**
 * \fn void reseau_equipe_envoyer(SOCKET sock, liste_perso_t * equipe)
 * \brief fonction qui permet l'envoie d'une equipe
 * \param  socket socket de la destination
 * \param  liste_perso équipe à envoyer
 * \return self
 *
*/
extern
void reseau_equipe_envoyer(SOCKET sock, liste_perso_t * equipe)
{
  int i;
  for(i=0; i<equipe->nb; i++)
  {
    reseau_personnage_envoyer(sock, equipe->liste[i]);
  }
}


/**
 * \fn void reseau_statut_attaque_envoyer(statut_att stat, SOCKET sock)
 * \brief fonction qui permet l'envoie d'un statu attaque
 * \param  socket socket de la destination
 * \param  statu_att statut à envoyer
 * \return self
 *
*/
extern
void reseau_statut_attaque_envoyer(statut_att stat, SOCKET sock)
{
  int i;

  /*Envoie de l'état de l'attaque*/
  if(send(sock, (char*)&stat.etat, sizeof(etat_attaque), 0) == SOCKET_ERROR)
  {
    printf("\nL'état de l'attaque n'est pas partie\n");
  }

  /*Envoie du nombre de cibles*/
  if(send(sock, (char*)&stat.nombre_cibles, sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nle nombre de cibles n'est pas partie\n");
  }

  /*Envoie du tableau de personnages cibles*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    reseau_personnage_envoyer(sock, stat.cibles[i]);
  }

  /*Envoie du type d'attaque*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    if(send(sock, (char*)&stat.type[i], sizeof(type_attaque), 0) == SOCKET_ERROR)
    {
      printf("\nLe type d'attaque n'est pas partie\n");
    }
  }

  /*envoie des dégats*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    if(send(sock, (char*)&stat.degats[i], sizeof(int), 0) == SOCKET_ERROR)
    {
      printf("\nLes degats de l'attaque ne sont pas partie\n");
    }
  }

  /*envoie des attaques esquivee*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    if(send(sock, (char*)&stat.attaque_esquivee[i], sizeof(int), 0) == SOCKET_ERROR)
    {
      printf("\nLes esquives de l'attaque ne sont pas partie\n");
    }
  }

  /*envoie des états des cibles*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    if(send(sock, (char*)&stat.etat_cible[i], sizeof(etat_t), 0) == SOCKET_ERROR)
    {
      printf("\nLes degats de l'attaque ne sont pas partie\n");
    }
  }

  /*Envoie description effet*/
  if(send(sock, stat.description_effet, strlen(stat.description_effet), 0) == SOCKET_ERROR)
  {
    printf("\nLa description des effets n'est pas partie\n");
  }

  /*envoie du nom de l'attaque*/
  if(send(sock, stat.nom_attaque, strlen(stat.nom_attaque), 0) == SOCKET_ERROR)
  {
    printf("\nle Nom de l'attaque n'est pas partie\n");
  }

  /*Envoie du lanceur*/
  reseau_personnage_envoyer(sock, stat.lanceur);

  /*envoie des soins sur le lanceur*/
  if(send(sock, (char*)&stat.soins_sur_lanceur, sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nles soins sur lanceur ne sont pas partie\n");
  }

  /*envoie des degats sur lanceur*/
  if(send(sock, (char*)&stat.degats_sur_lanceur, sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\ndegats sur lanceur ne sont pas partie\n");
  }

}


/**
 * \fn statut_att reseau_statut_attaque_recevoir(SOCKET sock)
 * \brief fonction qui permet la reception d'un statut attaque
 * \param  socket socket de la destination
 * \return statut_att
 *
*/

extern
statut_att reseau_statut_attaque_recevoir(SOCKET sock)
{
  int i;
  statut_att stat;

  /*reception de l'état de l'attaque*/
  if(recv(sock, (char*)&stat.etat, sizeof(etat_attaque), 0) == SOCKET_ERROR)
  {
    printf("\nL'état de l'attaque n'est pas partie\n");
  }

  /*reception du nombre de cibles*/
  if(recv(sock, (char*)&stat.nombre_cibles, sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nle nombre de cibles n'est pas partie\n");
  }

  /*reception du tableau de personnages cibles*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    stat.cibles[i] = reseau_personnage_receptionner(sock);
  }

  /*reception du type d'attaque*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    if(recv(sock, (char*)&stat.type[i], sizeof(type_attaque), 0) == SOCKET_ERROR)
    {
      printf("\nLe type d'attaque n'est pas partie\n");
    }
  }

  /*reception des dégats*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    if(recv(sock, (char*)&stat.degats[i], sizeof(int), 0) == SOCKET_ERROR)
    {
      printf("\nLes degats de l'attaque ne sont pas partie\n");
    }
  }

  /*reception des attaques esquivee*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    if(recv(sock, (char*)&stat.attaque_esquivee[i], sizeof(int), 0) == SOCKET_ERROR)
    {
      printf("\nLes esquives de l'attaque ne sont pas partie\n");
    }
  }

  /*reception des états des cibles*/
  for(i=0; i<NB_CASES_TABLEAU_CIBLES; i++)
  {
    if(recv(sock, (char*)&stat.etat_cible[i], sizeof(etat_t), 0) == SOCKET_ERROR)
    {
      printf("\nLes degats de l'attaque ne sont pas partie\n");
    }
  }

  /*reception description effet*/
  if(recv(sock, stat.description_effet, strlen(stat.description_effet), 0) == SOCKET_ERROR)
  {
    printf("\nLa description des effets n'est pas partie\n");
  }

  /*reception du nom de l'attaque*/
  if(recv(sock, stat.nom_attaque, strlen(stat.nom_attaque), 0) == SOCKET_ERROR)
  {
    printf("\nle Nom de l'attaque n'est pas partie\n");
  }

  /*reception du lanceur*/
  stat.lanceur = reseau_personnage_receptionner(sock);

  /*reception des soins sur le lanceur*/
  if(recv(sock, (char*)&stat.soins_sur_lanceur, sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nles soins sur lanceur ne sont pas partie\n");
  }

  /*reception des degats sur lanceur*/
  if(recv(sock, (char*)&stat.degats_sur_lanceur, sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\ndegats sur lanceur ne sont pas partie\n");
  }
  return (stat);

}

/*
  ---------------------------------------------------------------------------------------
  Fonction utilisé par les thread
  ---------------------------------------------------------------------------------------
*/

/**
 * \fn void * fonction_envoyer_carte(void * data)
 * \brief fonction qui permet l'envoie de la carte à l'aide d'un thread
 * \param  void * data structure de données
 * \return void *
 *
*/
extern
void * fonction_envoyer_carte(void * data)
{
  SOCKET socket = (SOCKET)*(((donnees_carte_t*)data)->arg);
  reseau_map_envoyer(socket, map);
  pthread_exit(NULL);
}

/**/
/**
 * \fn void * fonction_envoyer_equipe(void * data)
 * \brief fonction qui permet l'envoie d'une equipe à l'aide d'un thread
 * \param  void * data structure de données
 * \return void *
 *
*/
extern
void * fonction_envoyer_equipe(void * data)
{
  SOCKET socket = (SOCKET)*(((donnees_equipe_t*)data)->arg);
  reseau_equipe_envoyer(socket, (liste_perso_t*)(((donnees_equipe_t*)data)->equipe));
  pthread_exit(NULL);
}

/*
  -------------------------------------------------------------------------------------
  -------------------------------------------------------------------------------------
*/

/**
 * \fn void reseau_effectuer_tour(personnage_t * joueur, int * tour_pas_fini, SOCKET sock)
 * \brief fonction qui permet de faire jouer un tour à un client
 * \param  personnage_t personnage courant
 * \param  int * tour_pas_fini entier annoncant la fin du tour
 * \param  SOCKET socket du serveur pour communiquer les informations
 * \return void
 *
*/
extern
void reseau_effectuer_tour(personnage_t * joueur, int * tour_pas_fini, SOCKET sock)
{
  t_pos position;
	int choix_menu = 0;
	int choix_x, choix_y;
	printf("Tour du joueur en [%i,%i]\n", joueur->pos_x, joueur->pos_y);
	printf("Nom : %s, PV : %i", joueur->nom, joueur->pv);

	if(joueur->equipe == Bleu) printf("Equipe bleue\n");
	else printf("Equipe rouge\n");

	printf("1. Effectuer un déplacement\n");
	printf("2. Effectuer une attaque\n");

	printf("3. Terminer son tour\n");
	printf("4. Abandonner\n");

	do
	{
		printf("Action à effectuer :\n");
		scanf("%i", &choix_menu);
	}while(choix_menu < 1 || choix_menu > 4);

	switch(choix_menu)
	{
		case 1:
			if(joueur->pt_deplacement == 1)
			{

        send(sock, (char*)&choix_menu, sizeof(int), 0);

				printf("Liste des coordonnées disponibles pour le déplacement\n");
				position.x = joueur->pos_x;
				position.y = joueur->pos_y;
				liste_pos_t * positions_valides = deplacement_valide(joueur);
				ajouter_marqueurs_deplacements_carte(positions_valides);
				//afficher_liste_positions_terminal(positions_valides);
				afficher_plateau(VISUEL);
				printf("Choisir les coordonnées d'une case cible pour le déplacements\n");
				printf("Coordonnée x : "); scanf("%i", &choix_x);
				printf("Coordonnée y : "); scanf("%i", &choix_y);

				position.x = choix_x;
				position.y = choix_y;
				if( effectuer_deplacement(joueur, position) == 0 )
				{
					joueur->pt_deplacement = 0;
					reseau_afficher_direction_perso(joueur);
          reseau_personnage_envoyer(sock, joueur);
				}
				else printf("La case cible n'était pas valide\n");
				retirer_marqueurs_deplacements_carte(positions_valides);
			}
			else
			{
				printf("Points de déplacements insuffisants \n");
			}

			break;
		case 2:
			//SI LES POINTS D'ATTAQUES SONT SUFFISANT
			if(joueur->pt_attaque == 1)
			{
				//Choix des attaques
				printf("\n	1. Effectuer l'attaque 1 : \n"); //Afficher description avec infoatt
				afficher_description_attaque(joueur->info_comp1);
				printf("\n	2. Effectuer l'attaque 2 :\n");
				afficher_description_attaque(joueur->info_comp2);
				if(joueur->comp_special == true) //La compétence spéciale n'a pas encore été utilisée
				{
					printf("\n	3. Effectuer la competence spéciale\n");
					afficher_description_attaque(joueur->info_comp3);
				}

				printf("Attaque à effectuer :\n");
				scanf("%i", &choix_menu);

				//Récupération des informations de l'attaque choisie
				info_att info_attaque;
				switch(choix_menu)
				{
					case 1:
						info_attaque = joueur->info_comp1();
						break;
					case 2:
						info_attaque = joueur->info_comp2();
						break;
					case 3:
						if(joueur->comp_special == true) //La compétence spéciale n'a pas encore été utilisée
						{
							info_attaque = joueur->info_comp3();
						}
						else
						{
							return;
						}
						break;
					default:
						return;
						break;
				}
				int portee_attaque = info_attaque.portee;


				//Controle de validité de l'attaque
				liste_pos_t * positions_valides = attaque_valide(joueur, portee_attaque);
				ajouter_marqueurs_attaque_carte(positions_valides);
				afficher_plateau(VISUEL);
				int demanderCoordonnees = 1;
				do
				{
					//Choisir une case cible
					printf("Choisir les coordonnées d'une case cible pour l'attaque\n");
					printf("Coordonnée x : "); scanf("%i", &choix_x);
					printf("Coordonnée y : "); scanf("%i", &choix_y);
					position.x = choix_x;
					position.y = choix_y;
					demanderCoordonnees = controler_coordonnees_attaque(position, positions_valides);

				}while(demanderCoordonnees == 1);
				retirer_marqueurs_attaque_carte(positions_valides);
				liste_positions_detruire( &positions_valides );


				//Effectuer l'attaque
				statut_att nouvelle_attaque;
				switch(choix_menu)
				{
					case 1: //Attaque 1
						nouvelle_attaque = joueur->competence1(joueur, position);
						break;
					case 2: //Attaque 2
						nouvelle_attaque = joueur->competence2(joueur, position);
						break;
					case 3: //Compétence spéciale
						nouvelle_attaque = joueur->competence3(joueur, position);
						break;
				}
				afficher_statut_attaque(nouvelle_attaque);
				joueur->pt_attaque = 0;
			}


			break;
		case 3:
			printf("Le joueur passe son tour\n");
			*tour_pas_fini = 0;
			break;
		case 4:
			printf("Le joueur abandonne\n");
			*tour_pas_fini = 0;

			joueur->etat = Mort;
			int x = joueur->pos_x;
			int y = joueur->pos_y;
			map[x][y].type_emplacement = VIDE;
			map[x][y].perso = NULL;
			personnage_MAJ_equipe_mort(joueur);

			break;
	}
}

/**
 * \fn void reseau_recuperer_tour(personnage_t * personnage_courant, t_carte map[N][M], SOCKET sock)
 * \brief fonction qui permet de récuperer le tour en cours
 * \param  personnage_t personnage courant
 * \param map carte de jeu actuel
 * \param  SOCKET socket du serveur pour communiquer les informations
 * \return void
 *
*/
extern
void reseau_recuperer_tour(liste_perso_t * equipe, int p_equipe, personnage_t * personnage_courant, SOCKET sock)
{
  int choix_menu;
  //recv(sock, (char*)&choix_menu, sizeof(int), 0);
  if(recv(sock, (char*)&choix_menu, sizeof(int), 0) != SOCKET_ERROR)
    printf("\nChoix récuperer\n");

  printf("\n choix : %d\n", choix_menu);
  switch(choix_menu)
  {
    case 1:
      printf("\nCAS 1\n\n");
      personnage_courant = reseau_personnage_receptionner(sock);
      printf("\nAcualisation des données\n\n");
      reseau_actualiser_carte_perso_equipe(equipe, personnage_courant, p_equipe);
      afficher_plateau(VISUEL);

    case 2:
      reseau_map_recevoir(sock, map);
  }

}

/**
 * \fn void reseau_afficher_direction_perso(personnage_t * perso)
 * \brief fonction qui permet d'afficher la direction d'un personange
 * \param  personnage_t personnage courant
 * \return void
 *
*/
extern
void reseau_afficher_direction_perso(personnage_t * perso)
{
  printf("Perso %s : [%i,%i] ", perso->nom, perso->pos_x, perso->pos_y);
	switch(perso->direction)
	{
		case haut: printf("Regarde en haut"); break;
		case bas: printf("Regarde en bas"); break;
		case droite: printf("Regarde à droite"); break;
		case gauche: printf("Regarde à gauche"); break;
		default: /* Aucune action */ break;
	}
	printf("\n");
}

/**
 * \fn void reseau_afficher_direction_persos(liste_perso_t * equipe_rouge, liste_perso_t * equipe_bleue)
 * \brief fonction qui permet d'afficher la direction de tous les personanges des équipes
 * \param  liste_perso equipe 1
* \param  liste_perso equipe 2
 * \return void
 *
*/
extern
void reseau_afficher_direction_persos(liste_perso_t * equipe_rouge, liste_perso_t * equipe_bleue)
{
  int i;

  printf("Afficher nb_pers rouge : %i, Afficher nb_pers rouge : %i\n", equipe_rouge->nb, equipe_bleue->nb );

  printf("Equipe Bleue\n");
  for(i = 0; i < equipe_bleue->nb; i++)
  {
    personnage_t * perso = equipe_bleue->liste[i];
    reseau_afficher_direction_perso(perso);
  }

  printf("Equipe Rouge\n");
  for(i = 0; i < equipe_rouge->nb; i++)
  {

    personnage_t * perso = equipe_rouge->liste[i];
    reseau_afficher_direction_perso(perso);
  }
}

/*Fonction pour jouer un tour*/
/**
 * \fn reseau_jouer_tour(liste_perso_t * equipe, int p_equipe, SOCKET sock)
 * \brief fonction qui permet a un joueur de jouer son tour
 * \param  liste_perso equipe du joueur
* \param  int quel est le joueur a jouer
* \param SOcket du serveur
 * \return void
 *
*/
extern
void reseau_jouer_tour(liste_perso_t * equipe, int p_equipe, SOCKET sock)
{
  //Donner le tour à un joueur bleu
  personnage_t * joueur_courant = equipe->liste[p_equipe];
  int tour_pas_fini = 1;

  //Proposer des déplacements et attaques
  if(joueur_courant->etat == Vivant)//Passer au tour de l'équipe suivante si le joueur est mort
  {
    while(tour_pas_fini == 1)
    {
      reseau_effectuer_tour(joueur_courant, &tour_pas_fini, sock);
      afficher_plateau(VISUEL);
      send(sock, (char*)&tour_pas_fini, sizeof(int), 0);
    }

    joueur_courant->pt_attaque = 1;
    joueur_courant->pt_deplacement = 1;
  }
}

/**
 * \fn int reseau_envoyer_variables(int p_equipe, t_temps temps, SOCKET sock)
 * \brief fonction qui permet d'envoyer les variables
 * \param  int le numero du tour de l'équipe
* \param  t_temps le temps qu'il fait (jour ou nuit)
* \param Socket du client
 * \return int
 *
*/
extern
int reseau_envoyer_variables(int p_equipe, t_temps temps, SOCKET sock)
{
  if(send(sock, (char*)&p_equipe, sizeof(int), 0) == SOCKET_ERROR)
  {
    printf("\nErreur d'envoie de la variable p_equipe Jeu\n");
    return (-1);
  }


  if(send(sock, (char*)&temps, sizeof(t_temps), 0) == SOCKET_ERROR)
  {
      printf("\nErreur d'envoie de la variable temps Jeu\n");
      return(-1);
  }

  return(1);

}

/**
 * \fn void reseau_recevoir_variables(int * p_equipe, t_temps * temps, SOCKET sock)
 * \brief fonction qui permet d'envoyer les variables
 * \param  int le numero du tour de l'équipe
* \param  t_temps le temps qu'il fait (jour ou nuit)
* \param Socket du serveur
 * \return self
 *
*/
extern
void reseau_recevoir_variables(int * p_equipe, t_temps * temps, SOCKET sock)
{
  int p_equ;
  t_temps tmp;
  if(recv(sock, (char*)&p_equ, sizeof(int), 0) == SOCKET_ERROR)
    printf("\nErreur d'envoie de la variable p_equipe Jeu\n");

  if(recv(sock, (char*)&tmp, sizeof(t_temps), 0) == SOCKET_ERROR)
    printf("\nErreur d'envoie de la variable temps Jeu\n");

  *p_equipe = p_equ;
  *temps = tmp;
}


/**
 * \fn int reseau_actualiser_carte_perso_equipe(liste_perso_t * equipe, personnage_t * perso_courant, int p_equipe)
 * \brief fonction qui permet la msie a jour des pointeurs de la carte après un déplacement
 * \param  equipe a mettre à jour
 * \param  peronnage personnage de l'action en cours
 * \param p_equipe indice du personnage en cours
 * \return self
 *
*/
extern
int reseau_actualiser_carte_perso_equipe(liste_perso_t * equipe, personnage_t * perso_courant, int p_equipe)
{
  printf("\nDébut fonction acualiser\n\n");
  printf("\nCoordonnées du 1er perso : %d %d", equipe->liste[p_equipe]->pos_x,equipe->liste[p_equipe]->pos_y);
  map[equipe->liste[p_equipe]->pos_x][equipe->liste[p_equipe]->pos_y].type_emplacement = VIDE;
  map[equipe->liste[p_equipe]->pos_x][equipe->liste[p_equipe]->pos_y].perso = NULL;
  free(map[equipe->liste[p_equipe]->pos_x][equipe->liste[p_equipe]->pos_y].perso);

  equipe->liste[p_equipe] = NULL;
  equipe->liste[p_equipe] = perso_courant;

  map[perso_courant->pos_x][perso_courant->pos_y].perso = perso_courant;
  map[perso_courant->pos_x][perso_courant->pos_y].type_emplacement = PERSO;

  afficher_plateau(BINAIRE);
  return(1);
}
