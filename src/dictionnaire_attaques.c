#include "../include/attaques.h"

/**
 * \file dictionnaire_attaques.c
 * \brief Fonctions contenant la descriptions de chaque attaques
 * \author Yohann Delacroix
 * \version 1.0
 * \date 18 Février 2021
 */


/**
 * \fn info_att tirdelaigle_info()
 * \brief Description de l'attaque Tir de l'aigle
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att tirdelaigle_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 10;
	info.type = Attaque;
	info.degats_min = 1;
	info.degats_max = 10;
	info.description = "Inflige des dommages.";
	info.nom = "Tir de l'aigle";
	return info;
}

/**
 * \fn info_att tirdecombat_info()
 * \brief Description de l'attaque Tir de combat
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att tirdecombat_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 6;
	info.type = Attaque;
	info.degats_min = 15;
	info.degats_max = 20;
	info.description = "Inflige des dommages.";
	info.nom = "Tir de combat";
	return info;
}

/**
 * \fn info_att barrageabsolu_info()
 * \brief Description de l'attaque Barrage absolu
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att barrageabsolu_info()
{
	info_att info;
	info.zone = 4;
	info.portee = 8;
	info.type = Attaque;
	info.degats_min = 30;
	info.degats_max = 30;
	info.description = "Inflige des dommages en zone.";
	info.nom = "Barrage Absolu";
	return info;
}

/**
 * \fn info_att coupdeDague_info()
 * \brief Description de l'attaque Coup de Dague
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att coupdeDague_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 1;
	info.type = Attaque;
	info.degats_min = 10;
	info.degats_max = 20;
	info.description = "Inflige des dommages.";
	info.nom = "Coup de dague";
	return info;
}

/**
 * \fn info_att lancerdedagues_info()
 * \brief Description de l'attaque Lancer de dagues
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att lancerdedagues_info()
{
	info_att info;
	info.zone = 1;
	info.portee = 4;
	info.type = Attaque;
	info.degats_min = 5;
	info.degats_max = 8;
	info.description = "Inflige des dommages en zone (croix de 1 case)";
	info.nom = "Lancer de dagues";
	return info;
}

/**
 * \fn info_att beniparlesdieuxdusang_info()
 * \brief Description de l'attaque Béni par les dieux du sang
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att beniparlesdieuxdusang_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 1;
	info.type = Attaque;
	info.degats_min = 40;
	info.degats_max = 40;
	info.description = "-2 de déplacement   pour le lanceur pour 3 tours.     Malus de -5 dégâts pendant 3 tours.";
	info.nom = "Beni par les dieux du sang";
	return info;
}

/**
 * \fn info_att firaball_info()
 * \brief Description de l'attaque Firaball!
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att firaball_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 6;
	info.type = Attaque;
	info.degats_min = 10;
	info.degats_max = 15;
	info.description = "10% de chances       d'augmenter les dégâts de 10.";
	info.nom = "Firaball !";
	return info;
}

/**
 * \fn info_att fabriquantdeglacons_info()
 * \brief Description de l'attaque Fabriquant de glaçons
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att fabriquantdeglacons_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 15;
	info.type = Attaque;
	info.degats_min = 5;
	info.degats_max = 10;
	info.description = "Si la cible est au   corps à corps les dégats sont   augmentés de 20 et la cible passe son tour.";
	info.nom = "Fabriquant de glacons";
	return info;
}

/**
 * \fn info_att interitumnotitia_info()
 * \brief Description de l'attaque Interitum Notitia
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att interitumnotitia_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 10;
	info.type = Attaque;
	info.degats_min = 40;
	info.degats_max = 40;
	info.description = "Inflige d'importants dommages.";
	info.nom = "Interitum Notitia";
	return info;
}

/**
 * \fn info_att benedictiondivine_info()
 * \brief Description de l'attaque Bénédiction Divine
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att benedictiondivine_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 8;
	info.type = Soin;
	info.degats_min = 10;
	info.degats_max = 15;
	info.description = "Soigne la cible.";
	info.nom = "Benediction divine";
	return info;
}

/**
 * \fn info_att transfertdevitalite_info()
 * \brief Description de l'attaque Transfert de vitalité
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att transfertdevitalite_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 5;
	info.type = Soin;
	info.degats_min = 5;
	info.degats_max = 30;
	info.description = "Soigne la cible et   inflige des dégâts au lanceur.";
	info.nom = "Transfert de vitalite";
	return info;
}

/**
 * \fn info_att gracedeladeesse_info()
 * \brief Description de l'attaque Grâce de la déesse
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att gracedeladeesse_info()
{
	info_att info;
	info.zone = 3;
	info.portee = 0;
	info.type = Soin;
	info.degats_min = 30;
	info.degats_max = 40;
	info.description = "Soigne le lanceur et les alliés proches.";
	info.nom = "Grace de la deesse";
	return info;
}

/**
 * \fn info_att coupepeesimple_info()
 * \brief Description de l'attaque Coup d'épee simple
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att coupepeesimple_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 1;
	info.type = Attaque;
	info.degats_min = 10;
	info.degats_max = 15;
	info.description = "Inflige des dégats.";
	info.nom = "Coup d'epee simple";
	return info;
}

/**
 * \fn info_att coupdebouclierrecul_info()
 * \brief Description de l'attaque Coup de bouclier recul
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att coupdebouclierrecul_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 3;
	info.type = Attaque;
	info.degats_min = 5;
	info.degats_max = 10;
	info.description = "Inflige des dégats  et retire des PM pour 2 tours";
	info.nom = "Coup de Bouclier Recul";
	return info;
}

/**
 * \fn info_att volontedudefenseur_info()
 * \brief Description de l'attaque Volonté du défenseur
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att volontedudefenseur_info()
{
	info_att info;
	info.zone = 0;
	info.portee = 0;
	info.type = NoDamage;
	info.degats_min = 0;
	info.degats_max = 0;
	info.description = "Armure augmentée de 30 pendant 3 tours. Dégats       augmentés de 20 pendant 3 tours.";
	info.nom = "Volonte du defenseur";
	return info;
}

/**
 * \fn info_att voleedepee_info()
 * \brief Description de l'attaque Volée d'épée
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att voleedepee_info()
{
	info_att info;
	info.zone = 2;
	info.portee = 0;
	info.type = Attaque;
	info.degats_min = 15;
	info.degats_max = 20;
	info.description = "Dégats infligés en zone autour du lanceur.";
	info.nom = "Volee d'epee";
	return info;
}

/**
 * \fn info_att ragedudueliste_info()
 * \brief Description de l'attaque Rage du duéliste
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att ragedudueliste_info()
{
	info_att info;
	info.portee = 1;
	info.zone = 0;
	info.type = Attaque;
	info.degats_min = 20;
	info.degats_max = 30;
	info.description = "30% de chances       d'infliger un bonus de dégats         supplémentaires à la cible.";
	info.nom = "Rage du dueliste";
	return info;
}

/**
 * \fn info_att dansedelamort_info()
 * \brief Description de l'attaque Danse de la mort
 * \param self
 * \return Structure info_att contenant la description de l'attaque
 */
info_att dansedelamort_info()
{
	info_att info;
	info.portee = 1;
	info.zone = 0;
	info.type = Attaque;
	info.degats_min = 100;
	info.degats_max = 100;
	info.description = "Inflige d'importants dégats.";
	info.nom = "Danse de la Mort";
	return info;
}
