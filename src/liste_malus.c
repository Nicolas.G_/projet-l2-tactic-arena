/**
 * \file liste_malus.c
 * \brief Fichier qui gere les malus
 * \author
 * \version 1.0
 * \date
 *
 * Divers fonction sur la liste des malus
 *
 */

#include "../include/liste_malus.h"


#define MAX_MALUS 30

/*Définition des methodes*/

extern
void liste_malus_initialiser()
{
  liste_malus = malloc(sizeof(liste_malus));
  liste_malus->nb_malus = 0;
  liste_malus->malus_liste = NULL;
  liste_malus->malus_liste = malloc(sizeof(malus_t*)*MAX_MALUS);
  for(int i=0; i<MAX_MALUS; i++)
  {
    liste_malus->malus_liste[i] = NULL;
  }
}

/*Ajout dans une liste*/
/**
 * \fn void liste_malus_ajouter(liste_malus_t * liste, malus_t * malus)
 * \brief fonction qui ajoute un malus a la liste
 *
 * \param liste liste contenant tout les malus
 * \param malus malus a ajouter a la liste
 *
 * \return void pas de retour
 *
 */

extern
void liste_malus_ajouter(type_malus_t type, int duree, personnage_t * cible, int val_malus)
{
  if(!liste_malus_pleine(liste_malus)){
    malus_t * malus = creer_malus(type, duree, cible, val_malus);
    liste_malus->malus_liste[liste_malus->nb_malus] = malus;
    liste_malus->nb_malus++;
  }
}


/*Affichage de la liste des malus*/
/**
 * \fn void liste_malus_afficher(liste_malus_t * liste)
 * \brief fonction qui liste tout les malus
 *
 * \param liste liste contenant tout les malus
 *
 * \return void pas de retour
 *
 */
extern
void liste_malus_afficher()
{
  printf("{");
  for(int i=0; i<liste_malus->nb_malus; i++)
  {
    if(liste_malus->malus_liste[i] == NULL);
    else
    {
      printf("\n[ ");
      printf("malus numero %d ", i);
      printf("duree : %d ", liste_malus->malus_liste[i]->duree);
      printf("Personnage : \n");
      //liste->malus_liste[i]->print(liste->malus_liste[i]);
      liste_malus->malus_liste[i]->cible->print(liste_malus->malus_liste[i]->cible);
      printf(" ] ");
    }

  }
  printf("} ");
}

/*mise à jour des malus*/
/**
 * \fn void liste_malus_actualiser(t_temps jour_nuit, int tour, liste_malus_t * liste)
 * \brief fonction qui met a jour la liste des malus
 *
 * \param jour_nuit Cycle jour/nuit
 * \param tour Numero du tour actuel
 * \param liste Liste de tout les malus
 *
 * \return void pas de retour
 *
 */
extern
void liste_malus_actualiser()
{

  int duree;
  for(int i=0; i<liste_malus->nb_malus; i++)
  {
    duree = liste_malus->malus_liste[i]->decrementer(liste_malus->malus_liste[i]);
    /*Si la durée après la decrementation est égal à 0, alors je supprime le malus*/
    if(duree == 0)
    {
      liste_malus_supprimer(i);
    }
  }
}

/*Vider une liste de malus*/
/**
 * \fn void liste_malus_vider(liste_malus_t * liste)
 * \brief fonction qui met supprime tout les malus de la liste
 *
 * \param liste Liste de tout les malus
 *
 * \return void pas de retour
 *
 */
extern
void liste_malus_vider()
{
  int i;
  for(i=0; i<liste_malus->nb_malus; i++)
  {
    liste_malus->malus_liste[i]->detruire(&(liste_malus->malus_liste[i]));
  }
  liste_malus->nb_malus = 0;
}

/*Destruction des listes*/
/**
 * \fn err_t liste_malus_detruire(liste_malus_t ** liste)
 * \brief fonction qui detruit la liste des malus
 *
 * \param liste Liste de tout les malus
 *
 * \return Ok
 *
 */
extern
err_t liste_malus_detruire()
{
  /*je remet à nulle tous les pointeurs de la liste, mais je ne les free() pas, car les personnages pointés doivent resté en mémoire*/

  for(int i = 0; i<MAX_MALUS; i++)
  {
    free(liste_malus->malus_liste[i]);
    liste_malus->malus_liste[i] = NULL;
  }


  free(liste_malus->malus_liste);
  liste_malus->malus_liste = NULL;
  free(liste_malus);
  liste_malus = NULL;

  return(Ok);
}


/*Définiton des fonctions*/

/*suppression d'un malus*/
/**
 * \fn void liste_malus_supprimer(liste_malus_t * liste, int ind)
 * \brief fonction qui supprime un malus de la liste
 *
 * \param liste Liste de tout les malus
 * \param ind Indice du malus a supprimer
 *
 * \return void pas de retour
 *
 */
extern
void liste_malus_supprimer(int ind)
{

  if(!(liste_malus_vide(liste_malus)))
    liste_malus->malus_liste[ind]->detruire(&(liste_malus->malus_liste[ind]));

  /*Lorsque je supprime un malus, si il est au milieu de la liste, je dois recoler les malus suivant*/
  for(int i=ind; i<(liste_malus->nb_malus)-1; i++)
  {
    liste_malus->malus_liste[i] = liste_malus->malus_liste[i+1];
  }

  liste_malus->nb_malus--;
}

/**
 * \fn bool liste_malus_pleine(liste_malus_t * liste)
 * \brief fonction qui test si la liste des malus est pleine
 *
 * \param liste Liste de tout les malus
 *
 * \return bool
 *
 */
extern
bool liste_malus_pleine()
{
  return (liste_malus->nb_malus == MAX_MALUS);
}

/**
 * \fn bool liste_malus_vide(liste_malus_t * liste)
 * \brief fonction qui test si la liste des malus est vide
 *
 * \param liste Liste de tout les malus
 *
 * \return bool
 *
 */
extern
bool liste_malus_vide()
{
  return (liste_malus->nb_malus == 0);
}

/*
= malloc(sizeof(liste_malus_t));

liste_malus->nb_malus = 0;
liste_malus->malus_liste = malloc(sizeof(malus_t*)*MAX_MALUS);
for(int i=0; i<MAX_MALUS; i++)
{
 liste_malus->malus_liste[i] = NULL;
}
*/
