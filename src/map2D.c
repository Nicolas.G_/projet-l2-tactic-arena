/**
 * \file map2D.c
 * \brief Fichier pour charger et  gerer la map
 * \author Nicolas
 * \version 0.8
 * \date  12 mars 2021
 *
 * Affiche la map avec SDL2 a partir d'un fichier
 */

#include "../include/map2D.h"

/**
 * \fn int game (void)
 * \param self
 * \brief Entrée du programme de jeu utilise toute les fonctions
 *  pour afficher deplacer les perssonnage faire les attaques afficher toutes les information necesaire
 *
 * \return 1 on rejout | 0 fin 
 */
int game(){
    /**_*/

    srand(time(NULL));

    t_temps cycle_jour_nuit = jour;//variable indiquant le moment de la journer

	equipe_t tour_en_cours = Rouge;//indique quel equipe joue

    liste_perso_t *equipe_rouge = liste_perso_creer(NB_PERS);//contient l'equipe rouge
    liste_perso_t *equipe_bleu = liste_perso_creer(NB_PERS);//contient l'equipe bleu


    int tour_pas_fini = 0;//sert a savoir si le joueur a fini son tour
	int compteur_cycle = 1;//compte les cycle pour adapter cycle_jour_nuit
	int p_bleu = 0;//indice equipe bleu
	int p_rouge = 0;//indice equipe rouge
	personnage_t * joueur_courant; //Contient le joueur courant
	int nb_tours = 1;

    int i,j,k;

    int nb_arc = 0, nb_ass = 0, nb_heal = 0, nb_tank = 0, nb_epe = 0, nb_mage = 0;//Sert a verifier l'occurence de chaque classe

    equipe_t bleu = Bleu;
    equipe_t rouge = Rouge;

    int race[NB_PERS];
    int personnage;//race tableau ou l'on genere des race aleatoire

    liste_pos_t * liste_pos_deplacement;//va contenir la liste des deplacement possible du joueur_actuel
    t_pos pos_deplacement;//represente une seul position

    liste_pos_t * liste_pos_attaque_1;//va contenir la liste des position ateignable par l'attaque 1 du joueur_actuel
    liste_pos_t * liste_pos_attaque_2;//va contenir la liste des position ateignable par l'attaque 2 du joueur_actuel
    liste_pos_t * liste_pos_attaque_3;//va contenir la liste des position ateignable par l'attaque 3 joueur_actuel
    t_pos pos_attaque;//represente une seul position
    

    //On lit les fichier des equipes et on creer les liste des equipe avec les classe choisi precedement (selection.c)
    FILE * selection = fopen("../team_files/equipe_1.txt", "r");

    if(!selection){
        perror("Failed to load document\n");
        return EXIT_FAILURE;
    }

    //on creer des race aléatoire
    fscanf(selection,"%i",&personnage);
    for(i=0;i<NB_PERS;i++){
        race[i]=rand()%6+1;
        if(race[i]==6){
            race[i]=0;
        }
    }

    //ajout et creation de l'equipe rouge
    for(i=0;i<NB_PERS;i++){
        liste_ajouter_perso_equipe(personnage,race[i],equipe_rouge,rouge,&nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
        
        fscanf(selection,"%i",&personnage);
    }

    fclose(selection);

    //on recomence avec l'equipe bleu

    nb_arc = 0, nb_ass = 0, nb_heal = 0, nb_tank = 0, nb_epe = 0, nb_mage = 0;
    
    selection = fopen("../team_files/equipe_2.txt", "r");
    if(!selection){
        perror("Failed to load document\n");
        return EXIT_FAILURE;
    }

    fscanf(selection,"%i",&personnage);

    for(i=0;i<NB_PERS;i++){
        race[i]=rand()%6+1;
        if(race[i]==6){
            race[i]=0;
        }
    }

    for(i=0;i<NB_PERS;i++){
        liste_ajouter_perso_equipe(personnage,race[i],equipe_bleu,bleu,&nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
        
        fscanf(selection,"%i",&personnage);
    }

    fclose(selection);    

    //on active les different bonus/malus

    liste_malus_initialiser();
    
	race_activer(equipe_rouge);
    
	race_activer(equipe_bleu);

    //On charge aleatoirement un fichier de map
    if(chargement_carte())
    {
        printf("Failed to load map\n");
        return EXIT_FAILURE;
    }

    //on place les equipe sur la carte
    placer_equipes(equipe_rouge,equipe_bleu);
    

    /*Définition des variables SDL*/
    //Variable de base fenetre rendu positon et les surface de tuile
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Surface *mur = NULL;
    SDL_Surface *sol = NULL;
    SDL_Surface *claie = NULL;
    SDL_Surface *purple =NULL;
    SDL_Surface *icon = NULL;
    SDL_Surface *screen = NULL;
    SDL_Surface *cache = NULL;
    SDL_Rect position;

    //image Perssonage Rouge toute direction

    //BAS
    SDL_Surface *ImageArcherRougeBas = NULL;
    SDL_Surface *ImageAssassinRougeBas = NULL;
    SDL_Surface *ImageEpeisteRougeBas = NULL;
    SDL_Surface *ImageHealerRougeBas = NULL;
    SDL_Surface *ImageMageRougeBas = NULL;
    SDL_Surface *ImageTankRougeBas = NULL;

    //HAUT
    SDL_Surface *ImageArcherRougeHaut = NULL;
    SDL_Surface *ImageAssassinRougeHaut = NULL;
    SDL_Surface *ImageEpeisteRougeHaut = NULL;
    SDL_Surface *ImageHealerRougeHaut = NULL;
    SDL_Surface *ImageMageRougeHaut = NULL;
    SDL_Surface *ImageTankRougeHaut = NULL;

    //GAUCHE
    SDL_Surface *ImageArcherRougeGauche = NULL;
    SDL_Surface *ImageAssassinRougeGauche = NULL;
    SDL_Surface *ImageEpeisteRougeGauche = NULL;
    SDL_Surface *ImageHealerRougeGauche = NULL;
    SDL_Surface *ImageMageRougeGauche = NULL;
    SDL_Surface *ImageTankRougeGauche = NULL;

    //DROITE
    SDL_Surface *ImageArcherRougeDroite = NULL;
    SDL_Surface *ImageAssassinRougeDroite = NULL;
    SDL_Surface *ImageEpeisteRougeDroite = NULL;
    SDL_Surface *ImageHealerRougeDroite = NULL;
    SDL_Surface *ImageMageRougeDroite = NULL;
    SDL_Surface *ImageTankRougeDroite = NULL;
    
    //image Perssonage Bleu toute direction

    //HAUT
    SDL_Surface *ImageArcherBleuHaut = NULL;
    SDL_Surface *ImageAssassinBleuHaut = NULL;
    SDL_Surface *ImageEpeisteBleuHaut = NULL;
    SDL_Surface *ImageHealerBleuHaut = NULL;
    SDL_Surface *ImageMageBleuHaut = NULL;
    SDL_Surface *ImageTankBleuHaut = NULL;

    //BAS
    SDL_Surface *ImageArcherBleuBas = NULL;
    SDL_Surface *ImageAssassinBleuBas = NULL;
    SDL_Surface *ImageEpeisteBleuBas = NULL;
    SDL_Surface *ImageHealerBleuBas = NULL;
    SDL_Surface *ImageMageBleuBas = NULL;
    SDL_Surface *ImageTankBleuBas = NULL;

    //GAUCHE
    SDL_Surface *ImageArcherBleuGauche = NULL;
    SDL_Surface *ImageAssassinBleuGauche = NULL;
    SDL_Surface *ImageEpeisteBleuGauche = NULL;
    SDL_Surface *ImageHealerBleuGauche = NULL;
    SDL_Surface *ImageMageBleuGauche = NULL;
    SDL_Surface *ImageTankBleuGauche = NULL;

    //DROITE
    SDL_Surface *ImageArcherBleuDroite = NULL;
    SDL_Surface *ImageAssassinBleuDroite = NULL;
    SDL_Surface *ImageEpeisteBleuDroite = NULL;
    SDL_Surface *ImageHealerBleuDroite = NULL;
    SDL_Surface *ImageMageBleuDroite = NULL;
    SDL_Surface *ImageTankBleuDroite = NULL;
    
    //Surface du papier pein en fond
    SDL_Surface *background = NULL;

    //Surface pour les text sur les attaque (et plus par economie de resources)
    SDL_Surface *attaque_1 = NULL;
    SDL_Surface *attaque_2 = NULL;
    SDL_Surface *attaque_3 = NULL;

    SDL_Surface *infoAttaque_1 = NULL;
    SDL_Surface *infoAttaque_2 = NULL;
    //SDL_Surface *infoAttaque_3 = NULL;

    //Surface pour les divers texte 
    SDL_Surface *texteDeplacement = NULL;
    SDL_Surface *textFinTour = NULL;
    SDL_Surface *textTourActuel = NULL;
    SDL_Surface *textAttaque = NULL;
    SDL_Surface *textinfo = NULL;

    //Surface pour les boutton
    SDL_Surface *buttonPause = NULL;
    SDL_Surface *buttonAbandon = NULL;

    //image lors de la pause
    SDL_Surface *pause_foreground = NULL;   

    //variable pour ecrire du text et la couleur
    TTF_Font *font = NULL;
    TTF_Font *small_font = NULL;
    TTF_Font *simple_font = NULL;

    SDL_Color colorFont = {255,255,255};
    SDL_Color colorOver = {0,0,0};

    //entier pour verifier si l'on a deja affichier les information (sert a economiser les ressource)
    int showAtt=0,action=0,showInfoAtt=0,showInfoJ=0,abandon=0;
    //Tableau de caractere pour stocker des message avec des variable (va avec le TTF)
    char tourJ[100];
    char attJ[100];
    char info[100];

    //differente variable pour afficher au bonne place les texte
    SDL_Rect placeAtt_1={20,590},placeAtt_2={20,630},placeAtt_3={20,670},placeTextDeplacement={350,630},placetextFinT={520,630},placetextTourA={750,630},placeDetailAtt={710,320},posInfoJ={710,50},placeButton={950,5},placeCache={900,265};

    //variable pour savoir si on a deja recuperer pour les differente attaques et les deplacement du joueur_actuel
    int getDeplacement=0,getAtt_1=0,getAtt_2=0,getAtt_3=0;

    //boolen pour savoir si on est en pause ou pas
    bool pause=false;

    //sert a recuperer les information quand une attaque est lancer
    statut_att nouvelle_attaque;

    //initialisation des modules d'image et video et de texte de la SDL
    if(SDL_Init(SDL_INIT_VIDEO)!=0)
        SDLErrorr("Initalisation SDL");

    if(IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG)
    {
        SDLErrorr("Failed initialisation SDL Image");
        return EXIT_FAILURE;
    }

    if(TTF_Init()!=0)
        SDLErrorr("Failed initialisation of TTF");

    //Creation de la fenetre et du rendu
    window=SDL_CreateWindow("map2D",SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, TAILLE_BLOC*N+500, TAILLE_BLOC*M+10, 0);
    if(window==NULL)
        SDLErrorr("Impossible de creer la fenêtre");

    renderer = SDL_CreateRenderer(window,-1,0);

    //chargement de la police differente taille
    font = TTF_OpenFont("../fonts/Cardinal.ttf",30);
    if(font==NULL)
        SDLErrorr("Failed to load font");

    small_font = TTF_OpenFont("../fonts/Cardinal.ttf",26);
    if(small_font==NULL)
        SDLErrorr("Failed to load font");
    
    simple_font = TTF_OpenFont("../fonts/DejaVuSans.ttf",20);
    if(simple_font==NULL)
        SDLErrorr("Failed to load font");

    //Defini le titre et l'icone de la fenetre
    SDL_SetWindowTitle(window,"Tactis Arena");

    icon = chargementImage(icon,"../images/icon.png");
    
    SDL_SetWindowIcon(window,icon);

    //recupere l'affichage de la fenetre
    if((screen = SDL_GetWindowSurface(window))==NULL)
        SDLErrorr("impossible de créer la surface à partir de la fenêtre");

    background = chargementImage(background,"../images/map/background.png");

    //chargement des images de tuiles de map
    
    sol = chargementImage(sol,"../images/map/sol.png");
    mur = chargementImage(mur,"../images/map/mur.png");
    claie = chargementImage(claie,"../images/map/claie.png");
    purple = chargementImage(purple,"../images/map/purple.png");

    //chargement cache
    cache = chargementImage(cache,"../images/map/cache.png");

    //chargement buttons

    buttonPause = chargementImage(buttonPause,"../images/map/abandon.png");
    buttonAbandon = chargementImage(buttonPause,"../images/map/pause.png");
    pause_foreground = chargementImage(pause_foreground,"../images/map/pause_foreground.png");
    
    //chargement des tuiles personnages rouges (toute direction)
    
    //BAS
    ImageArcherRougeBas = chargementImage(ImageArcherRougeBas,"../images/perso/red_archer_bas.png");
    ImageAssassinRougeBas = chargementImage(ImageAssassinRougeBas,"../images/perso/red_assassin_bas.png");
    ImageEpeisteRougeBas = chargementImage(ImageEpeisteRougeBas,"../images/perso/red_epeiste_bas.png");
    ImageHealerRougeBas = chargementImage(ImageHealerRougeBas,"../images/perso/red_healer_bas.png");
    ImageMageRougeBas = chargementImage(ImageMageRougeBas,"../images/perso/red_mage_bas.png");
    ImageTankRougeBas = chargementImage(ImageTankRougeBas,"../images/perso/red_tank_bas.png");

    //HAUT
    ImageArcherRougeHaut = chargementImage(ImageArcherRougeHaut,"../images/perso/red_archer_haut.png");
    ImageAssassinRougeHaut = chargementImage(ImageAssassinRougeHaut,"../images/perso/red_assassin_haut.png");
    ImageEpeisteRougeHaut = chargementImage(ImageEpeisteRougeHaut,"../images/perso/red_epeiste_haut.png");
    ImageHealerRougeHaut = chargementImage(ImageHealerRougeHaut,"../images/perso/red_healer_haut.png");
    ImageMageRougeHaut = chargementImage(ImageMageRougeHaut,"../images/perso/red_mage_haut.png");
    ImageTankRougeHaut = chargementImage(ImageTankRougeHaut,"../images/perso/red_tank_haut.png");

    //GAUCHE
    ImageArcherRougeGauche = chargementImage(ImageArcherRougeGauche,"../images/perso/red_archer_gauche.png");
    ImageAssassinRougeGauche = chargementImage(ImageAssassinRougeGauche,"../images/perso/red_assassin_gauche.png");
    ImageEpeisteRougeGauche = chargementImage(ImageEpeisteRougeGauche,"../images/perso/red_epeiste_gauche.png");
    ImageHealerRougeGauche = chargementImage(ImageHealerRougeGauche,"../images/perso/red_healer_gauche.png");
    ImageMageRougeGauche = chargementImage(ImageMageRougeGauche,"../images/perso/red_mage_gauche.png");
    ImageTankRougeGauche = chargementImage(ImageTankRougeGauche,"../images/perso/red_tank_gauche.png");

    //DROITE
    ImageArcherRougeDroite = chargementImage(ImageArcherRougeDroite,"../images/perso/red_archer_droite.png");
    ImageAssassinRougeDroite = chargementImage(ImageAssassinRougeDroite,"../images/perso/red_assassin_droite.png");
    ImageEpeisteRougeDroite = chargementImage(ImageEpeisteRougeDroite,"../images/perso/red_epeiste_droite.png");
    ImageHealerRougeDroite = chargementImage(ImageHealerRougeDroite,"../images/perso/red_healer_droite.png");
    ImageMageRougeDroite = chargementImage(ImageMageRougeDroite,"../images/perso/red_mage_droite.png");
    ImageTankRougeDroite = chargementImage(ImageTankRougeDroite,"../images/perso/red_tank_droite.png");
    
    
    //chargement des tuiles personnages bleu (toute direction)
      
    //HAUT
    ImageArcherBleuHaut = chargementImage(ImageArcherBleuHaut,"../images/perso/blue_archer_haut.png");
    ImageAssassinBleuHaut= chargementImage(ImageAssassinBleuHaut,"../images/perso/blue_assassin_haut.png");
    ImageEpeisteBleuHaut = chargementImage(ImageEpeisteBleuHaut,"../images/perso/blue_epeiste_haut.png");
    ImageHealerBleuHaut = chargementImage(ImageHealerBleuBas,"../images/perso/blue_healer_haut.png");
    ImageMageBleuHaut = chargementImage(ImageMageBleuHaut,"../images/perso/blue_mage_haut.png");
    ImageTankBleuHaut = chargementImage(ImageTankBleuHaut,"../images/perso/blue_tank_haut.png");
    
    //BAS
    ImageArcherBleuBas = chargementImage(ImageArcherBleuBas,"../images/perso/blue_archer_bas.png");
    ImageAssassinBleuBas= chargementImage(ImageAssassinBleuBas,"../images/perso/blue_assassin_bas.png");
    ImageEpeisteBleuBas = chargementImage(ImageEpeisteBleuBas,"../images/perso/blue_epeiste_bas.png");
    ImageHealerBleuBas = chargementImage(ImageHealerBleuBas,"../images/perso/blue_healer_bas.png");
    ImageMageBleuBas = chargementImage(ImageMageBleuBas,"../images/perso/blue_mage_bas.png");
    ImageTankBleuBas = chargementImage(ImageTankBleuBas,"../images/perso/blue_tank_bas.png");

    //GAUCHE
    ImageArcherBleuGauche = chargementImage(ImageArcherBleuGauche,"../images/perso/blue_archer_gauche.png");
    ImageAssassinBleuGauche= chargementImage(ImageAssassinRougeGauche,"../images/perso/blue_assassin_gauche.png");
    ImageEpeisteBleuGauche = chargementImage(ImageEpeisteBleuGauche,"../images/perso/blue_epeiste_gauche.png");
    ImageHealerBleuGauche = chargementImage(ImageHealerBleuGauche,"../images/perso/blue_healer_gauche.png");
    ImageMageBleuGauche = chargementImage(ImageMageBleuGauche,"../images/perso/blue_mage_gauche.png");
    ImageTankBleuGauche = chargementImage(ImageTankBleuGauche,"../images/perso/blue_tank_gauche.png");

    //DROITE
    ImageArcherBleuDroite = chargementImage(ImageArcherBleuDroite,"../images/perso/blue_archer_droite.png");
    ImageAssassinBleuDroite= chargementImage(ImageAssassinBleuDroite,"../images/perso/blue_assassin_droite.png");
    ImageEpeisteBleuDroite = chargementImage(ImageEpeisteBleuDroite,"../images/perso/blue_epeiste_droite.png");
    ImageHealerBleuDroite = chargementImage(ImageHealerBleuDroite,"../images/perso/blue_healer_droite.png");
    ImageMageBleuDroite = chargementImage(ImageMageBleuDroite,"../images/perso/blue_mage_droite.png");
    ImageTankBleuDroite = chargementImage(ImageTankBleuDroite,"../images/perso/blue_tank_droite.png");

    // Placement des objets à l'écran (fond et affichage de la map)
    SDL_BlitSurface(background,NULL,screen,NULL);
    displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
    

    //Gestion des évènements*********************************
    SDL_bool program_launched=SDL_TRUE;
    SDL_Event event;

    //on place le joueur courrant
    joueur_courant=equipe_rouge->liste[p_rouge++];

    //et on recupere sa liste de position
    liste_pos_deplacement=deplacement_valide(joueur_courant);

    while(program_launched){   

        //Test de fin de partie
        if(equipe_bleu->nb_vivants == 0 || equipe_rouge->nb_vivants == 0){
            program_launched=SDL_FALSE;
            abandon=1;
	    }

        //Si le tour est finie on recupere les attaque du nouveau joueur_actuel
        if(tour_pas_fini==0){
            liste_pos_attaque_1 = attaque_valide(joueur_courant,joueur_courant->info_comp1().portee);
            liste_pos_attaque_2 = attaque_valide(joueur_courant,joueur_courant->info_comp2().portee);
            liste_pos_attaque_3 = attaque_valide(joueur_courant,joueur_courant->info_comp3().portee);
            displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
            tour_pas_fini=1;
        }

        //Si le perssonage courant a lancer une attaque et c'est tuer (met fin au tour)
        if((getAtt_1||getAtt_2||getAtt_3)&&joueur_courant->pt_attaque==0&&joueur_courant->etat==Mort){
            printf("KILL YOURSELF\n");
            //On change l'equipe et on passe au prochain joueur vivant
            if(tour_en_cours==Rouge){
                if(equipe_bleu->nb_vivants>0){
                    do{
                        if(p_bleu==NB_PERS)
                            p_bleu=0;
                        joueur_courant=equipe_bleu->liste[p_bleu++];
                    }
                    while(joueur_courant->etat==Mort);
                }
                
                tour_en_cours=Bleu;
            }
            else{
                if(equipe_bleu->nb_vivants>0){
                    do{
                        if(p_rouge==NB_PERS)
                            p_rouge=0;
                        joueur_courant=equipe_rouge->liste[p_rouge++];
                    }
                    while(joueur_courant->etat==Mort);
                }
                tour_en_cours=Rouge;
            }

            //Test de fin de partie
            if(equipe_bleu->nb_vivants == 0 || equipe_rouge->nb_vivants == 0){
                program_launched=SDL_FALSE;
                abandon=1;
            }

            printf("end loops DONE\n");

            //On enleve les marqueur de deplacement et on les place pour le nouveau perssonage
            retirer_marqueurs_deplacements_carte(liste_pos_deplacement);
            liste_pos_deplacement=deplacement_valide(joueur_courant);
            ajouter_marqueurs_deplacements_carte(liste_pos_deplacement);

            //on retirer les marqueur d'attaque
            retirer_marqueurs_attaque_carte(liste_pos_attaque_1);
            retirer_marqueurs_attaque_carte(liste_pos_attaque_2);
            retirer_marqueurs_attaque_carte(liste_pos_attaque_3);

            //on met a jour les malus
            liste_malus_actualiser();

            //on reactualise toute les valeur essentiel et on incremente le nombre de tour
            joueur_courant->pt_deplacement=1;
            joueur_courant->pt_attaque=1;
            getAtt_1=getAtt_2=getAtt_3=getDeplacement=showAtt=showInfoAtt=showInfoJ=0;
            tour_pas_fini=0;
            nb_tours++;
            action=0;
            placeDetailAtt.y=320;

            SDL_BlitSurface(background,NULL,screen,NULL);
        }

        //Si une attaque a ete lancer et qu'elle n'a pas echouer
        if(joueur_courant->pt_attaque==0 && showInfoAtt==0 && nouvelle_attaque.etat!=Echec){
            showInfoAtt=1;
            afficher_statut_attaque(nouvelle_attaque);

            SDL_BlitSurface(background,NULL,screen,NULL);

            //On recupere le nom du lanceur et de l'attaque lancer et on blit les information
            sprintf(attJ,"%s lance une attaque !",nouvelle_attaque.lanceur->nom);
            sprintf(info,"Nom : %s",nouvelle_attaque.nom_attaque);

            textAttaque = TTF_RenderUTF8_Solid(simple_font,attJ,colorFont);
            textinfo = TTF_RenderUTF8_Solid(simple_font,info,colorFont);

            SDL_BlitSurface(textAttaque,NULL,screen,&placeDetailAtt); 
            placeDetailAtt.y+=40;
            SDL_BlitSurface(textinfo,NULL,screen,&placeDetailAtt);
            placeDetailAtt.y+=40;

            //Si l'attaque a reussi et qu'elle a fait des degat on affiche combien
            if(nouvelle_attaque.etat == Reussite && nouvelle_attaque.degats>0 && nouvelle_attaque.type!=Attaque && !nouvelle_attaque.attaque_esquivee[0]){
                infoAttaque_1 = TTF_RenderUTF8_Solid(simple_font,"Attaque réussie avec succès",colorFont);
                if(nouvelle_attaque.cibles[0]->nom != NULL){
                    sprintf(tourJ,"sur le joueur %s - %i PV",nouvelle_attaque.cibles[0]->nom,nouvelle_attaque.degats[0]);
                }                
                infoAttaque_2 = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            } 
            //Si l'attaque n'a pas fait de degat elle a ete esquiver
            if(nouvelle_attaque.etat == Reussite && nouvelle_attaque.degats<=0 && nouvelle_attaque.attaque_esquivee[0]){
                infoAttaque_1 = TTF_RenderUTF8_Solid(simple_font,"Attaque ésquivé",colorFont);
                if(nouvelle_attaque.cibles[0]->nom != NULL){
                    sprintf(tourJ,"par le joueur %s",nouvelle_attaque.cibles[0]->nom);
                }
                infoAttaque_2 = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            }
            //Si l'attaque est un sort de soin
            if(nouvelle_attaque.etat == Reussite && nouvelle_attaque.type[0]==Soin){
                infoAttaque_1 = TTF_RenderUTF8_Solid(simple_font,"Soin Réussi avec succès",colorFont);
                 if(nouvelle_attaque.cibles[0]->nom != NULL){
                    sprintf(tourJ,"sur le joueur %s + %i PV",nouvelle_attaque.cibles[0]->nom,nouvelle_attaque.degats[0]);
                }
                infoAttaque_2 = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            }
            //Si il n'y avait perssone sur la case 
            if(nouvelle_attaque.etat == SansCible)
                infoAttaque_1 = TTF_RenderUTF8_Solid(simple_font,"L'attaque n'a touché personne",colorFont);

            //on blit les information et on recharge la map (va aussi afficher les info)
            SDL_BlitSurface(infoAttaque_1,NULL,screen,&placeDetailAtt);
            placeDetailAtt.y+=40;
            if(nouvelle_attaque.etat!=SansCible)
                SDL_BlitSurface(infoAttaque_2,NULL,screen,&placeDetailAtt);    
            displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
            showAtt=0;
        }

        if(showAtt==0){
            //Affiche le noms des attaque du perssonage actuel
            attaque_1 = TTF_RenderUTF8_Solid(font,joueur_courant->info_comp1().nom,colorFont);
            attaque_2 = TTF_RenderUTF8_Solid(font,joueur_courant->info_comp2().nom,colorFont);
            attaque_3 = TTF_RenderUTF8_Solid(font,joueur_courant->info_comp3().nom,colorFont);

            //Affiche le texte de deplacement de fin de tour et du joueur actuel
            texteDeplacement = TTF_RenderUTF8_Solid(font,"Déplacements",colorFont);
            textFinTour = TTF_RenderUTF8_Solid(font,"Fin du tour",colorFont);
            sprintf(tourJ,"C'est au tour de %s J%i",joueur_courant->nom,joueur_courant->equipe+1);
            textTourActuel = TTF_RenderUTF8_Solid(font,tourJ,colorFont);

            SDL_BlitSurface(textTourActuel,NULL,screen,&placetextTourA);
            SDL_BlitSurface(attaque_1,NULL,screen,&placeAtt_1);
            SDL_BlitSurface(attaque_2,NULL,screen,&placeAtt_2);
            SDL_BlitSurface(attaque_3,NULL,screen,&placeAtt_3);
            SDL_BlitSurface(texteDeplacement,NULL,screen,&placeTextDeplacement);
            SDL_BlitSurface(textFinTour,NULL,screen,&placetextFinT);

            placeButton.x=950;
            SDL_BlitSurface(buttonPause,NULL,screen,&placeButton);
            placeButton.x+=60;
            SDL_BlitSurface(buttonAbandon,NULL,screen,&placeButton);

            //On affiche toute les infos
            SDL_UpdateWindowSurface(window);
            showAtt=1;
        }

        if(showInfoJ==0){
            //On affiche les information du joueur courant 
            sprintf(tourJ,"Nom : %s",joueur_courant->nom);
            textinfo = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            sprintf(tourJ,"Race : %s",joueur_courant->race->nom_race);
            textTourActuel = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            switch(joueur_courant->classe){
                case 0:
                    sprintf(tourJ,"Classe : Archer");
                    break;
                case 1:
                    sprintf(tourJ,"Classe : Mage");
                    break;
                case 2:
                    sprintf(tourJ,"Classe : Tank");
                    break;
                case 3:
                    sprintf(tourJ,"Classe : Assassin");
                    break;
                case 4:
                    sprintf(tourJ,"Classe : Healer");
                    break;
                case 5:
                    sprintf(tourJ,"Classe : Epeiste");
                    break;
            }
            attaque_3 = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            
            posInfoJ.y=50;
            SDL_BlitSurface(textinfo,NULL,screen,&posInfoJ);
            posInfoJ.y+=35;
            SDL_BlitSurface(textTourActuel,NULL,screen,&posInfoJ); 
            posInfoJ.y+=35;
            SDL_BlitSurface(attaque_3,NULL,screen,&posInfoJ);
            posInfoJ.y+=35;

            sprintf(tourJ,"PV : %i",joueur_courant->pv);
            textinfo = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            sprintf(tourJ,"Déplacements : %i",joueur_courant->portee.deplacement);
            textTourActuel = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            sprintf(tourJ,"Esquive : %.0f %%",joueur_courant->esquive*10);
            attaque_3 = TTF_RenderUTF8_Solid(simple_font,tourJ,colorFont);
            SDL_BlitSurface(textinfo,NULL,screen,&posInfoJ);     
            posInfoJ.y+=35;
            SDL_BlitSurface(textTourActuel,NULL,screen,&posInfoJ); 
            posInfoJ.y+=35;
            SDL_BlitSurface(attaque_3,NULL,screen,&posInfoJ);


            //On met a jour pour tout afficher
            SDL_UpdateWindowSurface(window);      
            
            
            showInfoJ=1;

        }
        
        //Gestion evenement
        while (SDL_PollEvent(&event))
        {

            switch (event.type)
            {
            //Ferme le programme si on appuis sur la croix
            case SDL_QUIT:
                program_launched=SDL_FALSE;
                break;
            //gestion clavier pour quitter touche echap
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    program_launched=SDL_FALSE;
                    break;
                default:
                    continue;
                }
                break;
            
            case SDL_MOUSEMOTION:
                if(pause==false){
                    for(i=0;i<N;i++){
                        for(j=0;j<M;j++){
                            if(map[j][i].type_emplacement==PERSO){
                                for(k=0;k<NB_PERS;k++){
                                    if((((event.motion.x>=equipe_rouge->liste[k]->pos_y*TAILLE_BLOC)&&(event.motion.x<=equipe_rouge->liste[k]->pos_y*TAILLE_BLOC+TAILLE_BLOC))&&((event.motion.y>=equipe_rouge->liste[k]->pos_x*TAILLE_BLOC)&&(event.motion.y<=equipe_rouge->liste[k]->pos_x*TAILLE_BLOC+TAILLE_BLOC))) || (((event.motion.x>=equipe_bleu->liste[k]->pos_y*TAILLE_BLOC)&&(event.motion.x<=equipe_bleu->liste[k]->pos_y*TAILLE_BLOC+TAILLE_BLOC))&&((event.motion.y>=equipe_bleu->liste[k]->pos_x*TAILLE_BLOC)&&(event.motion.y<=equipe_bleu->liste[k]->pos_x*TAILLE_BLOC+TAILLE_BLOC)))){
                                        if(((event.motion.x>=equipe_rouge->liste[k]->pos_y*TAILLE_BLOC)&&(event.motion.x<=equipe_rouge->liste[k]->pos_y*TAILLE_BLOC+TAILLE_BLOC))&&((event.motion.y>=equipe_rouge->liste[k]->pos_x*TAILLE_BLOC)&&(event.motion.y<=equipe_rouge->liste[k]->pos_x*TAILLE_BLOC+TAILLE_BLOC)))
                                            sprintf(tourJ,"PV du joueur survolé : %i",equipe_rouge->liste[k]->pv);
                                        else if(((event.motion.x>=equipe_bleu->liste[k]->pos_y*TAILLE_BLOC)&&(event.motion.x<=equipe_bleu->liste[k]->pos_y*TAILLE_BLOC+TAILLE_BLOC))&&((event.motion.y>=equipe_bleu->liste[k]->pos_x*TAILLE_BLOC)&&(event.motion.y<=equipe_bleu->liste[k]->pos_x*TAILLE_BLOC+TAILLE_BLOC)))
                                            sprintf(tourJ,"PV du joueur survolé : %i",equipe_bleu->liste[k]->pv);
                                        attaque_1 = TTF_RenderUTF8_Solid(simple_font,tourJ,colorOver);
                                        posInfoJ.y=275;
                                        SDL_BlitSurface(cache,NULL,screen,&placeCache);
                                        SDL_BlitSurface(attaque_1,NULL,screen,&posInfoJ);
                                        SDL_UpdateWindowSurface(window);
                                    }  
                                }
                            
                            }
                            
                        }
                    }   
                }
                break;
            
            case SDL_MOUSEBUTTONUP://Quand on clique
                if(pause==false){//test que l'on est pas en pause
                    //Si on clique sur texte de deplacement affiche la zone de deplacement du joueur actuel
                    if(((event.motion.x>=placeTextDeplacement.x)&&(event.motion.x<=placeTextDeplacement.x+145))&&((event.motion.y>=placeTextDeplacement.y)&&(event.motion.y<=placeTextDeplacement.y+35))){
                        printf("Deplacement\n");
                        if(getDeplacement==0){//On affiche seulement on affiche pas deja
                            if(joueur_courant->pt_deplacement>0){//Et que le joueur peux encore ce deplacer
                                SDL_BlitSurface(background,NULL,screen,NULL);
                                displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                                //on eneleve les marqueur d'attaque
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_1);
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_2);
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_3);
                                //on recupere et on affiche la zone de deplacement
                                liste_pos_deplacement=deplacement_valide(joueur_courant);
                                ajouter_marqueurs_deplacements_carte(liste_pos_deplacement);
                                displayZoneDeplacement(claie,screen,position,window);
                                getDeplacement=1;
                                getAtt_1=getAtt_2=getAtt_3=showAtt=showInfoJ=0;
                            }
                            
                        }

                    }
                        //On parcour la map
                        for(i=0;i<N;i++){
                            for(j=0;j<M;j++){
                                position.x = j * TAILLE_BLOC;
                                position.y = i * TAILLE_BLOC;
                                if(((event.motion.x>=position.x)&&(event.motion.x<=position.x+TAILLE_BLOC))&&((event.motion.y>=position.y)&&(event.motion.y<=position.y+TAILLE_BLOC))){
                                    pos_deplacement.x=i;
                                    pos_deplacement.y=j;
                                    pos_attaque.x=i;
                                    pos_attaque.y=j;
                                    if( getDeplacement==1 && joueur_courant->pt_deplacement > 0 && (effectuer_deplacement(joueur_courant,pos_deplacement)==0)){
                                        //Si le joueur et en mode deplacement et qu'il peux ce deplacer sur la case selectionner
                                        joueur_courant->pt_deplacement=0;//met a zero les point de deplacement
                                        //On enleve tout les marqueur
                                        retirer_marqueurs_deplacements_carte(liste_pos_deplacement);
                                        retirer_marqueurs_attaque_carte(liste_pos_attaque_1);
                                        retirer_marqueurs_attaque_carte(liste_pos_attaque_2);
                                        retirer_marqueurs_attaque_carte(liste_pos_attaque_3);
                                        //Et on recupere tout par sureter
                                        liste_pos_attaque_1 = attaque_valide(joueur_courant,joueur_courant->info_comp1().portee);
                                        liste_pos_attaque_2 = attaque_valide(joueur_courant,joueur_courant->info_comp2().portee);
                                        liste_pos_attaque_3 = attaque_valide(joueur_courant,joueur_courant->info_comp3().portee);
                                        getDeplacement=showInfoJ=0;
                                        SDL_BlitSurface(background,NULL,screen,NULL);
                                        displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                                        showAtt=0;
                                        action=1;
                                    }
                                    if(action==0&&joueur_courant->pt_attaque > 0 && getAtt_1){
                                        //Si on a recuperer les position de l'attaque 1 et que l'on peux attaquer
                                        if(lire_liste_attaques(joueur_courant,pos_attaque,joueur_courant->info_comp1().portee)==0){
                                            //si la case cliquer corespond a une case ou l'attaque touche
                                            joueur_courant->pt_attaque=0;
                                            //on effectue l'attaque
                                            nouvelle_attaque = effectuer_attaque(joueur_courant,pos_attaque,joueur_courant->info_comp1().nom,joueur_courant->info_comp1().degats_min,joueur_courant->info_comp1().degats_max,joueur_courant->info_comp1().zone,joueur_courant->info_comp1().type);
                                            retirer_marqueurs_attaque_carte(liste_pos_attaque_1);//on enleve les marqueur et on affiche la map                         
                                            displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                                            printf("attaque 1 DONE \n");
                                        }
                                    }
                                    if(action==0&&joueur_courant->pt_attaque > 0 && getAtt_2){
                                        //Si on a recuperer les position de l'attaque 2 et que l'on peux attaquer
                                        if(lire_liste_attaques(joueur_courant,pos_attaque,joueur_courant->info_comp2().portee)==0){
                                            //si la case cliquer corespond a une case ou l'attaque touche
                                            joueur_courant->pt_attaque=0;
                                            //on effectue l'attaque
                                            nouvelle_attaque = effectuer_attaque(joueur_courant,pos_attaque,joueur_courant->info_comp2().nom,joueur_courant->info_comp2().degats_min,joueur_courant->info_comp2().degats_max,joueur_courant->info_comp2().zone,joueur_courant->info_comp2().type);
                                            retirer_marqueurs_attaque_carte(liste_pos_attaque_2);//on enleve les marqueur et on affiche la map 
                                            displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                                            printf("attaque 2 DONE \n");
                                        }
                                    }
                                    if(action==0&&joueur_courant->pt_attaque > 0 && getAtt_3){
                                        //Si on a recuperer les position de l'attaque 3 et que l'on peux attaquer
                                        if(lire_liste_attaques(joueur_courant,pos_attaque,joueur_courant->info_comp3().portee)==0){
                                            //si la case cliquer corespond a une case ou l'attaque touche
                                            joueur_courant->pt_attaque=0;
                                            //on effectue l'attaque
                                            nouvelle_attaque = effectuer_attaque(joueur_courant,pos_attaque,joueur_courant->info_comp3().nom,joueur_courant->info_comp3().degats_min,joueur_courant->info_comp3().degats_max,joueur_courant->info_comp3().zone,joueur_courant->info_comp3().type);
                                            retirer_marqueurs_attaque_carte(liste_pos_attaque_3);//on enleve les marqueur et on affiche la map 
                                            displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                                            joueur_courant->comp_special=false;
                                            printf("attaque 3 DONE \n");
                                        }
                                    }

                                }
                            }
                        }
                    
                    

                    if(((event.motion.x>=placetextFinT.x)&&(event.motion.x<=placetextFinT.x+120))&&((event.motion.y>=placetextFinT.y)&&(event.motion.y<=placetextFinT.y+35))){
                        printf("END\n");
                        //Si on clique sur Fin Du Tour
                        //On change l'equipe et on passe au prochain joueur vivant
                        if(tour_en_cours==Rouge){
                            if(equipe_bleu->nb_vivants>0){
                                do{
                                    if(p_bleu==NB_PERS)
                                        p_bleu=0;
                                    joueur_courant=equipe_bleu->liste[p_bleu++];
                                }
                                while(joueur_courant->etat==Mort);
                            }
                            
                            tour_en_cours=Bleu;
                        }
                        else{
                            if(equipe_bleu->nb_vivants>0){
                                do{
                                    if(p_rouge==NB_PERS)
                                        p_rouge=0;
                                    joueur_courant=equipe_rouge->liste[p_rouge++];
                                }
                                while(joueur_courant->etat==Mort);
                            }
                            tour_en_cours=Rouge;
                        }

                        //Test de fin de partie
                        if(equipe_bleu->nb_vivants == 0 || equipe_rouge->nb_vivants == 0){
                            program_launched=SDL_FALSE;
                            abandon=1;
                        }

                        //Incremente et adapt le cycle journalier
                        if(compteur_cycle == 5){
                            compteur_cycle = 0;
                            switch(cycle_jour_nuit){
                                case jour:
                                    printf("C'est le jour !\n");
                                    cycle_jour_nuit = nuit;
                                    break;
                                case nuit:
                                    printf("C'est la nuit !\n");
                                    cycle_jour_nuit = jour;
                                    break;
                            }
                        }
                        compteur_cycle++;

                        printf("end loops DONE\n");
                        
                        //On enleve les marqueur de deplacement et on les place pour le nouveau perssonage
                        retirer_marqueurs_deplacements_carte(liste_pos_deplacement);
                        liste_pos_deplacement=deplacement_valide(joueur_courant);
                        ajouter_marqueurs_deplacements_carte(liste_pos_deplacement);

                        //on retirer les marqueur d'attaque
                        retirer_marqueurs_attaque_carte(liste_pos_attaque_1);
                        retirer_marqueurs_attaque_carte(liste_pos_attaque_2);
                        retirer_marqueurs_attaque_carte(liste_pos_attaque_3);

                        //on met a jour les malus
                        liste_malus_actualiser();

                        //on reactualise toute les valeur essentiel et on incremente le nombre de tour

                        joueur_courant->pt_deplacement=1;
                        joueur_courant->pt_attaque=1;
                        getAtt_1=getAtt_2=getAtt_3=getDeplacement=showAtt=showInfoAtt=showInfoJ=0;
                        tour_pas_fini=0;
                        nb_tours++;
                        action=0;
                        placeDetailAtt.y=320;

                        SDL_BlitSurface(background,NULL,screen,NULL);
                    }
                    
                    if(((event.motion.x>=placeAtt_1.x)&&(event.motion.x<=placeAtt_1.x+310))&&((event.motion.y>=placeAtt_1.y)&&(event.motion.y<=placeAtt_1.y+53))){
                        printf("Attaque 1\n");
                        //Si on clique sur la premier attaque du joueur actuel et que l'on peux attaquer
                        if(getAtt_1==0){
                            if(joueur_courant->pt_attaque>0){
                                SDL_BlitSurface(background,NULL,screen,NULL);
                                //Affiche les information de l'attaque numero 1
                                displayInfoAtt(simple_font,colorFont,screen,window,joueur_courant->info_comp1());
                                displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                                //retire tout les marqueur
                                retirer_marqueurs_deplacements_carte(liste_pos_deplacement);
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_2);
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_3);
                                getDeplacement=getAtt_2=getAtt_3=action=showAtt=showInfoJ=0;
                                getAtt_1=1;
                                //ajoute seulement les marqueur de l'attaque 1
                                ajouter_marqueurs_attaque_carte(liste_pos_attaque_1);
                                //affiche avec les tuile violet les case attaquable
                                displayZoneAttaque(purple,screen,position,window);                            
                            }
                        }
                            
                    }

                    if(((event.motion.x>=placeAtt_2.x)&&(event.motion.x<=placeAtt_2.x+310))&&((event.motion.y>=placeAtt_2.y)&&(event.motion.y<=placeAtt_2.y+53))){
                        printf("Attaque 2\n");
                        //Si on clique sur la premier attaque du joueur actuel et que l'on peux attaquer
                        if(getAtt_2==0){
                            if(joueur_courant->pt_attaque>0){
                                //Affiche les information de l'attaque numero 2
                                SDL_BlitSurface(background,NULL,screen,NULL);
                                displayInfoAtt(simple_font,colorFont,screen,window,joueur_courant->info_comp2());
                                displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                                //retire tout les marqueur
                                retirer_marqueurs_deplacements_carte(liste_pos_deplacement);
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_1);
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_3);
                                getDeplacement=getAtt_1=getAtt_3=action=showAtt=showInfoJ=0;
                                getAtt_2=1;
                                //ajoute seulement les marqueur de l'attaque 2
                                ajouter_marqueurs_attaque_carte(liste_pos_attaque_2);
                                //affiche avec les tuile violet les case attaquable
                                displayZoneAttaque(purple,screen,position,window);        
                            }
                        }   
                    }

                    if(((event.motion.x>=placeAtt_3.x)&&(event.motion.x<=placeAtt_3.x+310))&&((event.motion.y>=placeAtt_3.y)&&(event.motion.y<=placeAtt_3.y+53))){
                        printf("Attaque 3\n");
                        //Si on clique sur la premier attaque du joueur actuel et que l'on peux attaquer
                        if(getAtt_3==0){
                            if(joueur_courant->pt_attaque>0){
                                //Affiche les information de l'attaque numero 3
                                SDL_BlitSurface(background,NULL,screen,NULL);
                                displayInfoAtt(simple_font,colorFont,screen,window,joueur_courant->info_comp3());
                                displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                                //retire tout les marqueur
                                retirer_marqueurs_deplacements_carte(liste_pos_deplacement);
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_1);
                                retirer_marqueurs_attaque_carte(liste_pos_attaque_2);
                                getDeplacement=getAtt_1=getAtt_2=action=showAtt=showInfoJ=0;
                                getAtt_3=1;
                                //ajoute seulement les marqueur de l'attaque 3
                                ajouter_marqueurs_attaque_carte(liste_pos_attaque_3);
                                //affiche avec les tuile violet les case attaquable
                                displayZoneAttaque(purple,screen,position,window);
                                
                            }
                        }    
                    }

                    if(((event.motion.x>=placeButton.x-50)&&(event.motion.x<=placeButton.x))&&((event.motion.y>=placeButton.y)&&(event.motion.y<=placeButton.y+50))){
                        printf("Boutton abandon\n");
                        //Quite si on abandonne
                        program_launched = SDL_FALSE;
                        abandon=1;
                    }
                }
                

                if(((event.motion.x>=placeButton.x)&&(event.motion.x<=placeButton.x+50))&&((event.motion.y>=placeButton.y)&&(event.motion.y<=placeButton.y+50))){
                    printf("Boutton pause\n");
                    //Affiche un ecrant de pause si on est pas en pause et l'enleve sinon
                    if(pause==false){
                        pause=true;
                        SDL_BlitSurface(pause_foreground,NULL,screen,NULL);
                    } 
                    else{
                        pause=false;
                        showInfoJ=0;
                        showAtt=0;
                        SDL_BlitSurface(background,NULL,screen,NULL);
                        displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
                    }
                    SDL_UpdateWindowSurface(window);
                        
                }

                break;
            }
        }
    }

//Affichage des score

    //Definition des police couleur et position du texte
    TTF_Font *endfont;
    SDL_Color endcolor={92,50,36};
    SDL_Rect endplace={10,100};

    endfont = TTF_OpenFont("../fonts/Cardinal.ttf",80);
    if(endfont==NULL)
        SDLErrorr("Failed to load font");

    //Seulement si la partie est fini ne ce lance pas si on quite avec echap
    if(abandon==1){
        //Affiche le nombre de tour qu'a durer la partie et l'equipe gagnante
        program_launched=SDL_TRUE;      
        sprintf(tourJ,"La partie a durée %i tours",nb_tours);
        attaque_1 = TTF_RenderUTF8_Solid(endfont,tourJ,endcolor);
        SDL_BlitSurface(background,NULL,screen,NULL);
        displayMap(sol,mur,screen,window,position,ImageArcherRougeBas,ImageAssassinRougeBas,ImageEpeisteRougeBas,ImageHealerRougeBas,ImageMageRougeBas,ImageTankRougeBas,ImageArcherRougeGauche,ImageAssassinRougeGauche,ImageEpeisteRougeGauche,ImageHealerRougeGauche,ImageMageRougeGauche,ImageTankRougeGauche,ImageArcherRougeDroite,ImageArcherRougeDroite,ImageEpeisteRougeDroite,ImageHealerRougeDroite,ImageMageRougeDroite,ImageTankRougeDroite,ImageArcherRougeHaut,ImageAssassinRougeHaut,ImageEpeisteRougeHaut,ImageHealerRougeHaut,ImageMageRougeHaut,ImageTankRougeHaut,ImageArcherBleuHaut,ImageAssassinBleuHaut,ImageArcherBleuHaut,ImageHealerBleuHaut,ImageMageBleuHaut,ImageTankBleuHaut,ImageArcherBleuBas,ImageAssassinBleuBas,ImageEpeisteRougeBas,ImageHealerBleuBas,ImageMageBleuBas,ImageTankBleuBas,ImageArcherBleuGauche,ImageAssassinBleuGauche,ImageEpeisteBleuGauche,ImageHealerBleuGauche,ImageMageBleuGauche,ImageTankBleuGauche,ImageArcherBleuDroite,ImageAssassinBleuDroite,ImageEpeisteBleuDroite,ImageHealerBleuDroite,ImageMageBleuDroite,ImageTankBleuDroite);
        SDL_BlitSurface(attaque_1,NULL,screen,&endplace);

        endplace.y+=100;

        if(equipe_bleu->nb_vivants == 0 || equipe_rouge->nb_vivants == 0){
		    if(equipe_bleu->nb_vivants == 0){
		    	attaque_1 = TTF_RenderUTF8_Solid(endfont,"Victoire de l'équipe bleue\n",endcolor);
		    }
		    if(equipe_rouge->nb_vivants == 0) {
		    	attaque_1 = TTF_RenderUTF8_Solid(endfont,"Victoire de l'équipe rouge\n",endcolor);
		    }
            
            SDL_BlitSurface(attaque_1,NULL,screen,&endplace);
        }
        else{
            if(tour_en_cours==Bleu){
                attaque_1 = TTF_RenderUTF8_Solid(endfont,"Victoire de l'équipe rouge\n",endcolor);
            }
            else{
                attaque_1 = TTF_RenderUTF8_Solid(endfont,"Victoire de l'équipe bleue\n",endcolor);
            }
            SDL_BlitSurface(attaque_1,NULL,screen,&endplace);
        }
        endplace.x+=80;
        endplace.y+=230;
        infoAttaque_1 = TTF_RenderUTF8_Solid(endfont,"Revenir au Menu",endcolor);
        SDL_BlitSurface(infoAttaque_1,NULL,screen,&endplace);
        SDL_UpdateWindowSurface(window);   
	}

    int replay=0;

    //Boucle pour laisser le temp a l'utilisateur de voir les resultat 
    while (program_launched){


        while (SDL_PollEvent(&event))
        {

            switch (event.type)
            {
            //Ferme le programme si on appuis sur la croix
            case SDL_QUIT:
                program_launched=SDL_FALSE;
                break;
            //gestion clavier pour quitter touche echap
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    program_launched=SDL_FALSE;
                    break;
                default:
                    continue;
                }
                break;

            case SDL_MOUSEBUTTONUP:
                if(((event.motion.x>=endplace.x)&&(event.motion.x<=endplace.x+510))&&((event.motion.y>=endplace.y)&&(event.motion.y<=endplace.y+80))){
                    printf("Back to Menu\n");
                    replay=1;
                    program_launched=SDL_FALSE;
                }
                break;
            }
        }
    }

    //liberation des persos et mise a null

    liste_detruire(&equipe_rouge);
    liste_detruire(&equipe_bleu);

    //libération des surfaces
    //liberation rouge

    //BAS
    SDL_FreeSurface(ImageArcherRougeBas);
    SDL_FreeSurface(ImageAssassinRougeBas);
    SDL_FreeSurface(ImageEpeisteRougeBas);
    SDL_FreeSurface(ImageHealerRougeBas);
    SDL_FreeSurface(ImageMageRougeBas);
    SDL_FreeSurface(ImageTankRougeBas);

    //HAUT
    SDL_FreeSurface(ImageArcherRougeHaut);
    SDL_FreeSurface(ImageAssassinRougeHaut);
    SDL_FreeSurface(ImageEpeisteRougeHaut);
    SDL_FreeSurface(ImageHealerRougeHaut);
    SDL_FreeSurface(ImageMageRougeHaut);
    SDL_FreeSurface(ImageTankRougeHaut);
    
    //GAUCHE
    SDL_FreeSurface(ImageArcherRougeGauche);
    SDL_FreeSurface(ImageAssassinRougeGauche);
    SDL_FreeSurface(ImageEpeisteRougeGauche);
    SDL_FreeSurface(ImageHealerRougeGauche);
    SDL_FreeSurface(ImageMageRougeGauche);
    SDL_FreeSurface(ImageTankRougeGauche);

    //DROITE
    SDL_FreeSurface(ImageArcherRougeDroite);
    SDL_FreeSurface(ImageAssassinRougeDroite);
    SDL_FreeSurface(ImageEpeisteRougeDroite);
    SDL_FreeSurface(ImageHealerRougeDroite);
    SDL_FreeSurface(ImageMageRougeDroite);
    SDL_FreeSurface(ImageTankRougeDroite);

    //liberation bleu

    //HAUT
    SDL_FreeSurface(ImageArcherBleuHaut);
    SDL_FreeSurface(ImageAssassinBleuHaut);
    SDL_FreeSurface(ImageEpeisteBleuHaut);
    SDL_FreeSurface(ImageHealerBleuHaut);
    SDL_FreeSurface(ImageMageBleuHaut);
    SDL_FreeSurface(ImageTankBleuHaut);

    //BAS
    SDL_FreeSurface(ImageArcherBleuBas);
    SDL_FreeSurface(ImageAssassinBleuBas);
    SDL_FreeSurface(ImageEpeisteBleuBas);
    SDL_FreeSurface(ImageHealerBleuBas);
    SDL_FreeSurface(ImageMageBleuBas);
    SDL_FreeSurface(ImageTankBleuBas);

    //GAUCHE
    SDL_FreeSurface(ImageArcherBleuGauche);
    SDL_FreeSurface(ImageAssassinBleuGauche);
    SDL_FreeSurface(ImageEpeisteBleuGauche);
    SDL_FreeSurface(ImageHealerBleuGauche);
    SDL_FreeSurface(ImageMageBleuGauche);
    SDL_FreeSurface(ImageTankBleuGauche);

    //DROITE
    SDL_FreeSurface(ImageArcherBleuDroite);
    SDL_FreeSurface(ImageAssassinBleuDroite);
    SDL_FreeSurface(ImageEpeisteBleuDroite);
    SDL_FreeSurface(ImageHealerBleuDroite);
    SDL_FreeSurface(ImageMageBleuDroite);
    SDL_FreeSurface(ImageTankBleuDroite);    
    
    //Surface utiliser pour afficher la map et zone
    SDL_FreeSurface(sol);
    SDL_FreeSurface(mur);
    SDL_FreeSurface(claie);
    SDL_FreeSurface(purple);

    SDL_FreeSurface(cache);
    
    //Surface elementaire
    SDL_FreeSurface(screen);
    SDL_FreeSurface(icon);
    SDL_FreeSurface(background);
    SDL_FreeSurface(pause_foreground);

    //Surface pour le texte
    SDL_FreeSurface(textTourActuel);
    SDL_FreeSurface(textFinTour);
    SDL_FreeSurface(texteDeplacement);
    SDL_FreeSurface(attaque_1);
    SDL_FreeSurface(attaque_2);
    SDL_FreeSurface(attaque_3);
    SDL_FreeSurface(infoAttaque_1);
    SDL_FreeSurface(infoAttaque_2);

    //Surface boutton
    SDL_FreeSurface(buttonPause);
    SDL_FreeSurface(buttonAbandon);

    //Liberation variable TTF police
    TTF_CloseFont(endfont);
    TTF_CloseFont(font);
    TTF_CloseFont(small_font);
    TTF_CloseFont(simple_font);

    //destructions du rendue et de la fenêtre
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    
    //On quite la librairie et les extension 

    TTF_Quit();
    IMG_Quit();
    SDL_Quit();

    if(replay)
        return replay;
    
    return 0;
}

/*Fonction pour renvoyer une erreur*/
/**
 * \fn void SDLErrorr(const char *message,SDL_Window *window,SDL_Renderer *renderer)
 * \brief fonction de renvoie d'une erreur pour la SDL2
 *
 * \param une chaine de caractère contenant un message le pointeur vers la fenêtre et le un autre vers le rendue pour les détruires
 *
 * \return void pas de retour
 *
 */
void SDLErrorr(const char *message)
{
    SDL_Log("Erreur : %s > %s ! \n",message,SDL_GetError());
    SDL_Quit();
    exit(EXIT_FAILURE);
}

/*Fonction de chargement d'une image*/
/**
 * \fn SDL_Surface * chargementImage(SDL_Surface * PtImage,char *chemin)
 * \brief fonction de chargement d'image servant pour la map en 2D
 *
 * \param le nom du pointeur de type SDL_Surface et le chemin de l'image
 *
 * \return SDL_Surface
 *
 */
SDL_Surface * chargementImage(SDL_Surface * PtImage,char *chemin)
{
    PtImage=IMG_Load(chemin);
    if(PtImage==NULL)
    {
        //indique le chemin sur le stderr de l'image introuvable
        fprintf(stderr," can't find  the : %s",chemin);
        SDLErrorr("Failed to Load Image check out the stderr output for more information");
    }
    return PtImage;
}

/**
 * \fn personnage_t * select_pers_SDL(liste_perso_t * armee, equipe_t equipe, race_t * race2, int * nb_arc, int * nb_ass, int * nb_heal, int * nb_tank, int * nb_epe, int * nb_mage,int personnage)
 * \param armee liste dans laquel on veux ajouter un personnage
 * \param equipe couleur de l'equipe du personnage
 * \param race2 race du personnage
 * \param nb_arc sert a controler le nombre d'archer
 * \param nb_ass sert a controler le nombre d'assassin
 * \param nb_heal sert a controler le nombre de healer
 * \param nb_tank sert a controler le nombre de tank
 * \param nb_epe sert a controler le nombre d'epeiste
 * \param nb_mage sert a controler le nombre de mage
 * \param personnage represente la valeur de la classe du personnage
 * 
 * \brief retourne un personnage choisis par l'utilisateur tout en controlant le nombre de chaque unité
 * (est apeller par liste_perso_equipe_creer_SDL)
 *
 * \return pers (un personnage de type personnage_t)
 */
personnage_t * select_pers_SDL(liste_perso_t * armee, equipe_t equipe, race_t * race2, int * nb_arc, int * nb_ass, int * nb_heal, int * nb_tank, int * nb_epe, int * nb_mage,int personnage){
  archer_t * archer;
  mage_t * mage;
  healer_t* healer;
  assassin_t* assassin;
  tank_t * tank;
  epeiste_t* epe;
  personnage_t * pers = NULL;

    switch (personnage) {

      case(1):
        if((*nb_arc) != 2)
        {
          archer = creer_archer(equipe, race2, armee);
          (*nb_arc)++;
          return ((personnage_t*)archer);
        }
        
        break;

      case(2):
        if((*nb_mage) != 2)
        {
          mage = creer_mage(equipe,  race2, armee);
          (*nb_mage)++;
          return ((personnage_t*)mage);
        }
        break;


      case(3):
        if((*nb_heal) != 2)
        {
          healer = creer_healer(equipe,  race2, armee);
          (*nb_heal)++;
          return ((personnage_t*)healer);
        }
        break;


      case(4):
        if((*nb_ass) != 2)
        {
          assassin = creer_assassin(equipe,  race2, armee);
          (*nb_ass)++;
          return ((personnage_t*)assassin);
        }
        break;


      case(5):
        if((*nb_tank) != 2)
        {
          tank = creer_tank(equipe,  race2, armee);
          (*nb_tank)++;
          return ((personnage_t*)tank);
        }
        break;


      case(6):
        if((*nb_epe) != 2)
        {
          epe = creer_epeiste(equipe,  race2, armee);
          (*nb_epe)++;
          return ((personnage_t*)epe);
        }
        break;

    }

  return pers;

}

/**
 * \fn void liste_perso_equipe_creer_SDL(liste_perso_t * armee1, equipe_t equipe,int selection,int personnage)
 * \param armee1 liste dans laquel ajouter un personnage
 * \param equipe couleur de l'equipe de personnage
 * \param selection represente la valeur de la race
 * \param personnage represente la valeur de la classe
 * 
 * \brief creer une liste de perssonage avec une race et une classe
 *
 * \return void pas de retour
 */
void liste_perso_equipe_creer_SDL(liste_perso_t * armee1, equipe_t equipe,int selection,int personnage){
  race_t * demon = race_creer_demon();
  race_t * kobolt = race_creer_orc();
  race_t * humain = race_creer_humain();
  race_t * elfe = race_creer_elfe();
  race_t * orc = race_creer_orc();
  race_t * nain = race_creer_nain();

  int nb_arc = 0, nb_ass = 0, nb_heal = 0, nb_tank = 0, nb_epe = 0, nb_mage = 0;


  personnage_t * perso = NULL;

  int n = 0;


    switch(selection)
    {
      case 1:
        perso =  select_pers_SDL(armee1, equipe, demon, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage,personnage);
        liste_ajouter(armee1, perso);
        n++;
        break;

      case 2:
        perso =  select_pers_SDL(armee1, equipe, humain, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage,personnage);
        liste_ajouter(armee1, perso);
        n++;
        break;

      case 3:
        perso =  select_pers_SDL(armee1, equipe, elfe, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage,personnage);
        liste_ajouter(armee1, perso);
        n++;
        break;

      case 4:
        perso =  select_pers_SDL(armee1, equipe, nain, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage,personnage);
        liste_ajouter(armee1, perso);
        n++;
        break;

      case 5:
        perso =  select_pers_SDL(armee1, equipe, kobolt, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage,personnage);
        liste_ajouter(armee1, perso);
        n++;
        break;

      case 6:
        perso =  select_pers_SDL(armee1, equipe, orc, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage,personnage);
        liste_ajouter(armee1, perso);
        n++;
        break;
    }

}

/**
 * \fn void displayMap(SDL_Surface *sol,SDL_Surface *mur, SDL_Surface *screen, SDL_Window *window, SDL_Rect position,
 * SDL_Surface *ImageArcherRougeBas, SDL_Surface *ImageAssassinRougeBas, SDL_Surface *ImageEpeisteRougeBas, SDL_Surface *ImageHealerRougeBas, SDL_Surface *ImageMageRougeBas, SDL_Surface *ImageTankRougeBas,
 * SDL_Surface *ImageArcherRougeGauche, SDL_Surface *ImageAssassinRougeGauche, SDL_Surface *ImageEpeisteRougeGauche, SDL_Surface *ImageHealerRougeGauche, SDL_Surface *ImageMageRougeGauche, SDL_Surface *ImageTankRougeGauche,
 * SDL_Surface *ImageArcherRougeDroite, SDL_Surface *ImageAssassinRougeDroite, SDL_Surface *ImageEpeisteRougeDroite, SDL_Surface *ImageHealerRougeDroite, SDL_Surface *ImageMageRougeDroite, SDL_Surface *ImageTankRougeDroite,
 * SDL_Surface *ImageArcherRougeHaut, SDL_Surface *ImageAssassinRougeHaut, SDL_Surface *ImageEpeisteRougeHaut, SDL_Surface *ImageHealerRougeHaut, SDL_Surface *ImageMageRougeHaut, SDL_Surface *ImageTankRougeHaut,
 * SDL_Surface *ImageArcherBleuHaut, SDL_Surface *ImageAssassinBleuHaut, SDL_Surface *ImageEpeisteBleuHaut, SDL_Surface *ImageHealerBleuHaut, SDL_Surface *ImageMageBleuHaut, SDL_Surface *ImageTankBleuHaut,
 * SDL_Surface *ImageArcherBleuBas, SDL_Surface *ImageAssassinBleuBas, SDL_Surface *ImageEpeisteBleuBas, SDL_Surface *ImageHealerBleuBas, SDL_Surface *ImageMageBleuBas, SDL_Surface *ImageTankBleuBas,
 * SDL_Surface *ImageArcherBleuGauche, SDL_Surface *ImageAssassinBleuGauche, SDL_Surface *ImageEpeisteBleuGauche, SDL_Surface *ImageHealerBleuGauche, SDL_Surface *ImageMageBleuGauche, SDL_Surface *ImageTankBleuGauche,
 * SDL_Surface *ImageArcherBleuDroite, SDL_Surface *ImageAssassinBleuDroite, SDL_Surface *ImageEpeisteBleuDroite, SDL_Surface *ImageHealerBleuDroite, SDL_Surface *ImageMageBleuDroite, SDL_Surface *ImageTankBleuDroite ){
 *
 * \param sol surface du sol
 * \param mur surface du mur
 * \param screen surface sur laquel on affiche la map
 * \param window fenetre pour l'affichage
 * \param position position ou l'on affiche la map
 * \param ImageArcherRougeBas archer pour equipe rouge (orientation bas)
 * \param ImageAssassinRougeBas assassin pour equipe rouge (orientation bas)
 * \param ImageEpeisteRougeBas epeiste pour equipe rouge (orientation bas)
 * \param ImageHealerRougeBas healer pour equipe rouge (orientation bas)
 * \param ImageMageRougeBas mage pour equipe rouge (orientation bas)
 * \param ImageTankRougeBas tank pour equipe rouge (orientation bas)
 * \param ImageArcherRougeGauche archer pour equipe rouge (orientation gauche)
 * \param ImageAssassinRougeGauche assassin pour equipe rouge (orientation gauche)
 * \param ImageEpeisteRougeGauche epeiste pour equipe rouge (orientation gauche)
 * \param ImageHealerRougeGauche healer pour equipe rouge (orientation gauche)
 * \param ImageMageRougeGauche mage pour equipe rouge (orientation gauche)
 * \param ImageTankRougeGauche tank pour equipe rouge (orientation gauche)
 * \param ImageArcherRougeDroite archer pour equipe rouge (orientation droite)
 * \param ImageAssassinRougeDroite assassin pour equipe rouge (orientation droite)
 * \param ImageEpeisteRougeDroite epeiste pour equipe rouge (orientation droite)
 * \param ImageHealerRougeDroite healer pour equipe rouge (orientation droite)
 * \param ImageMageRougeDroite mage pour equipe rouge (orientation droite)
 * \param ImageTankRougeDroite tank pour equipe rouge (orientation droite)
 * \param ImageArcherRougeHaut archer pour equipe rouge (orientation haut)
 * \param ImageAssassinRougeHaut assassin pour equipe rouge (orientation haut)
 * \param ImageEpeisteRougeHaut epeiste pour equipe rouge (orientation haut)
 * \param ImageHealerRougeHaut healer pour equipe rouge (orientation haut)
 * \param ImageMageRougeHaut mage pour equipe rouge (orientation haut)
 * \param ImageTankRougeHaut tank pour equipe rouge (orientation haut)
 * \param ImageArcherBleuHaut archer pour equipe bleu (orientation haut)
 * \param ImageAssassinBleuHaut assassin pour equipe bleu (orientation haut)
 * \param ImageEpeisteBleuHaut epeiste pour equipe bleu (orientation haut)
 * \param ImageHealerBleuHaut healer pour equipe bleu (orientation haut)
 * \param ImageMageBleuHaut mage pour equipe bleu (orientation haut)
 * \param ImageTankBleuHaut tank pour equipe bleu (orientation haut)
 * \param ImageArcherBleuBas archer pour equipe bleu (orientation bas)
 * \param ImageAssassinBleuBas assassin pour equipe bleu (orientation bas)
 * \param ImageEpeisteBleuBas epeiste pour equipe bleu (orientation bas)
 * \param ImageHealerBleuBas healer pour equipe bleu (orientation bas)
 * \param ImageMageBleuBas mage pour equipe bleu (orientation bas)
 * \param ImageTankBleuBas tank pour equipe bleu (orientation bas)
 * \param ImageArcherBleuGauche archer pour equipe bleu (orientation gauche)
 * \param ImageAssassinBleuGauche assassin pour equipe bleu (orientation gauche)
 * \param ImageEpeisteBleuGauche epeiste pour equipe bleu (orientation gauche)
 * \param ImageHealerBleuGauche healer pour equipe bleu (orientation gauche)
 * \param ImageMageBleuGauche mage pour equipe bleu (orientation gauche)
 * \param ImageTankBleuGauche tank pour equipe bleu (orientation gauche)
 * \param ImageArcherBleuDroite archer pour equipe bleu (orientation droite)
 * \param ImageAssassinBleuDroite assassin pour equipe bleu (orientation droite)
 * \param ImageEpeisteBleuDroite epeiste pour equipe bleu (orientation droite)
 * \param ImageHealerBleuDroite healer pour equipe bleu (orientation droite)
 * \param ImageMageBleuDroite mage pour equipe bleu (orientation droite)
 * \param ImageTankBleuDroite tank pour equipe bleu (orientation droite)
 * 
 * 
 * \brief Affiche la map avec les mur le sol et les perssonage dans la couleur de leur equipe avec une orientation specifique celon la map
 *
 * \return void pas de retour
 */
void displayMap(SDL_Surface *sol,SDL_Surface *mur, SDL_Surface *screen, SDL_Window *window, SDL_Rect position,
 SDL_Surface *ImageArcherRougeBas, SDL_Surface *ImageAssassinRougeBas, SDL_Surface *ImageEpeisteRougeBas, SDL_Surface *ImageHealerRougeBas, SDL_Surface *ImageMageRougeBas, SDL_Surface *ImageTankRougeBas,
 SDL_Surface *ImageArcherRougeGauche, SDL_Surface *ImageAssassinRougeGauche, SDL_Surface *ImageEpeisteRougeGauche, SDL_Surface *ImageHealerRougeGauche, SDL_Surface *ImageMageRougeGauche, SDL_Surface *ImageTankRougeGauche,
 SDL_Surface *ImageArcherRougeDroite, SDL_Surface *ImageAssassinRougeDroite, SDL_Surface *ImageEpeisteRougeDroite, SDL_Surface *ImageHealerRougeDroite, SDL_Surface *ImageMageRougeDroite, SDL_Surface *ImageTankRougeDroite,
 SDL_Surface *ImageArcherRougeHaut, SDL_Surface *ImageAssassinRougeHaut, SDL_Surface *ImageEpeisteRougeHaut, SDL_Surface *ImageHealerRougeHaut, SDL_Surface *ImageMageRougeHaut, SDL_Surface *ImageTankRougeHaut,
 SDL_Surface *ImageArcherBleuHaut, SDL_Surface *ImageAssassinBleuHaut, SDL_Surface *ImageEpeisteBleuHaut, SDL_Surface *ImageHealerBleuHaut, SDL_Surface *ImageMageBleuHaut, SDL_Surface *ImageTankBleuHaut,
 SDL_Surface *ImageArcherBleuBas, SDL_Surface *ImageAssassinBleuBas, SDL_Surface *ImageEpeisteBleuBas, SDL_Surface *ImageHealerBleuBas, SDL_Surface *ImageMageBleuBas, SDL_Surface *ImageTankBleuBas,
 SDL_Surface *ImageArcherBleuGauche, SDL_Surface *ImageAssassinBleuGauche, SDL_Surface *ImageEpeisteBleuGauche, SDL_Surface *ImageHealerBleuGauche, SDL_Surface *ImageMageBleuGauche, SDL_Surface *ImageTankBleuGauche,
 SDL_Surface *ImageArcherBleuDroite, SDL_Surface *ImageAssassinBleuDroite, SDL_Surface *ImageEpeisteBleuDroite, SDL_Surface *ImageHealerBleuDroite, SDL_Surface *ImageMageBleuDroite, SDL_Surface *ImageTankBleuDroite ){

    int i,j;
    for (i = 0 ; i < M ; i++){
            for (j = 0 ; j < N ; j++){
                position.x = i * TAILLE_BLOC;
                position.y = j * TAILLE_BLOC;
                switch(map[j][i].type_emplacement)
                { 
                    case VIDE:
                        SDL_BlitSurface(sol, NULL, screen, &position);
                        break;
                    case MUR:
                        SDL_BlitSurface(mur, NULL, screen, &position);
                        break;

                    case PERSO:
                    //Definie l'orientation celon la map
                        if(map[17][21].type_emplacement==MUR){
                            if(map[j][i].perso->equipe==Rouge)
                                map[j][i].perso->direction=bas;
                            else
                                map[j][i].perso->direction=haut;
                            
                        }
                        else if(map[N/2][M/2].type_emplacement==MUR){
                            if(map[j][i].perso->equipe==Rouge)
                                map[j][i].perso->direction=bas;
                            else
                                map[j][i].perso->direction=haut;
                        }
                        else{
                            if(map[j][i].perso->equipe==Rouge)
                                map[j][i].perso->direction=gauche;
                            else
                                map[j][i].perso->direction=droite;
                        }
                        switch(map[j][i].perso->classe)
                        {   //Affiche un perssonage celon la classe
                            case archer:
                                if(map[j][i].perso->equipe==Rouge){//test l'equipe pour savoir en quel couleur afficher 
                                    if(map[j][i].perso->direction==bas)//test les quatre direction pour savoir laquel choisir
                                        SDL_BlitSurface(ImageArcherRougeBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageArcherRougeHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageArcherRougeGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageArcherRougeDroite,NULL,screen,&position);
                                }
                                else{//De meme avec l'equipe bleu
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageArcherBleuBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageArcherBleuHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageArcherBleuGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageArcherBleuDroite,NULL,screen,&position);
                                }
                                    
                                break;
                            //On effectue cela pour tout les perssonnage des deux equipe
                            case assassin:
                                if(map[j][i].perso->equipe==Rouge){
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageAssassinRougeBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageAssassinRougeHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageAssassinRougeGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageAssassinRougeDroite,NULL,screen,&position);
                                }
                                else{
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageAssassinBleuBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageAssassinBleuHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageAssassinBleuGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageAssassinBleuDroite,NULL,screen,&position);
                                }
                                    
                                break;

                            case epeiste:
                                if(map[j][i].perso->equipe==Rouge){
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageEpeisteRougeBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageEpeisteRougeHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageEpeisteRougeGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageEpeisteRougeDroite,NULL,screen,&position);
                                }
                                else{
                                   if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageEpeisteBleuBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageEpeisteBleuHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageEpeisteBleuGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageEpeisteBleuDroite,NULL,screen,&position); 
                                }
                                    
                                break;

                            case healer:
                                if(map[j][i].perso->equipe==Rouge){
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageHealerRougeBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageHealerRougeHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageHealerRougeGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageHealerRougeDroite,NULL,screen,&position);
                                }
                                else{
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageHealerBleuBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageHealerBleuHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageHealerBleuGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageHealerBleuDroite,NULL,screen,&position);
                                }
                                    
                                break;

                            case mage:
                                if(map[j][i].perso->equipe==Rouge){
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageMageRougeBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageMageRougeHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageMageRougeGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageMageRougeDroite,NULL,screen,&position);
                                }
                                else{
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageMageBleuBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageMageBleuHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageMageBleuGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageMageBleuDroite,NULL,screen,&position);
                                }
                                    
                                break;

                            case tank:
                                if(map[j][i].perso->equipe==Rouge){
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageTankRougeBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageTankRougeHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageTankRougeGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageTankRougeDroite,NULL,screen,&position);
                                }                                  
                                else{
                                    if(map[j][i].perso->direction==bas)
                                        SDL_BlitSurface(ImageTankBleuBas,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==haut)
                                        SDL_BlitSurface(ImageTankBleuHaut,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==gauche)
                                        SDL_BlitSurface(ImageTankBleuGauche,NULL,screen,&position);
                                    else if(map[j][i].perso->direction==droite)
                                        SDL_BlitSurface(ImageTankBleuDroite,NULL,screen,&position);
                                }
                                    
                                break;
                    }
                    break;
                }
                
            }
    }
    SDL_UpdateWindowSurface(window);

}

/**
 * \fn void displayZoneAttaque(SDL_Surface *purple, SDL_Surface *screen, SDL_Rect position, SDL_Window *window)
 * \param purple surface violet pour mettre en evidence la zone d'attaque
 * \param screen surface ou l'on affiche l'attaque
 * \param position position ou l'on affiche les tuiles violet
 * \param window fenetre sur laquel on affiche le tout
 * 
 * \brief Affiche en violet la zone d'une attaque 
 *
 * \return void pas de retour
 */
void displayZoneAttaque(SDL_Surface *purple, SDL_Surface *screen, SDL_Rect position, SDL_Window *window){
    int i,j;
    for(i=0;i<N;i++){
        for(j=0;j<M;j++){
            position.x = j * TAILLE_BLOC;
            position.y = i * TAILLE_BLOC;
            if(map[i][j].po_attaque==1){
                SDL_BlitSurface(purple,NULL,screen,&position);
                }
            }
        }
    SDL_UpdateWindowSurface(window);
}

/**
 * \fn void displayZoneDeplacement(SDL_Surface *claie, SDL_Surface *screen, SDL_Rect position, SDL_Window *window)
 * \param claie surface jaune/marron clair pour mettre en evidence la zone de deplacement
 * \param screen surface ou l'on affiche le deplacement 
 * \param position position ou l'on affiche les tuiles couleur clai
 * \param window fenetre sur laquel on affiche le tout
 * 
 * \brief Affiche en jaune/marron claire la zone de deplacement
 *
 * \return void pas de retour
 */
void displayZoneDeplacement(SDL_Surface *claie, SDL_Surface *screen, SDL_Rect position, SDL_Window *window){
    int i,j;
    for(i=0;i<N;i++){
        for(j=0;j<M;j++){
            position.x = j * TAILLE_BLOC;
            position.y = i * TAILLE_BLOC;
            if(map[i][j].po_deplacement==1){
                SDL_BlitSurface(claie,NULL,screen,&position);
                }
            }
        }
    SDL_UpdateWindowSurface(window);
}

/**
 * \fn int lire_liste_attaques(personnage_t * joueur, t_pos case_cible, int portee)
 * \param joueur personnage qui lance l'attaque
 * \param case_cible case que l'on veux atteindre aved l'attaque
 * \param portee porte de l'attaque
 * 
 * \brief permet de savoir si la case selectionner est a portee
 *
 * \return int 0 (Reussite) | 1 (Echec)
 */
int lire_liste_attaques(personnage_t * joueur, t_pos case_cible, int portee){

    int i;	
	liste_pos_t * liste_de_positions = attaque_valide(joueur,portee);
	t_pos position_possible;	

	for(i = 0; i < liste_de_positions->nb_elem; i++){
		position_possible = liste_position_lire(liste_de_positions, i);

		if(position_possible.x == case_cible.x && position_possible.y == case_cible.y){
            liste_positions_detruire( &liste_de_positions );
			return 0;			
		}
	}
	liste_positions_detruire( &liste_de_positions );
	return 1;
}

/**
 * \fn void displayInfoAtt(TTF_Font *font,SDL_Color colorFont,SDL_Surface *screen, SDL_Window *window, info_att info_comp)
 * \param font police qui sera utiliser pour afficher les infos
 * \param colorFont couleur de la police
 * \param screen surface ou l'on affiche les information
 * \param window fenetre d'affichage
 * \param info_comp structure contenant toutes les info sur l'attaque
 * 
 * \brief affiche diver information sur l'attaque selectionner
 *
 * \return void pas de retour
 */
void displayInfoAtt(TTF_Font *font,SDL_Color colorFont,SDL_Surface *screen, SDL_Window *window, info_att info_comp){
    char info[120];
    char tooLong[90];
    SDL_Surface *text = NULL;
    SDL_Rect position={710,320};

    TTF_Font *smaller = NULL;

    smaller = TTF_OpenFont("../fonts/DejaVuSans.ttf",19);
    if(smaller==NULL)
        SDLErrorr("Failed to load font");
    
    sprintf(info,"Nom : %s",info_comp.nom);
    text = TTF_RenderUTF8_Solid(font,info,colorFont);
    SDL_BlitSurface(text,NULL,screen,&position);
    position.y+=30;
    
    switch (info_comp.zone){
    case 0:
        sprintf(info,"Zone : ciblage monocible");
        break;
    case 1:
        sprintf(info,"Zone : ciblage 4 case autour");
        break;
    case 2:
        sprintf(info,"Zone : ciblage 12 case autour");
        break;
    default:
        sprintf(info,"Zone : ciblage %i case autour",info_comp.zone);
        break;
    }
    text = TTF_RenderUTF8_Solid(font,info,colorFont);
    SDL_BlitSurface(text,NULL,screen,&position);
    position.y+=30;

    sprintf(info,"Portée : %i",info_comp.portee);
    text = TTF_RenderUTF8_Solid(font,info,colorFont);
    SDL_BlitSurface(text,NULL,screen,&position);
    position.y+=30;

    switch (info_comp.type){
    case Attaque:
        sprintf(info,"Type : Attaque");
        break;
    case Soin:
        sprintf(info,"Type : Soin");
        break;
    case NoDamage:
        sprintf(info,"Type : Sortilège");
        break;
    
    default:
        break;
    }

    text = TTF_RenderUTF8_Solid(font,info,colorFont);
    SDL_BlitSurface(text,NULL,screen,&position);
    position.y+=30;

    sprintf(info,"Dégats minimum : %i",info_comp.degats_min);
    text = TTF_RenderUTF8_Solid(font,info,colorFont);
    SDL_BlitSurface(text,NULL,screen,&position);
    position.y+=30;

    sprintf(info,"Dégats maximum : %i",info_comp.degats_max);
    text = TTF_RenderUTF8_Solid(font,info,colorFont);
    SDL_BlitSurface(text,NULL,screen,&position);
    position.y+=30;


    if(strlen(info_comp.description)>21){

        strncpy(tooLong,info_comp.description,21);

        sprintf(info,"Déscription : %s",tooLong);
    
        text = TTF_RenderUTF8_Solid(smaller,info,colorFont);
        SDL_BlitSurface(text,NULL,screen,&position);

        if(strlen(info_comp.description)>95){

            strncpy(tooLong,info_comp.description+21,34);

            position.y+=30;
            text = TTF_RenderUTF8_Solid(smaller,tooLong,colorFont);
            SDL_BlitSurface(text,NULL,screen,&position);


            strncpy(tooLong,info_comp.description+55,34);

            position.y+=30;
            text = TTF_RenderUTF8_Solid(smaller,tooLong,colorFont);
            SDL_BlitSurface(text,NULL,screen,&position);

            strncpy(tooLong,info_comp.description+89,strlen(info_comp.description));

            position.y+=30;
            text = TTF_RenderUTF8_Solid(smaller,tooLong,colorFont);
            SDL_BlitSurface(text,NULL,screen,&position);
        }
        else if(strlen(info_comp.description)>55){

            strncpy(tooLong,info_comp.description+21,34);

            position.y+=30;
            text = TTF_RenderUTF8_Solid(smaller,tooLong,colorFont);
            SDL_BlitSurface(text,NULL,screen,&position);


            strncpy(tooLong,info_comp.description+55,strlen(info_comp.description));

            position.y+=30;
            text = TTF_RenderUTF8_Solid(smaller,tooLong,colorFont);
            SDL_BlitSurface(text,NULL,screen,&position);

        }
        else{
            strncpy(tooLong,info_comp.description+21,strlen(info_comp.description));

            position.y+=30;
            text = TTF_RenderUTF8_Solid(font,tooLong,colorFont);
            SDL_BlitSurface(text,NULL,screen,&position);
        }
            
        
        
    }
    else{
        sprintf(info,"Déscription : %s",info_comp.description);
        text = TTF_RenderUTF8_Solid(font,info,colorFont);
        SDL_BlitSurface(text,NULL,screen,&position);
    }
    
    SDL_UpdateWindowSurface(window);
    TTF_CloseFont(smaller);
}