/**
 * \file Generique_debut_Terminal.c
 * \brief Fichier affichant le générique
 * \author 
 * \version 1.0
 * \date 
 *
 * Affiche les texte d'introduction de l'univers
 *
 */

#include<stdio.h>
#include <stdlib.h>
#include <unistd.h> 

/**
 * \fn void Generique_debut_terminal(void)
 * \brief fonction qui affiche les message du generique en laissant une pause entre chaque message
 * 
 * \param self
 * 
 * \return void pas de retour
 * 
 */
void Generique_debut_terminal()
{
// void Generique_debut_terminal(void)
//param self
//brief Affiche le générique de début
//return void
    printf("à l'aube des temps naissat l'univers de Doncon\n\n");
    sleep(3);
    printf("les dieux créerent plusieurs mondes\n\n"); 
    sleep(3);
    printf("le monde de Drargtor, le royaumme démoniaque,\n\n");
    sleep(3);
    printf("le monde de Shaosaka, le monde des anges,\n\n");
    sleep(3);
    printf("et mundus le monde du milieu\n\n");
    sleep(2);
    printf("Sur le monde de mundus vives plusieurs espèces:\n");
    sleep(2); 
    printf("les elfes des humanoides aux traits parfaits vivant dans les fôrets\n");
    sleep(2);
    printf("les humains:une espèce humanoide très adaptatives\n");
    printf("les nains: une espèces dont la barbe recouvrent l'entièreté de leur corps vivant dans les mines et adorant la boisson alcoolisé\n");
    sleep(2);
    print("les orcs:une espèce malodorante adorant se battre et assez obsédée\n");
    sleep(2);
    printf("les kobolt:une espèce faiblarde cousine des nains,rusé rapide et intélligente\n\n");
    sleep(2);
    printf("Bien que ses espèces vécurent en paix et luttant côte à côte contre les démons pendant un temps...\n");
    sleep(2);
    printf("L'an 400 fut le témoin d'un évènement tragique... la grande déchirure\n");
    sleep(2);
    printf("nous ne savons pas qu'elle race furent à l'origine du conflit... mais ses conséquences furent tragique.\n");
    sleep(2);
    printf("C'est ainsi que naquit deux clans:\n\n");
    sleep(2);
    printf("le clan de Werspell\n\n sous cette bannière se sont rangés les humains, les elfes et les nains.\n\n");
    sleep(2);
    printf("le clan de Garthsol\n\n sous cette banière se sont rangé les races des orcs des kobolt et des démons\n\n");
    sleep(2);
    printf("les anges quand à eux en accord avec leur principe resta à l'exterieur du conflit.\n\n");
    sleep(2);
    printf("luttant pour Werspell...ou pour Garthsol...\n\n");
    sleep(2);
    printf("C'est à vous de définir qui vous serez,ce que vous Serez....\n\n");
    sleep(4);
    printf("bienvenue dans ... \n");
    sleep(4);
    printf("Tactics Arena: Carnage Dans Mundus");
}

/**
 * \fn int main(void)
 * \brief fonction main
 * 
 * \param self
 * 
 * \return int 0
 * 
 */
int main()
{
    Generique_debut_terminal();
    return 0;
}
