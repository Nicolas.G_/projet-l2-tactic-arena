#ifndef H_MAP
#define H_MAP

#include <stdio.h>
#include<time.h>
#include<stdlib.h>
#include<string.h>
#include "personnage.h"
#include "commun.h"
/**
 * \file map.h
 * \brief Définition des structures de données et prototypes des fonctions liées à l'affichage de la carte
 * \author Yohann Delacroix
 * \version 1.0
 * \date 12 Février 2021
 */



//Définition des constantes
#define N 18 /**<N représente la taille de la map en hauteur */
#define M 22 /**<M représente la taille de la map en largeur */

#define MUR -1 /**<Emplacement de type obstacle non atteignable pour un joueur */
#define VIDE 0 /**<Emplacement vide potentiellement atteignable pour un joueur */
#define PERSO 1 /**<Emplacement où un personnage est positionné */
#define POSITION_ROUGE 2 /**<Emplacement de départ de l'équipe rouge */
#define POSITION_BLEUE 3/**<Emplacement de départ de l'équipe bleue */


/**
 * \struct t_carte
 * \brief Structure de données pour une case de la map
 * Contient le type d'emplacement de la case
 * Le personnage positionné sur cet emplacement si il y en a un
 * Un champ po_deplacement pour déterminer par 0 ou 1 si cette case est atteignable par un autre personnage pour un déplacement
 * Un champ po_attaque pour déterminer par 0 ou 1 si cette case est atteignable par un autre personnage pour une attaque
 */
typedef struct t_carte_s
{
	int type_emplacement;
	personnage_t * perso;
	int po_deplacement;
	int po_attaque;
}t_carte;

/**
 * \enum t_affichage_map
 * \brief Mode d'affichage pour la carte en mode console
 * VISUEL interprete les valeurs pour afficher une map visuellement cohérente
 * BINAIRE affiche des entiers correspondant aux define MUR, VIDE, PERSO, POSITION_ROUGE et POSITION_BLEUE
 */
typedef enum {BINAIRE, VISUEL} t_affichage_map;
t_carte map[N][M];/**<Définition de la variable globale de map qui est utilisée dans tout le programme */


//Prototypes de fonctions
int tirage();
int chargement_carte();
void afficher_plateau(t_affichage_map mode_affichage);
void placer_equipes(liste_perso_t * equipe_rouge, liste_perso_t * equipe_bleue);

t_pos premiere_position_equipe_adverse(t_pos position, equipe_t couleur);
void initialiser_orientation_personnages(liste_perso_t * equipe);

//Variable utilisée pour le generateur de carte
int plateau[N][M];
#endif
