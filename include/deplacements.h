
#ifndef _DEPLACEMENTS_H_
#define _DEPLACEMENTS_H_

#include <stdio.h>
#include "personnage.h"
#include "commun.h"
#include "liste_positions.h"
#include <math.h>
/**
 * \file deplacements.h
 * \brief Définition des structures de données et prototypes des fonctions liées aux déplacements des personnages
 * \author Yohann Delacroix
 * \version 1.0
 * \date 12 Février 2021
 */


	/* DEBUT FONCTIONS à UTILISER POUR IMPLEMENTER LA SDL */

//Renvoie une liste de cases que le joueur peut atteindre
liste_pos_t* deplacement_valide(personnage_t * joueur);

//Ajouter et retirer des marqueurs sur la carte pour connaître les déplacements possibles d'un personnage
void ajouter_marqueurs_deplacements_carte(liste_pos_t * liste_positions);
void retirer_marqueurs_deplacements_carte(liste_pos_t * liste_positions);

//Déplace le joueur sur la case renseignée en paramètre
int effectuer_deplacement(personnage_t * joueur, t_pos case_cible);

	/* FIN */



//Propose un déplacement au joueur en version terminale
void proposer_deplacement_terminal(personnage_t * joueur);

//Fonctions internes
int changer_orientation_personnage(personnage_t * perso, t_pos arrivee);
int compter_cases_valides(int taille_zone, t_pos case_cible);
#endif