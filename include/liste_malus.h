/**
  \file liste_malus.h
  \brief Les prototypes de la gestion des malus
  \details ce fichier contient la structure de la liste de malus et les prototypes suivants :
  - liste_malus_initialiser
  - liste_malus_supprimer
  - liste_malus_pleine
  - liste_malus_vide
  - liste_malus_creer
  - liste_malus_ajouter
  - liste_malus_afficher
  - liste_malus_actualiser
  - liste_malus_vider
  - liste_malus_detruire

  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_healer, healer_t creer_healer
*/




/*Définition de la structure liste de malus*/

#ifndef H_LISTE_MALUS
#define H_LISTE_MALUS

#include "malus.h"
#include "commun.h"


typedef struct liste_malus_s liste_malus_t;

/**
 * \struct liste_malus_t
 * \brief Structure de la liste de malus
 * - nb_malus
 * - liste_malus
 *
 */
struct liste_malus_s{
  int nb_malus;
  malus_t ** malus_liste;
};

/*Fonctions pour modifier la liste de malus*/

extern
void liste_malus_initialiser();

/*Met à jour la liste de malus*/
extern
void liste_malus_supprimer(int);


extern
bool liste_malus_pleine();


extern
bool liste_malus_vide();


extern
liste_malus_t * liste_malus_creer();

extern
void liste_malus_ajouter(type_malus_t type, int duree, personnage_t * cible, int malus);

extern
void liste_malus_afficher();

extern
void liste_malus_actualiser();

extern
void liste_malus_vider();

extern
err_t liste_malus_detruire();


/*je creer une liste de malus global*/

liste_malus_t * liste_malus;

#endif
