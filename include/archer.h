/**
  \file archer.h
  \brief Les en-tête de la classe archer
  \details ce fichier contient la structure de la classe archer et les prototypes de fonctions :
  - existe_archer
  - creer_archer
  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_archer, archer_t creer_archer
*/



#ifndef H_ARCHER
#define H_ARCHER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "personnage.h"
#include "attaques.h"
#include "nom_alea.h"


/*
  variables globales
*/


/*
Défnition de la strucure archer
*/

/**
 * \struct archer_t
 * \brief Structure définissant un archer
 */
typedef struct archer_s archer_t;

struct archer_s{
    #include "caract_perso.h"
};

/*
Fonctions
*/

extern
int existe_archer(archer_t * arc);

extern
archer_t * creer_archer(equipe_t equipe, race_t * race, liste_perso_t * liste);


#endif
