/**
 * \file systeme.h
 * \brief Fichier d'avantage
 * \author
 * \version 1.0
 * \date
 *
 * test les classe de deux perssonage et retourne un float si avantage > 1 ou < 1 pour desavantage
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "personnage.h"

extern
float Systeme(personnage_t * perso1, personnage_t * perso2);
